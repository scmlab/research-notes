\documentclass{article}

% build by
%  lhs2TeX FastFib.lhs | pdflatex --jobname=FastFib

%if False
\begin{code}
module FastFib where
\end{code}
%endif

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{scalerel}

%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt

%include ../common/Formatting.fmt

%let showproofs = True



\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\todo}[1]{{\bf todo}: {#1}}

%format x ^ y = "{" x "}^{" y "}"
%format (exp x (y)) = "{" x "}^{" y "}"
%format (sq x) = "{" x "}^{2}"
%format (db n) = "2" n
%format * = "\times"

\begin{document}

\title{Fast Fibonacci Using Vorob'ev's Equation}
\author{Shin-Cheng Mu}
\date{Feb 2019}
\maketitle

Recall the definition
\begin{spec}
fib 0      = 0
fib 1      = 1
fib (2+n)  = fib (1+n) + fib n {-"~~."-}
\end{spec}
This note is about a fast algorithm to compute |fib n| that uses $O(\log n)$ recursive calls
(which does not imply that the running time is $O(\log n)$ ---
far from it, in fact, as shown by Holloway~\cite{Holloway:88:Algorithms}).
In the note below we abbreviate |fib| to |f|.

One cannot help thinking whether we can express |f (k+n)| in terms of |f k| and |f n|.
If that is possible we may have a fast algorithm for computing |f|.
We do not have that yet but we have something good enough.
Holloway~\cite{Holloway:88:Algorithms} mentioned the following equation, which he attributed to Vorob'ev~\cite{Vorobev:11:Fibonacci}:
for |n >= 1| we have
\begin{equation}
   |f (k+n) = f (1+k) * f n + f k * f (n-1)|~~\mbox{~~.}
     \label{eq:vor0}
\end{equation}
Although \eqref{eq:vor0} is more symmetrical, for inductive proof we use this variation which holds for all natural number |n|:
\begin{equation}
   |f (1+k+n) = f (1+k) * f (1+n) + f k * f n|~~\mbox{~~.}
     \label{eq:vor1}
\end{equation}
From them we may conclude:
\begin{align}
  |f (1+db n)| &= |sq (f (1+n)) + sq (f n)| \mbox{~~,}\label{eq:bin-odd}\\
    |f (db n)| &= |2 * f(1+n) * f n - sq (f n)|  ~~\mbox{~~.}\label{eq:bin-even}
\end{align}
Equation \eqref{eq:bin-odd} follows immediately if we let |k := n| in \eqref{eq:vor1}. Validty of \eqref{eq:bin-even} will be shown later.

%format fib2
Let |fib2 = (fib n, fib (1+n))|.
With \eqref{eq:bin-odd} and \eqref{eq:bin-even} one can come up with the following algorithm:

\begin{code}
fib2 :: Integer -> (Integer, Integer)
fib2 0 = (0,1)
fib2 n  | n `mod` 2 == 0  = (c,d)
        | otherwise       = (d, c+d) {-"~~,"-}
  where  (a,b) = fib2 (n `div` 2)
         c  = 2 * a * b - a * a
         d  = a * a + b * b {-"~~."-}
\end{code}

Now we show that \eqref{eq:bin-even} is valid.
For |n := 0| it is immediate. For |n > 0| (such that |f (n-1)| is defined),
we let |k := n-1| in \eqref{eq:vor1}:
\begin{spec}
     (f (1+k+n) = f (1+k) * f (1+n) + f k * f n)[n-1 / k]
<=>  f (db n) = f n * f(1+n) + f (n-1) * f n
<=>    {- since |f (n-1) = f (n+1) - f n| -}
     f (db n) = f n * f(1+n) + (f (n+1) - f n ) * f n
<=>  f (db n) = 2 * f(1+n) * f n - sq (f n)  {-"~~."-}
\end{spec}

Finally we prove \eqref{eq:vor1}.
\begin{proof} Induction on |n|.

\noindent{\bf Case} |n:=0|: both sides reduce to |f (1+k)|.

\noindent{\bf Case} |n:=1|:
\begin{spec}
   f (1+k) * f 2 + f k * f 1
=  f (1+k) + f k
=  f (2+k) {-"~~."-}
\end{spec}

\noindent{\bf Case} |n:=2+n|. Assuming \eqref{eq:vor1} holds
for |n| and |1+n|, we reason:
\begin{spec}
   f (3+k+n)
=    {- definition of |f| -}
   f (2+k+n) + f (1+k+n)
=    {- inductive hypothesis for |n| and |1+n| -}
   f (1+k) * f (2+n) + f k * f (1+n) +
      f (1+k) * f (1+n) + f k * f n
=   {- arithmetics -}
   f (1+k) * (f (2+n) + f(1+n)) + f k * (f (1+n) + f n)
=   {- definition of |f| -}
   f (1+k) * f (3+n) + f k * f (2+n) {-"~~."-}
\end{spec}
\end{proof}

\bibliographystyle{alpha}
\bibliography{../common/bib}

\end{document}
