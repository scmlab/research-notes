\documentclass{article}

% build by
%  lhs2TeX FoldIter.lhs | pdflatex --jobname=FoldIter

\usepackage{amsmath}
\usepackage{amsthm}

%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt

%include ../common/Formatting.fmt

%let showproofs = True


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\todo}[1]{{\bf todo}: {#1}}

\begin{document}

\title{Fold and Iteration for Lists and Natural Numbers}
\author{Shin-Cheng Mu}
\date{July 2018}
\maketitle

\noindent {\em Inspired by Olivier Danvy's draft ``Folding left and right over natural numbers.''}

%if False
\begin{code}
e :: a
e = undefined

oplus :: a -> b -> b
oplus = undefined

type List a = [a]
type Nat = Int

z :: a
z = undefined

s :: a -> a
s = undefined
\end{code}
%endif

\paragraph{Fold and iteration on lists}
Given |e :: a| and |oplus :: a -> b -> b|, we have |foldr oplus e :: List a -> b|. Abbreviate |foldr oplus e| to |fl| (for ``fold on lists'') for brevity. We recall its definition:
\begin{code}
fl []      = e
fl (x:xs)  = x `oplus` fl xs {-"~~."-}
\end{code}
Also define |il :: List a -> b -> b| (for ``iteration on lists''):
\begin{code}
il []      w  = w
il (x:xs)  w  = il xs (x `oplus` w) {-"~~."-}
\end{code}
Certainly, |fl| and |il| compute different values:
\begin{spec}
fl [x0, x1, x2]    = x0 `oplus` (x1 `oplus` (x2 `oplus` e)) {-"~~,"-}
il [x0, x1, x2] w  = x2 `oplus` (x1 `oplus` (x0 `oplus` w)) {-"~~."-}
\end{spec}

\paragraph{Fold and iteration on natural numbers}
Similarly, given |z :: b| and |s :: b -> b|, one can define fold and interation on natural numbers:
\begin{spec}
fn :: Nat -> b
fn 0        = z
fn (Suc n)  = s (fn n) {-"~~,"-}

ni :: Nat -> b -> b
ni 0       w = w
ni (Suc n) w = ni n (s w) {-"~~."-}
\end{spec}
In contrast to lists, |fn| and |ni| compute the same value in the sense below:
\begin{theorem}\label{thm:fn-ni-eq} For all |n :: Nat| we have
|fn n = ni n z|.
\end{theorem}
\begin{proof} Induction on |n|. For case |n := 0|, both sides reduce to |z|. Note that this is the case that forbids us to generalize |z| to arbitrary values.

For the case |n := Suc n| we reason:
\begin{spec}
   fn (Suc n)
=  s (fn n)
=   {- induction -}
   s (ni n z)
=   {- Lemma~\ref{lma:s-ni} -}
   ni n (s z)
=  ni (Suc n) z {-"~~."-}
\end{spec}
\end{proof}
In the penultimate step of the proof we need a lemma that allows us to transpose
|s| and |ni n|:
\begin{lemma}\label{lma:s-ni}
For all |n| and |w| we have |s (ni n w) = ni n (s w)|.
\end{lemma}
\begin{proof} Induction on |n|. For |n := 0|, both sides reduce to |s w|. For the |n := Suc n| case:
\begin{spec}
   s (ni (Suc n) w)
=  s (ni n (s w))
=   {- induction -}
   ni n (s (s w))
=  ni (Suc n) (s w) {-"~~."-}
\end{spec}
\end{proof}

\paragraph{Conditional equivalence for lists} The proofs carry over to lists. The following is the list counterpart of Lemma~\ref{lma:s-ni}.
\begin{lemma}\label{lma:oplus-il}
For all |x|, |xs|, and |w| we have
|x `oplus` (il xs w) = il xs (x `oplus` w)|, if |x `oplus` (y `oplus` w) = y `oplus` (x `oplus` w)| for all |x|, |y|, and |w|.
\end{lemma}
\begin{proof} Induction on |xs|. For |xs := []|, both sides reduce to |x `oplus` w|. For |xs := y:xs|:
\begin{spec}
   x `oplus` il (y:xs) w
=  x `oplus` il xs (y `oplus` w)
=   {- induction -}
   il xs (x `oplus` (y `oplus` w))
=   {- assumption: |x `oplus` (y `oplus` w) = y `oplus` (x `oplus` w)| -}
   il xs (y `oplus` (x `oplus` w))
=  il (y:xs) (x `oplus` w) {-"~~."-}
\end{spec}
\end{proof}

\begin{theorem}\label{thm:fl-il-eq}
For all |xs :: List a| we have |fl xs = il xs e|, if |x `oplus` (y `oplus` w) = y `oplus` (x `oplus` w)| for all |x|, |y|, and |w|.
\end{theorem}
\begin{proof} Induction on |xs|.
For case |xs := []|, both sides reduce to |e|. Like natural numbers, this is the case that forbids us to generalize |e| to arbitrary values.

For the inductive case:
\begin{spec}
   fl (x : xs)
=  x `oplus` fl xs
=   {- induction -}
   x `oplus` il xs e
=   {- by Lemma~\ref{lma:oplus-il} -}
   il xs (x `oplus` e)
=  il (x : xs) e {-"~~."-}
\end{spec}
\end{proof}

\paragraph{Question} Can Theorem~\ref{thm:fl-il-eq} be strengthened to an if-and-only-if condition? That is, if |fl xs = il xs e| for all |xs|,
is it always the case that |x `oplus` (y `oplus` w) = y `oplus` (x `oplus` w)| for all |x|, |y|, and |w| (or |w| that is in the range of |fl|?)

Certainly, if we consider |[x,y]|, we get
\begin{spec}
   x `oplus` (y `oplus` e)
=  fl [x,y]
=   {- since |fl xs = il xs e| for all |xs| -}
   il [x,y] e
=  (y `oplus` (x `oplus` e) {-"~~."-}
\end{spec}
But that is only for |e|, not for general |w|.

\paragraph{Constructing iterative programs} We typically construct tail-recursive iterative programs using associativity. However, associativity
is defined only for binary operators of type |b -> b -> b|, while |oplus :: a -> b -> b|. Nevertheless we may have this generalization.
\begin{theorem} \label{fr-il-iter}
Given |odot :: b -> b -> b|. We have that, for all |w| and |xs|:
\begin{spec}
   w `odot` fr xs = il xs w <==
      (forall w . w `odot` e = w) &&
      (forall w x v .  w `odot` (x `oplus` v) = (x `oplus` w) `odot` v) {-"~~."-}
\end{spec}
\end{theorem}
\begin{proof} Induction on |xs|. With |xs := []| we reason:
\begin{spec}
   w `odot` fr []
=  w `odot` e
=   {- assumption: |w `odot` e = w| -}
   w
=  il [] w {-"~~."-}
\end{spec}
With |xs := x:xs| we reason:
\begin{spec}
   w `odot` fr (x:xs)
=  w `odot` (x `oplus` fr xs)
=    {- assumption: |w `odot` (x `oplus` v) = (x `oplus` w) `odot` v| -}
   (x `oplus` w) `odot` fr xs
=    {- induction -}
   il xs (x `oplus` w)
=  il (x:xs) w {-"~~."-}
\end{spec}
\end{proof}
For some intuition on the condition |w `odot` (x `oplus` v) = (x `oplus` w) `odot` v|, we can trace an example:
\begin{spec}
   w `odot` fr [x0,x1,x2]
=  w `odot` (x0 `oplus` (x1 `oplus` (x2 `oplus` e)))
=  (x0 `oplus` w) `odot` (x1 `oplus` (x2 `oplus` e))
=  (x1 `oplus` (x0 `oplus` w)) `odot` (x2 `oplus` e)
=  (x2 `oplus` (x1 `oplus` (x0 `oplus` w))) `odot` e
=  x2 `oplus` (x1 `oplus` (x0 `oplus` w))
=  il [x0,x1,x2] w {-"~~."-}
\end{spec}
We can see how |odot| ``absorbs'' elements from the right.

Easy examples of |oplus| and |odot| includes, |oplus = odot = (+)|. For a less
trivial example, with |oplus = (:)| and |e = []|, the requirements are:
\begin{spec}
ys `odot` []      = ys
ys `odot` (x:xs)  = (x : ys) `odot` xs {-"~~,"-}
\end{spec}
which actually says that |w `odot` xs = reverse xs ++ w|.

\paragraph{Question} Is Theorem~\ref{fr-il-iter} a generalization of Theorem~\ref{thm:fl-il-eq}, or are they just different properties?

\paragraph{Iterative algorithms on numbers} It can be easily shown that
Theorem~\ref{fr-il-iter} has a counterpart for natural numbers.
\begin{theorem} \label{fn-ni-iter}
Given |odot :: b -> b -> b|. We have that, for all |w| and |xs|:
\begin{spec}
   w `odot` fn n = ni n w <==
      (forall w . w `odot` z = w) &&
      (forall w v .  w `odot` (s v) = (s w) `odot` v) {-"~~."-}
\end{spec}
\end{theorem}

\paragraph{Question} Theorem~\ref{thm:fn-ni-eq} holds unconditionally. Is it
the case that, for all |s| and |z|, there always exists an |odot| that meets
the requirements of Theorem~\ref{fn-ni-iter}?

Consider, as an example, Fibonacci numbers:
\begin{spec}
fib 0      = 0
fib 1      = 1
fib (2+n)  = fib (Suc n) + fib n {-"~~."-}
\end{spec}
With the spec |ffib n = (fib (Suc n), fib n)|, we can come up with an inductive definition of |ffib| (which is a fold):
\begin{spec}
ffib 0        = (1,0)
ffib (Suc n)  = s (ffib n) {-"~~,"-}

s (x,y) = (x+y,x) {-"~~."-}
\end{spec}
By Theorem~\ref{thm:fn-ni-eq}, we may construct a tail-recursive program:
\begin{spec}
ifib 0        (x,y) = (x,y)
ifib (Suc n)  (x,y) = ifib n (s (x,y)) {-"~~,"-}
\end{spec}
such that |ffib n = ifib n (1,0)|. For this |s| (and |z = (1,0)|), is there an
|odot| satisfying the conditions of Theorem~\ref{thm:fn-ni-eq}?

A possible hint: with the spec |ifib' n (x,y) = x * fib n + y * fib (Suc n)|,
we can also construct a iterative algorithm computing fib (that returns a single number rather than a pair):
\begin{spec}
ifib' 0        (x,y) = x
ifib' (Suc n)  (x,y) = ifib' n (s (x,y)) {-"~~,"-}
\end{spec}
and we have |fib n = ifib n (0,1)| (note the subtle differences, such as the initial value is not |(1,0)|, and the base case returns |x| rather than |y|). And indeed, if we define |(i,j) `odot` (x,y) = i*x + j*y|, we have
\begin{spec}
   (i,j) `odot` s (x,y)
=  (i,j) `odot` (x+y,x)
=  i * x + i * y + j * x
=  (i+j,i) `odot` (x,y)
=  (s (i,j)) `odot` (x,y) {-"~~."-}
\end{spec}
However, this |odot| does not have type |b -> b -> b|.
\end{document}
