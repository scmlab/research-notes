module Test where

import Control.Monad
import ProtocolT

test1 x y = run $ (alice x `par` bob y) where
  alice :: Int -> Protocol Bool
  alice x = send x >> recv >>= \z -> return z

  bob :: Int -> Protocol Bool
  bob y = recv >>= \x -> send (x == y) >> return (x == y)

test2 x y = run $ (alice x `par` bob y) where
  alice :: Int -> NProtocol Bool
  alice x = send x >> recv >>= \z -> return z

  bob :: Int -> NProtocol Bool
  bob y = recv >>= \x -> send (x == y) >> return (x == y)

test3 x y = run $ (alice x `par` bob y) where
  alice :: Int -> NProtocol Bool
  alice x = (send x `mplus` send x) >> recv >>= \z -> return z

  bob :: Int -> NProtocol Bool
  bob y = recv >>= \x -> send (x == y) >> return (x == y)
