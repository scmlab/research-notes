{-# LANGUAGE ExistentialQuantification #-}
module ProtocolT where

import Control.Applicative
import Control.Monad
import Control.Monad.Identity
import Control.Monad.Trans.Class

import Data.Dynamic

type Protocol = ProtocolT Identity
type NProtocol = ProtocolT []

data ProtocolT' m a
  = Pure a
  | Send Dynamic (ProtocolT m a)
  | Recv (Dynamic -> ProtocolT m a)
  | forall b c. Par (ProtocolT m b) (ProtocolT m c) ((b, c) -> ProtocolT m a)

newtype ProtocolT m a = ProtocolT { unProtocolT :: m (ProtocolT' m a) }

instance Monad m => Functor (ProtocolT m) where
  fmap = liftM

instance Monad m => Applicative (ProtocolT m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (ProtocolT m) where
  return = ProtocolT . return . Pure
  ProtocolT m >>= f = 
    ProtocolT $ m >>= \p ->
    case p of
      Pure x -> unProtocolT (f x)
      Send x n -> return $ Send x (n >>= f)
      Recv k -> return $ Recv ((>>= f) . k)
      Par n n' k -> return $ Par n n' ((>>= f) . k)

instance MonadTrans ProtocolT where
  lift = ProtocolT . fmap Pure

instance MonadPlus m => Alternative (ProtocolT m) where
  empty = mzero
  (<|>) = mplus

instance MonadPlus m => MonadPlus (ProtocolT m) where
  mzero = ProtocolT mzero
  ProtocolT m `mplus` ProtocolT n = ProtocolT (m `mplus` n)


run :: Monad m => ProtocolT m a -> m a
run (ProtocolT m) = m >>= run' where
  run' :: Monad m => ProtocolT' m a -> m a
  run' (Pure x)     = return x
  run' (Send x m)   = error "unmatched Send"
  run' (Recv k)     = error "unmatched Recv"
  run' (Par m m' k) = runPar m m' >>= run . k

  runPar :: Monad m => ProtocolT m b -> ProtocolT m c -> m (b, c)
  runPar (ProtocolT m) (ProtocolT m') =
    liftM2 (,) m m' >>= \(p, p') ->
    case (p, p') of
      (Send x n, Recv k) -> runPar n (k x)
      (Recv k, Send x n) -> runPar (k x) n
      _                  -> liftM2 (,) (run' p) (run' p')

wrap :: Monad m => ProtocolT' m a -> ProtocolT m a
wrap = ProtocolT . return

send :: (Monad m, Typeable a) => a -> ProtocolT m ()
send x = wrap $ Send (toDyn x) (return ())

recv :: (Monad m, Typeable a) => ProtocolT m a
recv = wrap $ Recv (\x -> return $ fromDyn x (error "wrong type"))

par :: Monad m => ProtocolT m a -> ProtocolT m b -> ProtocolT m (a, b)
par m n = wrap $ Par m n return
