{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}

module TypedProtocol where

import Prelude hiding (return, (>>=), (>>))
import Unsafe.Coerce

infixl 1  >>, >>=

data PType = TPure | TSend * PType | TRecv * PType

type family TDual t where
  TDual TPure = TPure
  TDual (TSend a t) = TRecv a (TDual t)
  TDual (TRecv a t) = TSend a (TDual t)

type family TCat t t' where
  TCat TPure t' = t'
  TCat (TSend a t) t' = TSend a (TCat t t')
  TCat (TRecv a t) t' = TRecv a (TCat t t')


data Protocol :: PType -> * -> * where
  Pure :: a -> Protocol TPure a
  Send :: b -> Protocol t a -> Protocol (TSend b t) a
  Recv :: (b -> Protocol t a) -> Protocol (TRecv b t) a
  Par :: Protocol t b -> Protocol (TDual t) c -> ((b, c) -> Protocol t' a) -> Protocol t' a

instance Functor (Protocol t) where
  fmap f m = unsafeCoerce (m >>= return . f)

return :: a -> Protocol TPure a
return = Pure

(>>=) :: Protocol t a -> (a -> Protocol t' b) -> Protocol (TCat t t') b
Pure x >>= f = f x
Send x m >>= f = Send x (m >>= f)
Recv k >>= f = Recv ((>>= f) . k)
Par m m' k >>= f = Par m m' ((>>= f) . k)

(>>) :: Protocol t a -> Protocol t' b -> Protocol (TCat t t') b
m >> n = m >>= const n


run :: Protocol TPure a -> a
run (Pure x) = x
run (Par (Send x m) (Recv k) k') = run (Par m (k x) k')
run (Par (Recv k) (Send x m) k') = run (Par (k x) m k')
run (Par (Pure x) (Pure x') k) = run (k (x, x'))

send :: a -> Protocol (TSend a TPure) ()
send x = Send x (return ())

recv :: Protocol (TRecv a TPure) a
recv = Recv return

par :: Protocol t a -> Protocol (TDual t) b -> Protocol TPure (a, b)
par m n = Par m n return
