{-# LANGUAGE ExistentialQuantification #-}
module Protocol where

import Control.Monad
import Data.Dynamic

data Protocol a
  = Pure a
  | Send Dynamic (Protocol a)
  | Recv (Dynamic -> Protocol a)
  | forall b c. Par (Protocol b) (Protocol c) ((b, c) -> Protocol a)

instance Functor Protocol where
  fmap = liftM

instance Applicative Protocol where
  pure = return
  (<*>) = ap

instance Monad Protocol where
  return = Pure
  Pure x >>= f = f x
  Send x m >>= f = Send x (m >>= f)
  Recv k >>= f = Recv ((>>= f) . k)
  Par m n k >>= f = Par m n ((>>= f) . k)


run :: Protocol a -> a
run (Pure x)     = x
run (Send x m)   = error "unmatched Send"
run (Recv k)     = error "unmatched Recv"
run (Par p p' k) = (run . k) (runPar p p') where
  runPar (Send x n) (Recv k) = runPar n (k x)
  runPar (Recv k) (Send x n) = runPar (k x) n
  runPar p p'                = (run p, run p')


send :: Typeable a => a -> Protocol ()
send x = Send (toDyn x) (return ())

recv :: Typeable a => Protocol a
recv = Recv (\x -> return $ fromDyn x (error "wrong type"))

par :: Protocol a -> Protocol b -> Protocol (a, b)
par m n = Par m n return
