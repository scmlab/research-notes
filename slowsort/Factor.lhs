\documentclass{article}

% build by
%  lhs2TeX Factor.lhs | pdflatex --jobname=Factor

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{scalerel}
\usepackage{../common/doubleequals}

%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt

%include ../common/Formatting.fmt
%include ../common/Monad.fmt

%let showproofs = True

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\todo}[1]{{\bf todo}: {#1}}

%format m1
%format m2
%format m3
%format f1
%format f2

\begin{document}

\title{Factors and Minimum}
\author{Shin-Cheng Mu}
\date{March 2019}
\maketitle

\paragraph{Intersection} Define intersection by
\begin{spec}
intersect :: m a -> m a -> m a
m1 `intersect` m2 =  m1 >>= \x1 -> m2 >>= \x2 ->
                     guard (x1 == x2) >> return x1 {-"~~."-}
\end{spec}

%format \\ = "\mathbin{\backslash}"

\paragraph{Factor}
For |f :: a -> m b|, |g :: b -> m c| and |h :: a -> m c|,
we define the right factor |h / g :: a -> m b| by
\begin{spec}
f >=> g  `sqse` h  {-"~"-}<=>{-"~"-} f `sqse` h / g {-"~~."-}
\end{spec}
\begin{spec}
f >=> g  `sqse` h  {-"~"-}<=>{-"~"-} g `sqse` f \\ h {-"~~."-}
\end{spec}

\paragraph{Membership} The membership relation:
\begin{spec}
choose :: List a -> m a
choose []      = mzero
choose (x:xs)  = return x `mplus` choose xs {-"~~."-}
\end{spec}
And its converse:
\begin{spec}
embed :: a -> m (List a)
embed x =  any >>= \xs -> return (x:xs) `mplus`
           any >>= \y -> embed x >>= \xs ->
            return (y:xs) {-"~~."-}
\end{spec}
If we use derived operators, |embed| can be defined as:
\begin{spec}
embed :: a -> m (List a)
embed x =  (x:) <$> any `mplus`
           liftM2 (:) any (embed x) {-"~~."-}
\end{spec}
Is that easier for calculation?

Furthermore, we assume a monadic program |atleast :: Int -> m Int|
such that |atleast x| non-deterministically returns a value
that is at least |x|. \todo{how to specify that formally?}

%format `min` = "\mathbin{\downarrow}"

\paragraph{Minimum} We can now define |min| to be:
\begin{spec}
minimum = choose `intersect` (embed \\ atleast)  {-"~~."-}
\end{spec}

Also define:
\begin{spec}
minlist [x]     = x
minlist (x:xs)  = x `min` minlist xs {-"~~."-}
\end{spec}
\todo{use fold on non-empty lists and use fold-fusion?}

Prove that |return . minlist `sqse` minimum|.

\begin{proof} We reason:
\begin{spec}
     return . minlist `sqse` minimum
<=>  return . minlist `sqse` choose `intersect` (embed \\ atleast)
<=>  return . minlist `sqse` choose &&
     return . minlist `sqse` embed \\ atleast {-"~~."-}
\end{spec}

For |return . minlist `sqse` choose|:
\begin{spec}
        return (minlist (x:xs))
=       return (x `min` minlist xs)
`sqse`  return x `mplus` return (minlist xs)
`sqse`    {- induction -}
        return x `mplus` choose xs
`sqse`  choose (x:xs) {-"~~."-}
\end{spec}
\todo{other cases and failed cases.}

For |return . minlist `sqse` embed \\ atleast|
\begin{spec}
     return . minlist `sqse` embed \\ atleast
<=>  embed >=> (return . minlist) `sqse` atleast {-"~~."-}
\end{spec}
From the LHS:
\begin{spec}
   embed x >>= (return . minlist)
=  (minlist . (x:)) <$> any `mplus`
   minlist <$> (liftM2 (:) any (embed x))
\end{spec}

\todo{finish this.}
\end{proof}

\paragraph{Properties of Intersection} Some basic properties that we need.
\begin{lemma}\label{lma:intersect-mplus-distr}
Intersection distributes into |mplus|. That is,
|(r `mplus` s) `intersect` t = (r `intersect` t) `mplus` (s `intersect` t)|.
\end{lemma}
\begin{proof} We reason:
\begin{spec}
  (r `mplus` s) `intersect` t
=   {- definition of |(`intersect`)| -}
  (r `mplus` s) >>= \x -> t >>= filt (x ==)
=   {- left distributivity -}
  (r >>= \x -> t >>= filt (x ==)) `mplus`
  (s >>= \x -> t >>= filt (x ==))
=   {- definition of |(`intersect`)| -}
  (r `intersect` t) `mplus` (s `intersect` t) {-"~~."-}
\end{spec}
\end{proof}


\begin{lemma}\label{lma:intersect-mono}
Intersection is monotonic. That is,
|r `sqse` s ==> (r `intersect` t) `sqse` (s `intersect` t)|.
\end{lemma}
\begin{proof} Assuming |r `sqse` s|, that is,
|r `mplus` s = s|.
\begin{spec}
  s `intersect` t
=   {- since  |r `mplus` s = s| -}
  (r `mplus` s) `intersect` t
=   {- Lemma~\ref{lma:intersect-mplus-distr} -}
  (r `intersect` t) `mplus` (s `intersect` t) {-"~~."-}
\end{spec}
Thus |(r `intersect` t) `sqse` (s `intersect` t)|.
\end{proof}

\begin{lemma}\label{lma:intersect-descend}
|s `intersect` r `sqse` r| .
\end{lemma}
\todo{how?}

\begin{lemma} |r `intersect` r = r| .
\end{lemma}
\begin{proof} With Lemma~\ref{lma:intersect-descend}
and |(`sqse`)| being anti-symmetric, we only
need to prove that |r `sqse` r `intersect` r|.
\begin{spec}
  r `mplus` (r `intersect` r)
\end{spec}
\todo We cannot use distributivity (whose proof depends on this one),
so we are stuck.
\end{proof}
\todo{confirm: is |(`sqse`)| anti-symmetric?}

\begin{lemma}\label{lma:mplus-intersect-distr}
Union distributes into intersection. That is,
|r `mplus` (s `intersect` t) = (r `mplus` s) `intersect` (r `mplus` t).|
\end{lemma}
\begin{proof} We reason:
\begin{spec}
   (r `mplus` s) `intersect` (r `mplus` t)
=    {- |(`intersect`)| distributes into |mplus| -}
   (r `intersect` (r `mplus` t)) `mplus` (s `intersect` (r `mplus` t))
=    {- |(`intersect`)| distributes into |mplus| -}
   (r `intersect` r) `mplus` (r `intersect` t) `mplus` (s `intersect` r) `mplus` (s `intersect` t)
=    {- |(`intersect`)| idempotence -}
   r `mplus` (r `intersect` t) `mplus` (s `intersect` r) `mplus` (s `intersect` t)
=    {- Lemma~\ref{lma:intersect-descend}: |r `intersect` t `sqse` r| -}
   r `mplus` (s `intersect` r) `mplus` (s `intersect` t)
=    {- Lemma~\ref{lma:intersect-descend}: |s `intersect` r `sqse` r| -}
   r `mplus` (s `intersect` t) {-"~~."-}
\end{spec}
\end{proof}

\begin{lemma}
|r `sqse` s && r `sqse` t ==> r `sqse` (s `intersect` t)|.
\end{lemma}
\begin{proof}
\begin{spec}
   r `mplus` (s `intersect` t)
=    {- Lemma~\ref{lma:mplus-intersect-distr}: |mplus| distributes into |(`intersect`)| -}
   (r `mplus` s) `intersect` (r `mplus` t)
=    {- assumption: |r `sqse` s && r `sqse` t| -}
   s `intersect` t {-"~~."-}
\end{spec}
Thus |r `sqse` (s `intersect` t)|.
\end{proof}
\end{document}
