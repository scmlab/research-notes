\documentclass{article}

% build by
%  lhs2TeX SlowSort.lhs | pdflatex --jobname=SlowSort

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{scalerel}

%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt

%include ../common/Formatting.fmt
%include ../common/Monad.fmt

%let showproofs = True

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\todo}[1]{{\bf todo}: {#1}}

%format m1
%format m2
%format m3
%format f1
%format f2

\begin{document}

\title{Non-deterministic Monads as Specifications}
\author{Shin-Cheng Mu}
\date{August 2018}
\maketitle

We work with a monad that supports non-determinism, with two
operators |mzero| and |mplus|, satisfying the following laws:

\begin{align}
  |mzero `mplus` m| & = |m| ~=~ |m `mplus` mzero| \mbox{~~,}
     \label{eq:mzero-id}\\
  |(m1 `mplus` m2) `mplus` m3| &=
     |m1 `mplus` (m2 `mplus` m3)| \mbox{~~,}
     \label{eq:mplus-assoc}\\
  |(m1 `mplus` m2) >>= f| &= |(m1 >>= f) `mplus` (m2 >>= f)| \mbox{~~,}
     \label{eq:nd-left-distr}\\
  |m >>= (\x -> f1 `mplus` f2)| &= |(m >>= f1) `mplus` (m >>= f2)| \mbox{~~.}
     \label{eq:nd-right-distr}
\end{align}

\paragraph{Refinement} We need a concept of program refinement.
Define:
\begin{spec}
  m1 `sqse` m2  {-"~"-}<=>{-"~"-} (exists n . m1 `mplus` n = m2) {-"~~."-}
\end{spec}
Apparently |sqse| is reflexive and transitive.
We need to show that refinement is preserved by monadic bind.
\begin{lemma} Refinement is preserved by |(>>=)|. That is,
\begin{enumerate}
\item |m1 `sqse` m2 {-"~"-}==>{-"~"-} m1 >>= f `sqse` m2 >>= f|.
\item |(forall x . f1 x `sqse` f2 x) {-"~"-}==>{-"~"-} m >>= f1 `sqse` m >>= f2|.
\end{enumerate}
\end{lemma}
\begin{proof}
For 1. we reason:
\begin{spec}
   m2 >>= f
=    {- since |m1 `sqse` m2|, assume |m1 `mplus` n = m2|. -}
   (m1 `mplus` n) >>= f
=    {- by \eqref{eq:nd-left-distr}  -}
   (m1 >>= f) `mplus` (n >>= f) {-"~~,"-}
\end{spec}
thus |m1 >>= f `sqse` m2 >>= f|.

For 2. we reason:
\begin{spec}
   m >>= \x -> f2 x
=    {- since |f1 x `sqse` f2 x| -}
   m >>= \x -> f1 x `mplus` n
=    {- by \eqref{eq:nd-right-distr} -}
   (m >>= f1) `mplus` (m >>= \x -> n) {-"~~,"-}
\end{spec}
thus |m >>= f1 `sqse` m >>= f2|.
\end{proof}

\paragraph{Note} Use of the symbol |sqse| might cause complaints. Historically, in refinement calculus, |p `sqse` q| denotes that |q| is more refined (more deterministic, closer to the final program, etc.) than |p|.
Unfortunately, in relational algebra, |R `sse` S| means that |R| is more deterministic.

\paragraph{Specification of Sorting} A general specification of sorting:
choose among all permutation those that are sorted:
\begin{spec}
slowsort :: Ord a => List a -> m (List a)
slowsort xs = perm xs >>= filt sorted {-"~~."-}
\end{spec}
The predicate |sorted| is defined by:
\begin{spec}
sorted :: Ord a => List a -> Bool
sorted []      = True
sorted (x:xs)  = all (x<=) xs && sorted xs {-"~~."-}
\end{spec}

There are many ways |perm| can be specified. This one would give us insertion sort:
\begin{spec}
insert :: a -> List a -> m (List a)
insert x []      = return [x]
insert x (y:xs)  = return (x:y:xs) `mplus` ((y:) <$> insert x xs) {-"~~,"-}

perm :: List a -> m (List a)
perm []      = return []
perm (x:xs)  = perm xs >>= insert x {-"~~."-}
\end{spec}
And |filt| is defined by:
\begin{spec}
filt p x  = guard (p x) >> return x {-"~~,"-}
guard b   = if b then return () else mzero {-"~~."-}
\end{spec}

Simple properties about |guard|:
\begin{align}
  |guard p `sqse` guard q| &\Leftarrow |(p <== q)| \mbox{~~,}
     \label{eq:guard-weaken}\\
  |guard (p && q)| &\equiv |guard p >> guard q| \mbox{~~.}
\end{align}

\paragraph{Insertion Sort}
Task: construct |isort| such that
\begin{spec}
  slowsort xs {-"~"-}`sqpe`{-"~"-} return (isort xs) {-"~~,"-}
\end{spec}
% where |isort| is given by:
% \begin{spec}
% isort = foldr ins [] {-"~~,"-}
%
% ins x []      =  [x]
% ins x (y:xs)  = if x <= y then x:y:xs else y : ins x xs {-"~~."-}
% \end{spec}

Consider |xs|.
\noindent{Case} |xs := []|,
\begin{spec}
   slowsort []
=  perm [] >>= filt sorted
=  return [] >>= filt sorted
=  filt sorted []
=  return []
\end{spec}

\noindent{Case} |xs := x:xs|
\begin{spec}
        slowsort (x:xs)
=       perm (x:xs) >>= filt sorted
=       (perm xs >>= insert x) >>= filt sorted
=       perm xs >>= (insert x >=> filt sorted)
`sqpe`    {- construct |ins|  -}
        perm xs >>= (filt sorted >=> (return . ins x))
=       (perm xs >>= filt sorted) >>= (return . ins x)
`sqpe`    {- assumption: |slowsort xs `sqpe` return (isort xs)|  -}
        return (isort xs) >>= (return . ins x)
=       return (ins x (isort xs)) {-"~~."-}
\end{spec}

To conclude, if we define
\begin{spec}
isort []      = []
isort (x:xs)  = ins (isort xs) {-"~~,"-}
\end{spec}
with |insert x >=> filt sorted `sqpe` filt sorted >=> (return . ins x)|,
we shall have |slowsort xs `sqpe` return (isort xs)|. The rest is
to construct |ins|.

Now we try to construct |ins|. The aim can be written as that for all |xs|,
\begin{spec}
  insert x xs >>= filt sorted {-"~"-}`sqpe`{-"~"-} ins x <$> filt sorted xs {-"~~."-}
\end{spec}

The following property can be proved in the world of pure functional programs:
\begin{align}
  |all (x<=) (y:xs) && sorted (y:xs)| &~\equiv~ |x <= y && sorted (y:xs)|
   \mbox~{~~.} \label{eq:sorted-fst-two}
\end{align}
Furthermore we need
\begin{equation}
  |(guard p >> m1) `mplus` (guard (not p) >> m2)| ~=~
     |if p then m1 else m2| \mbox{~~.} \label{eq:guard-if}
\end{equation}


Consider |xs|. For |xs := []| we can apparently choose
|ins x [] = [x]|. For |xs := y:xs| we reason:
\begin{spec}
   insert x (y:xs) >>= filt sorted
=  (return (x:y:xs) `mplus` ((y:) <$> insert x xs)) >>= filt sorted
=   {- by \eqref{eq:nd-left-distr} -}
   (return (x:y:xs) >>= filt sorted) `mplus`
   (((y:) <$> insert x xs) >>= filt sorted) {-"~~."-}
\end{spec}
Focus on the first branch:
\begin{spec}
   return (x:y:xs) >>= filt sorted
=    {- definition of |filt| and monad law -}
   guard (sorted (x:y:xs)) >> return (x:y:xs)
=    {- definition of |sorted| -}
   guard (all (x<=) (y:xs) && sorted (y:xs)) >> return (x:y:xs)
=    {- by \eqref{eq:sorted-fst-two} -}
   guard (x <= y && sorted (y:xs)) >> return (x:y:xs)
=  guard (sorted (y:xs)) >> guard (x<=y) >> return (x:y:xs) {-"~~."-}
\end{spec}
On the second branch:
\begin{spec}
        ((y:) <$> insert x xs) >>= filt sorted
=         {- since | (f <$> m) >>= k = m >>= (k . f)| -}
        insert x xs >>= (filt sorted . (y:))
=       insert x xs >>= \zs ->
        guard (sorted (y:zs)) >> return (y:zs)
=       insert x xs >>= \zs ->
        guard (sorted zs) >> guard (all (y<=) zs) >>
        return (y:zs)
=         {- definition of |filt|, monad laws -}
        (insert x xs >>= filt sorted) >>= \zs ->
        guard (all (y <=) zs) >> return (y:zs)
`sqpe`    {- induction -}
        (filt sorted xs >>= (return . ins x)) >>= \zs ->
        guard (all (y <=) zs) >> return (y:zs)
=       guard (sorted xs) >> guard (all (y <=) (ins x xs)) >> return (y:ins x xs)
=       guard (sorted xs && all (y <=) (ins x xs)) >> return (y:ins x xs)
=         {- assumption: |all (y <=) (ins x xs) <=> y <= x && all (y <=) xs| -}
        guard (sorted xs && y <= x && all (y <=) xs) >> return (y : ins x xs)
=       guard (sorted (y:xs)) >> guard (y <= x) >> return (y: ins x xs) {-"~~."-}
\end{spec}

I have to say that the assumption is a bit bold,
considering that we do not even (officially) know what |ins| is.
But anyway, combining the two branches we get:
\begin{spec}
        (return (x:y:xs) >>= filt sorted) `mplus` (((y:) <$> insert x xs) >>= filt sorted)
`sqpe`  (guard (sorted (y:xs)) >> guard (x<=y) >> return (x:y:xs)) `mplus`
        (guard (sorted (y:xs)) >> guard (y <= x) >> return (y: ins x xs))
=          {- by \eqref{eq:nd-right-distr} -}
        guard (sorted (y:xs)) >> (  (guard (x <= y) >> return (x:y:xs)) `mplus`
                                    (guard (y <= x) >> return (y: ins x xs)))
`sqpe`     {- by \eqref{eq:guard-weaken} and \eqref{eq:guard-if} -}
        guard (sorted (y:xs)) >>
          if x<=y then return (x:y:xs)) else return (y: ins x xs)
=       guard (sorted (y:xs)) >>
          return (if x<=y then x:y:xs else y : ins x xs){-"~~."-}
\end{spec}
In conclusion, we have constructed:
\begin{spec}
ins x []      = [x]
ins x (y:xs)  = if x<=y then x:y:xs else y : ins x xs {-"~~."-}
\end{spec}
We still need to confirm that |all (y <=) (ins x xs) <=> y <= x && all (y <=) xs|, but this can be omitted.

\paragraph{Fold Fusion} Apparently, both |perm| and |isort| are |foldr|,
and the derivation could be done by |foldr|-fusion.

Is this theorem true?
\begin{theorem} We have
  |h (foldr oplus e) `sqpe` foldr otimes d| if
|h e `sqpe` d| and
|h (x `oplus` m) `sqpe` x `otimes` h m|.
\end{theorem}

\paragraph{Quicksort} To derive quick sort, we use a different definition of |perm|:
\begin{spec}
split :: List a -> m (List a :* List a)
split []      =  return ([],[])
split (x:xs)  =  split xs >>= \(ys, zs) ->
                 return (x:ys, zs) `mplus` return (ys, x:zs) {-"~~,"-}

perm :: List a -> m (List a)
perm []      =  return []
perm [x]     =  return [x]
perm (x:xs)  =  split xs >>= \(ys, zs) -> liftM2 (cat3 x) (perm ys) (perm zs) {-"~~,"-}
   where cat3 x ys zs = ys ++ [x] ++ zs {-"~~."-}
\end{spec}

The aim is still to construct |qsort| such that
|slowsort xs `sqpe` return (qsort xs)|.
The derivation for the base cases are easy. The inductive case goes:
\begin{spec}
   slowsort (x:xs)
=  perm (x:xs) >>= filt sorted
=  split xs >>= \(ys, zs) ->
   liftM2 (cat3 x) (perm ys) (perm zs) >>= filt sorted
=  split xs >>= \(ys, zs) ->
   perm ys >>= \ys' -> perm zs >>= \zs' ->
   filt (sorted (cat3 ys' zs'))
=    {- by \eqref{eq:sorted-cat3} -}
   split xs >>= \(ys, zs) ->
   perm ys >>= \ys' -> perm zs >>= \zs' ->
   guard (sorted ys' && sorted zs' && all (<=x) ys' && all (x<=) zs') >>
   return (ys' ++ [x] ++ zs')
=  split xs >>= \(ys, zs) ->
   guard (all (<=x) ys && all (x<=) zs') >>
   (perm ys >>= filt sorted) >>= \ys' ->
   (perm zs >>= filt sorted) >>= \zs' ->
   return (ys' ++ [x] ++ zs')
`sqpe` {- assumption -}
   split xs >>= \(ys, zs) ->
   guard (all (<=x) ys && all (x<=) zs') >>
   return (qsort ys) >>= \ys' ->
   return (qsort zs) >>= \zs' ->
   return (ys' ++ [x] ++ zs')
`sqpe` {- construct |partition| -}
   return (partition x xs) >>= \(ys, zs) ->
   return (qsort ys ++ [x] ++ qsort zs)
=  return (let (ys,zs) = partition x xs in qsort ys ++ [x] ++ qsort zs) {-"~~."-}
\end{spec}

\begin{equation}
\begin{split}
&  |sorted (ys ++ [x] ++ zs)| ~\equiv~ \\
&\quad    |sorted ys && sorted zs && all (<=x) ys && all (x<=) zs|  \mbox{~~.}
\end{split}
    \label{eq:sorted-cat3}
\end{equation}

The specification for |partition| is:
\begin{spec}
split xs >>= filt (\(ys, zs) -> all (<=x) ys && all (x<=) zs') `sqpe`
  return (partition x xs) {-"~~."-}
\end{spec}

\end{document}
