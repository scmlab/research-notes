data State = Pair Int Int
           | Trip Int Int Int
  deriving (Show, Eq)

data Test = T1 (Int,Int) (Int,Int)
          | T2 (Int,Int,Int) (Int,Int,Int)
  deriving (Show, Eq)

outcomes :: State -> Test -> [State]
outcomes (Pair n r) (T1 (a,b) (c,d)) =
  [Trip a c (n+r-a-c),
   Pair (n-a-c) (r+a+c),
   Trip c a (n+r-a-c)]
outcomes (Trip l h r) (T2 (a,b,c) (d,e,f)) =
  [Trip a e (l+h+r-a-e),
   Trip (l-a-d) (h-b-e) (r+a+d+b+e),
   Trip d b (l+h+r-b-d)]

tests :: State -> [Test]
tests q = [t | t <- weighings q,
               better (outcomes q t) q]
 where better :: [State] -> State -> Bool
       better qs q = and [less q' q | q' <- qs]

       less :: State -> State -> Bool
       less (Pair a b)   (Pair c d)   = a < c
       less (Trip a b c) (Pair d e)   = True
       less (Trip a b c) (Trip d e f) = a + b < d + e

weighings :: State -> [Test]
weighings (Pair n r) =
  [T1 (a,b) (a+b,0) | a <- [0..n `div` 2],
                      b <- [0..min r (n-2*a)],
                      a+b /= 0]
weighings (Trip l h r) =
  [T2 (a,b,c) (d,e,f) | k <- [1..(l+h+r) `div` 2],
      (a,b,c) <- choices k (l,h,r),
      (d,e,f) <- choices k (l-a,h-b,r-c),
      0<d+e, c*f == 0, (a,b,c)<=(d,e,f)]
 where choices :: Int -> (Int,Int,Int) -> [(Int,Int,Int)]
       choices k (l,h,r) =
         [(i,j,k-i-j) | i <- [0..l],
                        j <- [max 0 (k-i-r).. min (k-i) h]]

data Tree = Stop State
          | Node Int Test [Tree]
  deriving (Show, Eq)

hgt :: Tree -> Int
hgt (Stop _)     = 0
hgt (Node h _ _) = h

node :: Test -> [Tree] -> Tree
node t ts = Node h t ts
   where h = 1 + maximum (map hgt ts)

findOne :: State -> Tree
findOne q
  | final q = Stop q
  | otherwise = best [node t (map findOne (outcomes q t))
                       | t <- tests q]

final :: State -> Bool
final (Pair n r) = n == 0
final (Trip l h r) = l + h <= 1

best :: [Tree] -> Tree
best = foldr1 better
  where better t1 t2 | hgt t1 <= hgt t2 = t1
                     | otherwise = t2

findBest :: State -> Tree
findBest q
  | final q = Stop q
  | otherwise = node t (map findBest (outcomes q t))
 where t = head (bestTests q)

bestTests :: State -> [Test]
bestTests (Pair n r) =
  [T1 (a,b) (c,d) | T1 (a,b) (c,d) <- weighings (Pair n r),
                    2*a + b <= p, n-2*a-b <= q]
 where p = 3 ^ (t-1)
       q = (p-1) `div` 2
       t = ceiling (logBase 3 (fromIntegral (2*n+k)))
       k = if r==0 then 2 else 1
bestTests (Trip l h r) =
  [T2 (a,b,c) (d,e,f) |
    T2 (a,b,c) (d,e,f) <- weighings (Trip l h r),
    (a+e) `max` (b+d) `max` (l-a-d+h-b-e) <= p]
 where p = 3 ^ (t-1)
       t = ceiling (logBase 3 (fromIntegral (l+h)))

findAllBest :: State -> [Tree]
findAllBest q
  | final q = [Stop q]
  | otherwise =
     [node t ts | t <- bestTests q,
                  ts <- cp (map findAllBest (outcomes q t))]

cp :: [[a]] -> [[a]]
cp [] = [[]]
cp (xs:xss) = [y:ys | y<-xs, ys <- yss]
   where yss = cp xss

main = do n <- read <$> getLine
          print (length $ findAllBest (Pair n 0))
