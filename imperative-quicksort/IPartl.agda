-- vim:foldmethod=marker:foldmarker=begin,∎

open import Monad
module IPartl {M : Set → Set} {{_ : Monad M}} where

open import Data.Bool as B hiding (_≤?_)
open import Data.List hiding (partition)
open import Data.List.Properties
open import Data.Product hiding (swap)
open import Data.Unit using (⊤; tt)
open import Data.Nat
open import Data.Nat.Properties
open import Relation.Binary.PropositionalEquality as P using (_≡_)

open import Refinement
open import Ord
open import Nondet {M}

private
  variable
    A B C D : Set

  # : List A → ℕ
  # = length


{-# TERMINATING #-}
ipartl : {{_ : Ord A}} {{_ : MonadArr A M}}
       → A → ℕ → (ℕ × ℕ × ℕ) → M (ℕ × ℕ)
ipartl p i (ny , nz , 0) = return (ny , nz)
ipartl p i (ny , nz , suc k) =
  read (i + ny + nz) >>= λ x →
  if x ≤ᵇ p then swap (i + ny) (i + ny + nz) >>
                 ipartl p i (ny + 1 , nz , k)
            else ipartl p i (ny , nz + 1 , k)
  where open Ord.Ord {{...}}

dispatch : {{_ : Ord A}} {{_ : MonadPlus M}}
         → A → A → (List A × List A × List A) → M (List A × List A × List A)
dispatch x p (ys , zs , xs) =
  if x ≤ᵇ p then (perm zs >>= λ zs' → return (ys ++ [ x ] , zs' , xs))
            else (perm (zs ++ [ x ]) >>= λ zs' → return (ys , zs' , xs))
  where open Ord.Ord {{...}}

partl'-to-dispatch
  : {{_ : Ord A}} {{_ : MonadPlus M}}
  → (x : A) → (p : A) → (ys,zs,xs : (List A × List A × List A))
  → let (ys , zs , xs) = ys,zs,xs
    in partl' p (ys , zs , x ∷ xs) ≈ dispatch x p (ys , zs , xs) >>= partl' p

partl'-to-dispatch x p (ys , zs , xs) =
  begin
    partl' p (ys , zs , x ∷ xs)
  ≈⟨ if-cong (>>=-cong refl (λ _ → sym >>=-identity-l)) (>>=-cong refl (λ _ → sym >>=-identity-l)) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → return (ys ++ [ x ] , zs' , xs) >>= partl' p)
               else (perm (zs ++ [ x ]) >>= λ zs' → return (ys , zs' , xs) >>= partl' p))
  ≈⟨ if-cong (sym >>=-assoc) (sym >>=-assoc) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → return (ys ++ [ x ] , zs' , xs)) >>= partl' p
               else (perm (zs ++ [ x ]) >>= λ zs' → return (ys , zs' , xs)) >>= partl' p)
  ≈⟨ sym push->>=-into-if-l ⟩
    dispatch x p (ys , zs , xs) >>= partl' p
  ∎
  where open ≈-Reasoning
        open Ord.Ord {{...}} using (_≤ᵇ_)


{-# TERMINATING #-}
ipartl-spec : {{_ : Ord A}} {{_ : MonadPlus M}} {{_ : MonadArr A M}}
            → (i : ℕ) (p : A)
            → write3L i >=> ipartl p i ⊑ partl' p >=> write2L i

ipartl-spec i p (ys , zs , []) =
  begin
    partl' p (ys , zs , []) >>= write2L i
  ≈⟨ >>=-identity-l ⟩
    writeList i (ys ++ zs) >> return (# ys , # zs)
  ≡⟨ P.cong₂ _>>_ (P.cong (writeList i) (P.cong (ys ++_) (P.sym (++-identityʳ zs)))) P.refl ⟩
    writeList i (ys ++ zs ++ []) >> return (# ys , # zs)
  ≈⟨ >>-cong refl (sym >>=-identity-l) ⟩
    writeList i (ys ++ zs ++ []) >> (return (# ys , # zs , 0) >>= ipartl p i)
  ≈⟨ sym >>=-assoc ⟩
    write3L i (ys , zs , []) >>= ipartl p i
  ∎
  where open ⊇-Reasoning

ipartl-spec {A} i p (ys , zs , x ∷ xs) =
  begin
    partl' p (ys , zs , x ∷ xs) >>= write2L i
  ≈⟨ >>=-cong (partl'-to-dispatch x p (ys , zs , xs)) (λ _ → refl) ⟩
    (dispatch x p (ys , zs , xs) >>= partl' p) >>= write2L i
  ≈⟨ >>=-assoc ⟩
    dispatch x p (ys , zs , xs) >>= (partl' p >=> write2L i)
  ⊇⟨ >>=-mono ⊇-refl (ipartl-spec i p) ⟩
    dispatch x p (ys , zs , xs) >>= (write3L i >=> ipartl p i)
  ≈⟨ >>=-cong refl (λ _ → trans >>=-assoc (>>=-cong refl (λ _ → >>=-identity-l))) ⟩
    dispatch x p (ys , zs , xs) >>= (λ { (ys' , zs' , xs) →
    writeList i (ys' ++ zs' ++ xs) >>
    ipartl p i (# ys' , # zs' , # xs) })
  ≈⟨ >>=-cong refl (λ { (ys' , zs' , xs) →
     >>-cong (reflexive (P.cong (writeList i) (P.sym (++-assoc ys' zs' xs)))) refl }) ⟩
    dispatch x p (ys , zs , xs) >>= (λ { (ys' , zs' , xs) →
    writeList i ((ys' ++ zs') ++ xs) >>
    ipartl p i (# ys' , # zs' , # xs) })
  ≈⟨ >>=-cong refl (λ { (ys' , zs' , xs) →
     >>-cong (writeList-++ (ys' ++ zs') xs) refl }) ⟩
    dispatch x p (ys , zs , xs) >>= (λ { (ys' , zs' , xs) →
    writeList i (ys' ++ zs') >>
    writeList (i + # (ys' ++ zs')) xs >>
    ipartl p i (# ys' , # zs' , # xs) })
  ≈⟨ >>=-cong refl (λ { (ys' , zs' , xs) →
     >>-cong (writeList-writeList-comm' (ys' ++ zs') xs ≤-refl) refl }) ⟩
    dispatch x p (ys , zs , xs) >>= (λ { (ys' , zs' , xs) →
    writeList (i + # (ys' ++ zs')) xs >>
    writeList i (ys' ++ zs') >>
    ipartl p i (# ys' , # zs' , # xs) })
  ≈⟨ >>=-cong refl (λ { (ys' , zs' , xs) →
     >>-cong (>>-cong (reflexive (P.cong (λ j → writeList j xs) (P.cong (i +_) (length-++ ys' {zs'})))) refl) refl }) ⟩
    dispatch x p (ys , zs , xs) >>= (λ { (ys' , zs' , xs) →
    writeList (i + (# ys' + # zs')) xs >>
    writeList i (ys' ++ zs') >>
    ipartl p i (# ys' , # zs' , # xs) })
  ≈⟨ push->>=-into-if-l ⟩
    (let f = λ { (ys' , zs' , xs) →
                 writeList (i + (# ys' + # zs')) xs >>
                 writeList i (ys' ++ zs') >>
                 ipartl p i (# ys' , # zs' , # xs) }
     in if x ≤ᵇ p then (perm zs >>= λ zs' → return (ys ++ [ x ] , zs' , xs)) >>= f
                  else (perm (zs ++ [ x ]) >>= λ zs' → return (ys , zs' , xs)) >>= f)
  ≈⟨ if-cong >>=-assoc >>=-assoc ⟩
    (let f = λ { (ys' , zs' , xs) →
                 writeList (i + (# ys' + # zs')) xs >>
                 writeList i (ys' ++ zs') >>
                 ipartl p i (# ys' , # zs' , # xs) }
     in if x ≤ᵇ p then (perm zs >>= λ zs' → return (ys ++ [ x ] , zs' , xs) >>= f)
                  else (perm (zs ++ [ x ]) >>= λ zs' → return (ys , zs' , xs) >>= f))
  ≈⟨ if-cong (>>=-cong refl (λ _ → >>=-identity-l)) (>>=-cong refl (λ _ → >>=-identity-l)) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → 
                     writeList (i + (# (ys ++ [ x ]) + # zs')) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# (ys ++ [ x ]) , # zs' , # xs))
               else (perm (zs ++ [ x ]) >>= λ zs' →
                     writeList (i + (# ys + # zs')) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , # zs' , # xs)))
  ≈⟨ if-cong (>>=-cong refl (λ _ → sym >>=-identity-l)) (>>=-cong refl (λ _ → sym >>=-identity-l)) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → return (zs' , # zs') >>= λ { (zs' , #zs') →
                     writeList (i + (# (ys ++ [ x ]) + #zs')) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# (ys ++ [ x ]) , #zs' , # xs) })
               else (perm (zs ++ [ x ]) >>= λ zs' → return (zs' , # zs') >>= λ { (zs' , #zs') →
                     writeList (i + (# ys + #zs')) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , #zs' , # xs)}))
  ≈⟨ if-cong (sym >>=-assoc) (sym >>=-assoc) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → return (zs' , # zs')) >>= (λ { (zs' , #zs') →
                     writeList (i + (# (ys ++ [ x ]) + #zs')) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# (ys ++ [ x ]) , #zs' , # xs) })
               else (perm (zs ++ [ x ]) >>= λ zs' → return (zs' , # zs')) >>= (λ { (zs' , #zs') →
                     writeList (i + (# ys + #zs')) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , #zs' , # xs)}))
  ≈⟨ if-cong (>>=-cong (perm-preserves-length zs) (λ _ → refl))
             (>>=-cong (perm-preserves-length (zs ++ [ x ])) (λ _ → refl)) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → return (zs' , # zs)) >>= (λ { (zs' , #zs') →
                     writeList (i + (# (ys ++ [ x ]) + #zs')) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# (ys ++ [ x ]) , #zs' , # xs) })
               else (perm (zs ++ [ x ]) >>= λ zs' → return (zs' , # (zs ++ [ x ]))) >>= (λ { (zs' , #zs') →
                     writeList (i + (# ys + #zs')) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , #zs' , # xs)}))
  ≈⟨ if-cong >>=-assoc >>=-assoc ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' → return (zs' , # zs) >>= λ { (zs' , #zs') →
                     writeList (i + (# (ys ++ [ x ]) + #zs')) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# (ys ++ [ x ]) , #zs' , # xs) })
               else (perm (zs ++ [ x ]) >>= λ zs' → return (zs' , # (zs ++ [ x ])) >>= λ { (zs' , #zs') →
                     writeList (i + (# ys + #zs')) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , #zs' , # xs)}))
  ≈⟨ if-cong (>>=-cong refl (λ _ → >>=-identity-l)) (>>=-cong refl (λ _ → >>=-identity-l)) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' →
                     writeList (i + (# (ys ++ [ x ]) + # zs)) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# (ys ++ [ x ]) , # zs , # xs))
               else (perm (zs ++ [ x ]) >>= λ zs' →
                     writeList (i + (# ys + # (zs ++ [ x ]))) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , # (zs ++ [ x ]) , # xs)))
  ≈⟨ if-cong (>>=-cong refl (λ zs' → reflexive (P.cong (λ #ys+1 →
                writeList (i + (#ys+1 + # zs)) xs >>
                writeList i ((ys ++ [ x ]) ++ zs') >>
                ipartl p i (#ys+1 , # zs , # xs)) (length-++ ys))))
             (>>=-cong refl (λ zs' → reflexive (P.cong (λ #zs+1 → 
                writeList (i + (# ys + #zs+1)) xs >>
                writeList i (ys ++ zs') >>
                ipartl p i (# ys , #zs+1 , # xs)) (length-++ zs) ))) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' →
                     writeList (i + ((# ys + 1) + # zs)) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# ys + 1 , # zs , # xs))
               else (perm (zs ++ [ x ]) >>= λ zs' →
                     writeList (i + (# ys + (# zs + 1))) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ if-cong {_} {_} {x ≤ᵇ p}
       (>>=-cong refl (λ zs' → reflexive (P.cong (λ i+#ys+#zs+1 →
        writeList i+#ys+#zs+1 xs >>
        writeList i ((ys ++ [ x ]) ++ zs') >>
        ipartl p i (# ys + 1 , # zs , # xs)) (+-lemma₁ i (# ys) (# zs))))) refl ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' →
                     writeList (i + (# ys + (# zs + 1))) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs') >>
                     ipartl p i (# ys + 1 , # zs , # xs))
               else (perm (zs ++ [ x ]) >>= λ zs' →
                     writeList (i + (# ys + (# zs + 1))) xs >>
                     writeList i (ys ++ zs') >>
                     ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ if-cong (sym >>=-assoc) (sym >>=-assoc) ⟩
    (if x ≤ᵇ p then (perm zs >>= λ zs' →
                     writeList (i + (# ys + (# zs + 1))) xs >>
                     writeList i ((ys ++ [ x ]) ++ zs')) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else (perm (zs ++ [ x ]) >>= λ zs' →
                     writeList (i + (# ys + (# zs + 1))) xs >>
                     writeList i (ys ++ zs')) >>
                    ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ if-cong (>>-cong (mpo-comm (mpo-perm zs)) refl) (>>-cong (mpo-comm (mpo-perm (zs ++ [ x ]))) refl) ⟩
    (if x ≤ᵇ p then writeList (i + (# ys + (# zs + 1))) xs >>
                    (perm zs >>= λ zs' → writeList i ((ys ++ [ x ]) ++ zs')) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else writeList (i + (# ys + (# zs + 1))) xs >>
                    (perm (zs ++ [ x ]) >>= λ zs' → writeList i (ys ++ zs')) >>
                    ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ if-cong >>=-assoc >>=-assoc ⟩
    (if x ≤ᵇ p then writeList (i + (# ys + (# zs + 1))) xs >>
                    ((perm zs >>= λ zs' → writeList i ((ys ++ [ x ]) ++ zs')) >>
                     ipartl p i (# ys + 1 , # zs , # xs))
               else writeList (i + (# ys + (# zs + 1))) xs >>
                    ((perm (zs ++ [ x ]) >>= λ zs' → writeList i (ys ++ zs')) >>
                     ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ sym push->>=-into-if-r ⟩
    writeList (i + (# ys + (# zs + 1))) xs >>
    (if x ≤ᵇ p then (perm zs >>= λ zs' → writeList i ((ys ++ [ x ]) ++ zs')) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else (perm (zs ++ [ x ]) >>= λ zs' → writeList i (ys ++ zs')) >>
                    ipartl p i (# ys , # zs + 1 , # xs))
  ⊇⟨ >>-mono ⊇-refl (if-mono (>>-mono first-branch ⊇-refl) (>>-mono second-branch ⊇-refl)) ⟩
    writeList (i + (# ys + (# zs + 1))) xs >>
    (if x ≤ᵇ p then writeList i (ys ++ zs ++ [ x ]) >>
                    swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else writeList i (ys ++ zs ++ [ x ]) >>
                    ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong refl (if-cong {M} {_} {x ≤ᵇ p} >>-assoc refl) ⟩
    writeList (i + (# ys + (# zs + 1))) xs >>
    (if x ≤ᵇ p then writeList i (ys ++ zs ++ [ x ]) >>
                    (swap (i + # ys) (i + # ys + # zs) >>
                     ipartl p i (# ys + 1 , # zs , # xs))
               else writeList i (ys ++ zs ++ [ x ]) >>
                    ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong refl (sym push->>=-into-if-r) ⟩
    writeList (i + (# ys + (# zs + 1))) xs >>
    (writeList i (ys ++ zs ++ [ x ]) >>
     (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                     ipartl p i (# ys + 1 , # zs , # xs)
                else ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ sym >>=-assoc ⟩
    writeList (i + (# ys + (# zs + 1))) xs >> writeList i (ys ++ zs ++ [ x ]) >>
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong (>>-cong (reflexive (P.cong (λ j → writeList (i + j) xs) (P.sym (length-++-++ ys zs [ x ])))) refl) refl ⟩
    writeList (i + # (ys ++ zs ++ [ x ])) xs >> writeList i (ys ++ zs ++ [ x ]) >>
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong (sym (writeList-writeList-comm' (ys ++ zs ++ [ x ]) xs ≤-refl)) refl ⟩
    writeList i (ys ++ zs ++ [ x ]) >> writeList (i + # (ys ++ zs ++ [ x ])) xs >>
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong (sym (writeList-++ (ys ++ zs ++ [ x ]) xs)) refl ⟩
    writeList i ((ys ++ zs ++ [ x ]) ++ xs) >>
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong (reflexive (P.cong (writeList i) (++-lemma₁ ys zs x xs))) refl ⟩
    writeList i (ys ++ zs ++ (x ∷ xs)) >>
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs))
  ≈⟨ >>-cong refl (sym >>=-identity-l) ⟩
    writeList i (ys ++ zs ++ (x ∷ xs)) >> (return x >>= λ x →
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ sym >>=-assoc ⟩
    (writeList i (ys ++ zs ++ (x ∷ xs)) >> return x) >>= (λ x →
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ >>=-cong introduce-read (λ _ → refl) ⟩
    (writeList i (ys ++ zs ++ (x ∷ xs)) >> read (i + # ys + # zs)) >>= (λ x →
    (if x ≤ᵇ p then swap (i + # ys) (i + # ys + # zs) >>
                    ipartl p i (# ys + 1 , # zs , # xs)
               else ipartl p i (# ys , # zs + 1 , # xs)))
  ≈⟨ >>=-assoc ⟩
    writeList i (ys ++ zs ++ (x ∷ xs)) >> ipartl p i (# ys , # zs , # (x ∷ xs))
  ≈⟨ >>=-cong refl (λ _ → sym >>=-identity-l) ⟩
    writeList i (ys ++ zs ++ (x ∷ xs)) >> (return (# ys , # zs , # (x ∷ xs)) >>= ipartl p i)
  ≈⟨ sym >>=-assoc ⟩
    write3L i (ys , zs , x ∷ xs) >>= ipartl p i
  ∎
  where
    open Ord.Ord {{...}} using (_≤ᵇ_)

    postulate
      introduce-read : writeList i (ys ++ zs ++ (x ∷ xs)) >> return x ≈
                       writeList i (ys ++ zs ++ (x ∷ xs)) >> read (i + # ys + # zs)
      introduce-swap
        : ∀ {j} → (perm zs >>= λ zs' → writeList j ([ x ] ++ zs')) ⊇
                  writeList j (zs ++ [ x ]) >> swap j (j + # zs)

    +-lemma₁ : (a b c : ℕ) → a + ((b + 1) + c) ≡ a + (b + (c + 1))
    +-lemma₁ a b c =
      begin
        a + ((b + 1) + c)
      ≡⟨ P.cong (a +_) (+-assoc b 1 c) ⟩
        a + (b + (1 + c))
      ≡⟨ P.cong (a +_) (P.cong (b +_) (+-comm 1 c)) ⟩
        a + (b + (c + 1))
      ∎
      where open P.≡-Reasoning

    length-++-++ : (xs ys zs : List A) → length (xs ++ ys ++ zs) ≡ length xs + (length ys + length zs)
    length-++-++ xs ys zs =
      begin
        length (xs ++ ys ++ zs)
      ≡⟨ length-++ xs ⟩
        length xs + length (ys ++ zs)
      ≡⟨ P.cong (length xs +_) (length-++ ys) ⟩
        length xs + (length ys + length zs)
      ∎
      where open P.≡-Reasoning

    ++-lemma₁ : (ys zs : List A) (x : A) (xs : List A) → ((ys ++ zs ++ [ x ]) ++ xs) ≡ (ys ++ zs ++ (x ∷ xs))
    ++-lemma₁ ys zs x xs =
      begin
        (ys ++ zs ++ [ x ]) ++ xs
      ≡⟨ ++-assoc ys (zs ++ [ x ]) xs ⟩
        ys ++ ((zs ++ [ x ]) ++ xs)
      ≡⟨ P.cong (ys ++_) (++-assoc zs [ x ] xs) ⟩
        ys ++ zs ++ (x ∷ xs)
      ∎
      where open P.≡-Reasoning

    second-branch : (perm (zs ++ [ x ]) >>= λ zs' → writeList i (ys ++ zs')) ⊇
                    writeList i (ys ++ zs ++ [ x ])
    second-branch =
      begin
        (perm (zs ++ [ x ]) >>= λ zs' → writeList i (ys ++ zs'))
      ⊇⟨ >>=-mono (return⊑perm (zs ++ [ x ])) (λ _ → ⊇-refl) ⟩
        (return (zs ++ [ x ]) >>= λ zs' → writeList i (ys ++ zs'))
      ≈⟨ >>=-identity-l ⟩
        writeList i (ys ++ zs ++ [ x ])
      ∎
      where open ⊇-Reasoning

    first-branch : (perm zs >>= λ zs' → writeList i ((ys ++ [ x ]) ++ zs')) ⊇
                   writeList i (ys ++ zs ++ [ x ]) >> swap (i + # ys) (i + # ys + # zs)
    first-branch =
      begin
        (perm zs >>= λ zs' → writeList i ((ys ++ [ x ]) ++ zs'))
      ≈⟨ >>=-cong refl (λ zs' → reflexive (P.cong (writeList i) (++-assoc ys ([ x ]) zs'))) ⟩
        (perm zs >>= λ zs' → writeList i (ys ++ ([ x ] ++ zs')))
      ≈⟨ >>=-cong refl (λ zs' → writeList-++ ys ([ x ] ++ zs')) ⟩
        (perm zs >>= λ zs' → writeList i ys >> writeList (i + # ys) ([ x ] ++ zs'))
      ≈⟨ mpo-comm (mpo-perm zs) ⟩
        writeList i ys >> (perm zs >>= λ zs' → writeList (i + # ys) ([ x ] ++ zs'))
      ⊇⟨ >>-mono ⊇-refl introduce-swap ⟩
        writeList i ys >> (writeList (i + # ys) (zs ++ [ x ]) >> swap (i + # ys) (i + # ys + # zs))
      ≈⟨ sym >>-assoc ⟩
        (writeList i ys >> writeList (i + # ys) (zs ++ [ x ])) >> swap (i + # ys) (i + # ys + # zs)
      ≈⟨ >>-cong (sym (writeList-++ ys (zs ++ [ x ]))) refl ⟩
        writeList i (ys ++ zs ++ [ x ]) >> swap (i + # ys) (i + # ys + # zs)
      ∎
      where open ⊇-Reasoning

    open ⊇-Reasoning
