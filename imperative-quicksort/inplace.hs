-- from nondet.hs
partl' p (ys, zs) [] = return (ys, zs)
partl' p (ys, zs) (x:xs) =
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  partl' p (ys', zs') xs

slowsort [] = return []
slowsort (x:xs) =
  partl' x ([], []) xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' ->
  slowsort zs >>= \zs' ->
  return (ys' ++ [x] ++ zs')

-- axiom
if m contains only nondeterminism
then m commutes with (read i), (write i x)

-- lemma
if m contains only nondeterminism
then m commutes with (readList i k), (writeList i xs)


-- Goal: find ipartl s.t.
-- (idea: store ys, zs, xs consecutively)
  partl' p (ys, zs) xs >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
⊒ writeList i (ys++zs++xs) >>
  ipartl p i (length ys, length zs) (length xs)

-- then
  readList i k >>= \xs ->
  partl' p ([], []) xs >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
⊒ readList i k >>= \xs -> writeList i ([]++[]++xs) >>
  ipartl p i (length [], length []) (length xs)
= ipartl p i (0, 0) (length xs)

-- lemma
  writeList i ([x]++xs++[x'])
= writeList i ([x']++xs++[x]) >> swap i (i+1+length xs)

-- proof {{{
  writeList i ([x]++xs++[x'])
= {- writeList-++ -}
  write i x >> writeList (i+1) xs >> write (i+1+length xs) x'
= {- commute -}
  writeList (i+1) xs >> write i x >> write (i+1+length xs) x'
= {- write-swap -}
  writeList (i+1) xs >> write i x' >> write (i+1+length xs) x >> swap i (i+1+length xs)
= {- commute -}
  write i x' >> writeList (i+1) xs >> write (i+1+length xs) x >> swap i (i+1+length xs)
= {- writeList-++ -}
  writeList i ([x']++xs++[x]) >> swap i (i+1+length xs)
-- }}}

-- lemma
return (xs++[x]) ⊑ perm (x:xs)

-- TODO proof

-- lemma
  perm zs >>= \zs' -> writeList i ([x]++zs')
⊒ writeList i (zs++[x]) >> swap i (i+length zs)

-- proof {{{
-- case zs := []
  perm [] >>= \zs' -> writeList i ([x]++zs')
= writeList i (x:[])
= writeList i ([]++[x]) >> swap i i

-- case zs := (z:zs)
  perm (z:zs) >>= \zs' -> writeList i ([x]++zs')
⊒ return (zs++[z]) >>= \zs' -> writeList i ([x]++zs')
= writeList i ([x]++zs++[z])
= writeList i ([z]++zs++[x]) >> swap i (i+length zs+1)
= writeList i ((z:zs)++[x]) >> swap i (i+length (x:zs)
-- }}}


-- derivation ipartl {{{
-- case xs := []
  partl' p (ys, zs) [] >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
= {- def partl' -}
  return (ys, zs) >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
= {- return x >>= f = f x -}
  writeList i (ys++zs++[]) >> return (length ys, length zs)

-- case xs := (x:xs)
  partl' p (ys, zs) (x:xs) >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
= {- def partl' -}
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  partl' p (ys', zs') xs >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
⊒ {- assumption -}
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  writeList i (ys'++zs'++xs) >>
  ipartl p i (length ys', length zs') (length xs)
= {- writeList-++ -}
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  writeList i (ys'++zs') >> writeList (i+length (ys'++zs')) xs >>
  ipartl p i (length ys', length zs') (length xs)
= {- perm preserves length, commutativity -}
  writeList (i+length ys+length zs+1) xs >>
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  writeList i (ys'++zs') >>
  ipartl p i (length ys', length zs') (length xs)
= {- dist -}
  writeList (i+length ys+length zs+1) xs >>
  mplus (guard (x<=p) >> perm2 (ys++[x], zs) >>= \(ys', zs') ->
         writeList i (ys'++zs') >> ipartl p i (length ys', length zs') (length xs))
        (guard (p<=x) >> perm2 (ys, zs++[x]) >>= \(ys', zs') ->
         writeList i (ys'++zs') >> ipartl p i (length ys', length zs') (length xs))
= {- perm preserves length -}
  writeList (i+length ys+length zs+1) xs >>
  mplus (guard (x<=p) >>
         perm2 (ys++[x], zs) >>= \(ys', zs') -> writeList i (ys'++zs') >>
         ipartl p i (length ys+1, length zs) (length xs))
        (guard (p<=x) >>
         perm2 (ys, zs++[x]) >>= \(ys', zs') -> writeList i (ys'++zs') >>
         ipartl p i (length ys, length zs+1) (length xs))

-- Goal: find m1, m2 s.t.
1.   perm2 (ys++[x], zs) >>= \(ys', zs') -> writeList i (ys'++zs')
   ⊒ writeList i (ys++zs++[x]) >> m1

2.   perm2 ([x], zs++[x]) >>= \(ys', zs') -> writeList i (ys'++zs')
   ⊒ writeList i (ys++zs++[x]) >> m2

-- first branch {{{
  perm2 (ys++[x], zs) >>= \(ys', zs') -> writeList i (ys'++zs')
= {- def perm2 -}
  perm (ys++[x]) >>= \ys' -> perm zs >>= \zs' -> writeList i (ys'++zs')
⊒ {- return xs ⊑ perm xs -}
  return (ys++[x]) >>= \ys' -> perm zs >>= \zs' -> writeList i (ys'++zs')
= {- return x >>= f = f x -}
  perm zs >>= \zs' -> writeList i (ys++[x]++zs')
= {- writeList-++, commute -}
  writeList i ys >> perm zs >>= \zs' -> writeList (i+length ys) ([x]++zs')
⊒ {- lemma -}
  writeList i ys >> writeList (i+length ys) (zs++[x]) >> swap (i+length ys) (i+length ys+length zs)
= {- writeList-++ -}
  writeList i (ys++zs++[x]) >> swap (i+length ys) (i+length ys+length zs)
-- }}}

-- second branch {{{
  perm2 (ys, zs++[x]) >>= \(ys', zs') -> writeList i (ys'++zs')
⊒ return (ys, zs++[x]) >>= \(ys', zs') -> writeList i (ys'++zs')
= writeList i (ys++zs++[x])
-- }}}

= {- dist, writeList-++ -}
  writeList i (ys++zs++(x:xs)) >>
  mplus (guard (x<=p) >>
         swap (i+length ys) (i+length ys+length zs) >>
         ipartl p i (length ys+1, length zs) (length xs))
        (guard (p<=x) >>
         ipartl p i (length ys, length zs+1) (length xs))
= {- write-read -}
  writeList i (ys++zs++(x:xs)) >>
  read (i+length ys+length zs) >>= \x ->
  mplus (guard (x<=p) >>
         swap (i+length ys) (i+length ys+length zs) >>
         ipartl p i (length ys+1, length zs) (length xs))
        (guard (p<=x) >>
         ipartl p i (length ys, length zs+1) (length xs))
⊒ writeList i (ys++zs++(x:xs)) >>
  read (i+length ys+length zs) >>= \x ->
  if x<p then swap (i+length ys) (i+length ys+length zs) >>
              ipartl p i (length ys+1, length zs) (length xs)
         else ipartl p i (length ys, length zs+1) (length xs)
-- }}}

-- def
ipartl p i (ny, nz) 0 = return (ny, nz)
ipartl p i (ny, nz) (1+k) =
  read (i+ny+nz) >>= \x ->
  if x<p then swap (i+ny) (i+ny+nz) >> ipartl p i (ny+1, nz) k
         else ipartl p i (ny, nz+1) k


-- Goal: find iqsort s.t.
  slowsort xs >>= writeList i
⊒ writeList i xs >> iqsort i (length xs)

-- then
  readList i k >>= slowsort >>= writeList i
= readList i k >>= writeList i >> iqsort i k
= iqsort i k

-- lemma
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  writeList i (ys'++[x]++zs')
⊒ writeList i (ys++[x]++zs) >>
  iqsort i (length ys) >> iqsort (i+length ys+1) (length zs)

-- proof {{{
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  writeList i (ys'++[x]++zs')
= slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  writeList i ys' >> write (i+length ys) x >> writeList (i+length ys+1) zs'
= {- commutativity -}
  write (i+length ys) x >>
  slowsort ys >>= \ys' -> writeList i ys' >>
  slowsort zs >>= \zs' -> writeList (i+length ys+1) zs'
⊒ {- assumption -}
  write (i+length ys) x >>
  slowsort ys >>= \ys' -> writeList i ys' >>
  writeList (i+length ys+1) zs >> iqsort (i+length ys+1) (length zs)
= {- commutativity -}
  write (i+length ys) x >> writeList (i+length ys+1) zs
  slowsort ys >>= \ys' -> writeList i ys' >>
  iqsort (i+length ys+1) (length zs)
⊒ {- assumption -}
  write (i+length ys) x >> writeList (i+length ys+1) zs
  writeList i ys >> iqsort i (length ys)
  iqsort (i+length ys+1) (length zs)
= {- commutativity, writeList-++ -}
  writeList i (ys++[x]++zs) >> iqsort i (length ys) >> iqsort (i+length ys+1 (length zs)
-- }}}

-- lemma
return (x:xs) ⊑ perm (xs++[x])

-- TODO proof

-- lemma
  perm ys >>= \ys' -> writeList i (ys'++[x]++zs)
⊒ writeList i ([x]++ys++zs) >> swap i (i+length ys)

-- proof {{{
-- case ys := []
  perm [] >>= \ys' -> writeList i (ys'++[x]++zs)
= return [] >>= \ys' -> writeList i (ys'++[x]++zs)
= writeList i ([]++[x]++zs)
= writeList i ([x]++[]++zs)
= writeList i ([x]++[]++zs) >> swap i i
= writeList i ([x]++[]++zs) >> swap i (i+length [])

-- case ys := ys++[y]
  perm (ys++[y]) >>= \ys' -> writeList i (ys'++[x]++zs)
⊒ return (y:ys) >>= \ys' -> writeList i (ys'++[x]++zs)
= writeList i ([y]++ys++[x]++zs)
⊒ writeList i ([x]++ys++[y]++zs) >> swap i (i+length (ys++[y]))
-- }}}


-- derivation iqsort {{{
  slowsort [] >>= writeList i
= writeList i []
= writeList i [] >> return ()

  slowsort (x:xs) >>= writeList i
= {- def slowsort, return x >>= f = f x -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  writeList i (ys'++[x]++zs')
= {- perm xs >>= slowsort = slowsort xs -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> slowsort ys' >>= \ys'' ->
  slowsort zs >>= \zs' ->
  writeList i (ys''++[x]++zs')
= {- lemma -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> writeList i (ys'++[x]++zs) >>
  iqsort i (length ys') >> iqsort (i+length ys+1) (length zs)
= {- perm preserves length -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> writeList i (ys'++[x]++zs) >>
  iqsort i (length ys) >> iqsort (i+length ys+1) (length zs)
= {- lemma -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  writeList i ([x]++ys++zs)) >> swap i (i+length ys) >>
  iqsort i (length ys) >> iqsort (i+length ys+1) (length zs)
= {- def writeList -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  write i x >> write (i+1) (ys++zs) >> swap i (i+length ys) >>
  iqsort i (length ys) >> iqsort (i+length ys+1) (length zs)
= {- commutativity -}
  write i x >>
  partl' x ([], []) xs >>= \(ys, zs) -> write (i+1) (ys++zs) >>
  swap i (i+length ys) >>
  iqsort i (length ys) >> iqsort (i+length ys+1) (length zs)
= {- return x >>= f = f x -}
  write i x >>
  partl' x ([], []) xs >>= \(ys, zs) -> write (i+1) (ys++zs) >>
  return (length ys, length zs) >>= \(ny, nz) ->
  swap i (i+ny) >>
  iqsort i ny >> iqsort (i+ny+1) nz
= {- construction ipartl -}
  write i x >> writeList (i+1) xs >>
  ipartl x (i+1) (0, 0) (length xs) >>= \(ny, nz) ->
  swap i (i+ny) >>
  iqsort i ny >> iqsort (i+ny+1) nz
= {- def writeList -}
  writeList i (x:xs) >>
  ipartl x (i+1) (0, 0) (length xs) >>= \(ny, nz) ->
  swap i (i+ny) >>
  iqsort i ny >> iqsort (i+ny+1) nz
= {- write-read -}
  writeList i (x:xs) >>
  read i >>= \x ->
  ipartl x (i+1) (0, 0) (length xs) >>= \(ny, nz) ->
  swap i (i+ny) >>
  iqsort i ny >> iqsort (i+ny+1) nz
-- }}}

-- def
iqsort i 0 = return ()
iqsort i (1+k) =
  read i >>= \x ->
  ipartl x (i+1) (0, 0) k >>= \(ny, nz) ->
  swap i (i+ny) >>
  iqsort i ny >> iqsort (i+ny+1) nz

-- vim:foldmethod=marker:
