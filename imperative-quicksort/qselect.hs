-- from nondet.hs
partition p xs >>= perm2 = partl' p ([], []) xs

-- from inplace.hs
  partl' p (ys, zs) xs >>= \(ys', zs') ->
  writeList i (ys'++zs') >> return (length ys', length zs')
⊒ writeList i (ys++zs++xs) >>
  ipartl p i (length ys, length zs) (length xs)

-- def
qselect xs n = (!!n) <$> slowsort xs

-- lemma
qselect []     n  = mzero
qselect (x:xs) n  =
  partl' x ([], []) xs >>= \(ys, zs) ->
  if length ys > n then qselect ys n
  else if length ys == n then return x
  else qselect zs (n - length ys - 1)

-- TODO proof

-- axiom
wipe :: m ()
-- (write-wipe)
write i x >> wipe = wipe

-- lemma (writeList-wipe)
writeList i xs >> wipe = wipe


-- def
wipeRet x = wipe >> return x

-- lemma (writeList-wipeRet)
writeList i xs >> wipeRet x = wipeRet x

-- proof {{{
  writeList i xs >> wipeRet x
= {- def wipeRet -}
  writeList i xs >> wipe >> return x
= {- writeList-wipe -}
  wipe >> return x
= {- def wipeRet -}
  wipeRet x
-- }}}


-- Goal: find iqselect s.t. (for any i)
  qselect xs n >>= wipeRet
⊒ writeList i xs >> iqselect i (length xs) n >>= wipeRet

-- lemma
  return x >>= wipeRet
= writeList i xs >> return x >>= wipeRet

-- proof {{{
  return x >>= wipeRet
= {- return x >>= f = f x -}
  wipeRet x
= {- writeList-wipeRet -}
  writeList i xs >> wipeRet x
= {- return x >>= f = f x -}
  writeList i xs >> return x >>= wipeRet
-- }}}

-- lemma
  qselect xs n >>= wipeRet
⊒ writeList i (ys++xs++zs) >> iqselect (i+length ys) (length xs) n >>= wipeRet

-- proof {{{
  qselect xs n >>= wipeRet
= {- apply writeList-wipeRet twice -}
  qselect xs n >>= \x ->
  writeList i ys >> writeList (i+length ys+length xs) zs >> wipeRet x
= {- commutativity -}
  writeList i ys >> writeList (i+length ys+length xs) zs >>
  qselect xs n >>= \x -> wipeRet x
= {- assumption -}
  writeList i ys >> writeList (i+length ys+length xs) zs >>
  writeList (i+length ys) xs >> iqselect (i+length ys) (length xs) n >>= wipeRet
= {- commutativity, writeList-++ -}
  writeList i (ys++xs++zs) >> iqselect (i+length ys) (length xs) n >>= wipeRet
-- }}}


-- derivation iqselect {{{
-- case xs 
  qselect [] n >>= wipeRet
= mzero >>= wipeRet
= writeList i xs >> mzero >>= wipeRet

-- case xs := (x:xs)
  qselect (x:xs) n >>= wipeRet
= partl' x ([], []) xs >>= \(ys, zs) ->
  if length ys > n then qselect ys n >>= wipeRet
  else if length ys == n then return x >>= wipeRet
  else qselect zs (n-length ys-1) >>= wipeRet
⊒ {- lemma -}
  partl' x ([], []) xs >>= \(ys, zs) ->
  if length ys > n then writeList i (x:(ys++zs)) >> iqselect (i+1) (length ys) n >>= wipeRet
  else if length ys == n then writeList i (x:(ys++zs)) >> return x >>= wipeRet
  else writeList i (x:(ys++zs)) >> iqselect (i+1+length ys) (length zs) (n-length ys-1) >>= wipeRet
= partl' x ([], []) xs >>= \(ys, zs) ->
  writeList i (x:(ys++zs)) >> 
  if length ys > n then iqselect (i+1) (length ys) n >>= wipeRet
  else if length ys == n then return x >>= wipeRet
  else iqselect (i+1+length ys) (length zs) (n-length ys-1) >>= wipeRet
= {- return x >>= f = f x-}
  partl' x ([], []) xs >>= \(ys, zs) ->
  writeList i (x:(ys++zs)) >> return (length ys, length zs) >>= \(ny, nz) ->
  if ny > n then iqselect (i+1) ny n >>= wipeRet
  else if ny == n then return x >>= wipeRet
  else iqselect (i+1+ny) nz (n-ny-1) >>= wipeRet
= {- construction ipartl -}
  writeList i (x:xs) >>
  ipartl x (i+1) (0, 0) (length xs) >>= \(ny, nz) ->
  if ny > n then iqselect (i+1) ny n >>= wipeRet
  else if ny == n then return x >>= wipeRet
  else iqselect (i+1+ny) nz (n-ny-1) >>= wipeRet
= {- write-read -}
  writeList i (x:xs) >>
  read i x >>= \x ->
  ipartl x (i+1) (0, 0) (length xs) >>= \(ny, nz) ->
  if ny > n then iqselect (i+1) ny n >>= wipeRet
  else if ny == n then return x >>= wipeRet
  else iqselect (i+1+ny) nz (n-ny-1) >>= wipeRet
= writeList i (x:xs) >>
  (read i x >>= \x ->
   ipartl x (i+1) (0, 0) (length xs) >>= \(ny, nz) ->
   if ny > n then iqselect (i+1) ny n
   else if ny == n then return x
   else iqselect (i+1+ny) nz (n-ny-1)) >>= wipeRet
-- }}}

-- def
iqselect i 0 n = mzero
iqselect i (1+k) n =
  read i x >>= \x ->
  ipartl x (i+1) (0, 0) k >>= \(ny, nz) ->
  if ny > n then iqselect (i+1) ny n
  else if ny == n then return x
  else iqselect (i+1+ny) nz (n-ny-1)
