-- axiom (?)
mzero = m >> mzero
m = m `mplus` m
(`mplus`) associative
if m, n contains only nondeterminism then
    m >>= \x -> n >>= \y -> f x y
  = n >>= \y -> m >>= \x -> f x y

-- def
m ⊒ n  iff  exists e. m = n `mplus` e

-- lemma
if m ⊒ n then n `mplus` m = m

-- proof {{{
exists e. m = n `mplus` e

  n `mplus` m
= n `mplus` n `mplus` e
= n `mplus` e
= m
-- }}}

The other direction, that n `mplus` m = m implies
m ⊒ n, is immediate.

(⊒) is a partial order. That is, it is reflexive,
anti-symmetric, and transitive.

-- lemma
for all m, m ⊒ m.
This is immediate, since m `mplus` mzero =  m.

-- lemma
if m ⊒ n && n ⊒ m then m = n

-- proof {{{
m = n `mplus` m = n
-- }}}

-- lemma
if m ⊒ n && n ⊒ k then m ⊒ k.

-- proof {{{
   k
=   {- since n ⊒ k -}
   n `mplus` k
=   {- since m ⊒ n -}
   (m `mplus` n) `mplus` k
=   {- (`mplus`) associative -}
   m `mplus` (n `mplus` k)
-- }}}



-- def
split [] = return ([], [])
split (x:xs) =
  split xs >>= \(ys, zs) -> return (x:ys, zs) `mplus` return (ys, x:zs)

perm [] = return []
perm [x] = return [x]
perm (x:xs) = split xs >>= \(ys, zs) -> liftM2 (++[x]++) (perm ys) (perm zs)

slowsort xs = perm xs >>= filt sorted

-- lemma
split xs ⊒ return ([], xs)

-- proof {{{
  split []
= return ([], [])

  split (x:xs)
= split xs >>= \(ys, zs) -> return (x:ys, zs) `mplus` return (ys, x:zs)
⊒ {- induction -}
  return ([], xs) >>= \(ys, zs) -> return (x:ys, zs) `mplus` return (ys, x:zs)
= {- return x >>= f = f x -}
  return (x:[], xs) `mplus` return ([], x:xs)
⊒ return ([], x:xs)
-- }}}

-- lemma
perm xs ⊒ return xs

-- proof {{{
  perm []
= return []

  perm (x:xs)
= split xs >>= \(ys, zs) -> liftM2 (++[x]++) (perm ys) (perm zs)
⊒ {- induction -}
  split xs >>= \(ys, zs) -> liftM2 (++[x]++) (return ys) (return zs)
= split xs >>= \(ys, zs) -> return (ys++[x]++zs)
⊒ {- split xs ⊒ return ([], xs) -}
  return ([], xs) >>= \(ys, zs) -> return (ys'++[x]++zs')
= {- return x >>= f = f x -}
  return ([]++[x]++xs)
= return (x:xs)
-- }}}

-- lemma
perm xs = perm xs >>= perm

-- TODO proof



-- def
g preserves length iff
  g xs >>= \xs' -> f xs' (length xs')
= g xs >>= \xs' -> f xs' (length xs)

-- lemma
  split xs >>= \(ys, zs) -> f ys zs (length (ys++zs))
= split xs >>= \(ys, zs) -> f ys zs (length xs)

-- proof {{{
  split (x:xs) >>= \(ys, zs) -> f ys zs (length (ys++zs))
= split xs >>= \(ys, zs) ->
  mplus (f (x:ys) zs (length ((x:ys)++zs)))
        (f ys (x:zs) (length (ys++(x:zs))))
= split xs >>= \(ys, zs) ->
  mplus (f (x:ys) zs (length [x]+length ys+length zs))
        (f ys (x:zs) (length ys+length [x]+length zs))
= {- induction -}
  split xs >>= \(ys, zs) ->
  mplus (f (x:ys) zs (length [x]+length xs))
        (f ys (x:zs) (length [x]+length xs))
= split (x:xs) >>= \(ys, zs) -> f ys zs (length (x:xs))
-- }}}

-- lemma
perm preserves length

-- proof {{{
  perm (x:xs) >>= \xs' -> f xs (length xs')
= split xs >>= \(ys, zs) ->
  perm ys >>= \ys' ->
  perm zs >>= \zs' ->
  f (ys'++[x]++zs') (length (ys'++[x]++zs'))
= split xs >>= \(ys, zs) ->
  perm ys >>= \ys' ->
  perm zs >>= \zs' ->
  f (ys'++[x]++zs') (length ys'+length [x]+length zs')
= {- induction -}
  split xs >>= \(ys, zs) ->
  perm ys >>= \ys' ->
  perm zs >>= \zs' ->
  f (ys'++[x]++zs') (length ys+length [x]+length zs)
= split xs >>= \(ys, zs) ->
  perm ys >>= \ys' ->
  perm zs >>= \zs' ->
  f (ys'++[x]++zs') (length [x]+length xs)
= split xs >>= \(ys, zs) ->
  perm ys >>= \ys' ->
  perm zs >>= \zs' ->
  f (ys'++[x]++zs') (length (x:xs))
= perm (x:xs) >>= \xs' -> f xs' (length (x:xs))
-- }}}

-- lemma
(filt p) preserves length

-- proof {{{
  filt p xs >>= \xs' -> f xs' (length xs')
= filt p xs >>= \xs' -> f xs' (length xs')
= guard (p xs) >> return xs >>= \xs' -> f xs' (length xs')
= guard (p xs) >> f xs (length xs)
= guard (p xs) >> return xs >>= \xs' -> f xs' (length xs)
= filt p xs >>= \xs' -> f xs' (length xs)
-- }}}

-- lemma
if g, h both preserve length then (g >=> h) preserves length

-- proof {{{
  (g >=> h) xs >>= \xs' -> f xs' (length xs')
= g xs >>= h >>= \xs' -> f xs' (length xs')
= g xs >>= \xs' -> h xs' >>= \xs'' -> f xs'' (length xs'')
= g xs >>= \xs' -> h xs' >>= \xs'' -> f xs'' (length xs')
= g xs >>= \xs' -> h xs' >>= \xs'' -> f xs'' (length xs)
= g xs >>= h >>= \xs'' -> f xs'' (length xs)
= (g >=> h) xs >>= \xs' -> f xs' (length xs)
-- }}}

-- lemma
slowsort preserves length

-- proof {{{
perm preserves length, (filt p) preserves length
slowsort = (perm >=> filt sorted) preserves length
-- }}}



-- lemma
guard p >> m >>= f = m >>= \x -> guard p >> f x

-- proof {{{
  guard True >> m >>= f
= m >>= f
= m >>= \x -> f x
= m >>= \x -> return () >> f x
= m >>= \x -> guard True >> f x

  guard False >> m >>= f
= mzero >> m >>= f
= mzero
= {- m >> mzero = mzero -}
  m >> mzero
= m >>= \x -> mzero
= m >>= \x -> mzero >> f x
= m >>= \x -> guard False >> f x
-- }}}

-- lemma
guard (p && q) = guard p >> guard q

-- proof {{{
  guard (True && q)
= guard q
= return () >> guard q
= guard True >> guard q

  guard (False && q)
= guard False
= mzero
= mzero >> guard q
= guard False >> guard q
-- }}}


-- Goal: recursive definition for partition and slowsort
-- def
partition p xs = split xs >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)

-- lemma
partition p [] = return ([], [])
partition p (x:xs) =
  partition p xs >>= \(ys, zs) ->
  mplus (guard (x<=p) >> return (x:ys, zs))
        (guard (p<=x) >> return (ys, x:zs)))

-- proof {{{
  partition p []
= split [] >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
= return ([], []) >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
= if all (<=p) [] && all (p<=) [] then return ([], []) else mzero
= return ([], [])

  partition p (x:xs)
= split xs >>= \(ys, zs) -> return (x:ys, zs) `mplus` return (ys, x:zs)
           >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
= split xs >>= \(ys, zs) ->
  mplus (return (x:ys, zs) >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs))
        (return (ys, x:zs) >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs))
= split xs >>= \(ys, zs) ->
  mplus (guard (all (<=p) (x:ys) && all (p<=) zs) >> return (x:ys, zs))
        (guard (all (<=p) ys && all (p<=) (x:zs)) >> return (ys, x:zs))
= split xs >>= \(ys, zs) ->
  mplus (guard (x<=p && all (<=p) ys && all (p<=) zs) >> return (x:ys, zs))
        (guard (all (<=p) ys && p<=x && all (p<=) zs) >> return (ys, x:zs))
= split xs >>= \(ys, zs) ->
  mplus (guard (all (<=p) ys && all (p<=) zs) >> guard (x<=p) >> return (x:ys, zs))
        (guard (all (<=p) ys && all (p<=) zs) >> guard (p<=x) >> return (ys, x:zs))
= split xs >>= \(ys, zs) ->
  guard (all (<=p) ys && all (p<=) zs)
  mplus (guard (x<=p) >> return (x:ys, zs))
        (guard (p<=x) >> return (ys, x:zs))
= partition p xs >>= \(ys, zs) ->
  mplus (guard (x<=p) >> return (x:ys, zs))
        (guard (p<=x) >> return (ys, x:zs))
-- }}}

-- lemma
slowsort [] = return []
slowsort (x:xs) =
  partition x xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')

-- proof {{{
  slowsort (x:xs)
= perm (x:xs) >>= filt sorted
= split xs >>= \(ys, zs) ->
  liftM2 (++[x]++) (perm ys) (perm zs) >>= filt sorted
= split xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> perm zs >>= \zs' ->
  filt (sorted (cat3 ys' zs'))
= {- by \eqref{eq:sorted-cat3} -}
  split xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> perm zs >>= \zs' ->
  guard (sorted ys' && sorted zs' && all (<=x) ys' && all (x<=) zs') >>
  return (ys'++[x]++zs')
= {- guard (p && q) = guard p >> guard q -}
  split xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> perm zs >>= \zs' ->
  guard (sorted ys') >> guard (sorted zs') >> guard (all (<=x) ys' && all (x<=) zs') >>
  return (ys'++[x]++zs')
= split xs >>= \(ys, zs) ->
  (perm ys >>= filt sorted) >>= \ys' -> (perm zs >>= filt sorted) >>= \zs' ->
  guard (all (<=x) ys && all (x<=) zs) >>
  return (ys'++[x]++zs')
= split xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  guard (all (<=x) ys && all (x<=) zs) >>
  return (ys'++[x]++zs')
= {- guard p >> m >>= f = m >>= \x -> guard p >> f x -}
  split xs >>= \(ys, zs) ->
  guard (all (<=x) ys && all (x<=) zs) >>
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')
= partition x xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')
-- }}}



-- def
perm2 (ys, zs) = liftM2 (,) (perm ys) (perm zs)

-- lemma
slowsort xs = perm xs >>= slowsort

-- proof {{{
  slowsort xs
= perm xs >>= filt sorted
= perm xs >>= filt sorted
= perm xs >>= perm >>= filt sorted
= perm xs >>= slowsort
-- }}}

-- lemma
  slowsort (x:xs)
= partition x xs >>= perm2 >>= \(ys, zs) ->
  slowsort ys >>= \ys' ->
  slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')

-- proof {{{
  slowsort (x:xs)
= partition x xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')
= {- slowsort xs = perm xs >>= slowsort -}
  partition x xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> slowsort ys' >>= \ys'' ->
  perm zs >>= \zs' -> slowsort zs' >>= \zs'' ->
  return (ys'++[x]++zs')
= {- perm zs commutes with slowsort ys' -}
  partition x xs >>= \(ys, zs) ->
  perm ys >>= \ys' -> perm zs >>= \zs' ->
  slowsort ys' >>= \ys'' -> slowsort zs' >>= \zs'' ->
  return (ys'++[x]++zs')
= {- def perm2 -}
  partition x xs >>= perm2 >>= \(ys, zs) ->
  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')
-- }}}



-- Goal: tail-recursive partition
-- find partl s.t.
partl p (ys, zs) xs = partition p xs >>= \(us, vs) -> return (ys++us, zs++vs)

-- then
partition p = partl p ([], [])

-- derivation partl {{{
  partition p [] >>= \(us, vs) -> return (ys++us, zs++vs)
= return ([], []) >>= \(us, vs) -> return (ys++us, zs++vs)
= return (ys, zs)

  partition p (x:xs) >>= \(us, vs) -> return (ys++us, zs++vs)
= partition p xs >>= \(us, vs) ->
  mplus (guard (x<=p) >> return (ys++(x:us), zs++vs))
        (guard (p<=x) >> return (ys++us, zs++(x:vs)))
= mplus (guard (x<=p) >> partition p xs >>= \(us, vs) ->
         return ((ys++[x])++us, zs++vs))
        (guard (p<=x) >> partition p xs >>= \(us, vs) ->
         return (ys++us, (zs++[x])++vs)))
= mplus (guard (x<=p) >> partl p (ys++[x], zs) xs)
        (guard (p<=x) >> partl p (ys, zs++[x]) xs)
-- }}}

-- def
partl p (ys, zs) [] = return (ys, zs)
partl p (ys, zs) (x:xs) =
  mplus (guard (x<=p) >> partl p (ys++[x], zs) xs)
        (guard (p<=x) >> partl p (ys, zs++[x]) xs)



-- Goal: fuse partl and perm2
-- find partl' s.t.
  partl p (ys, zs) xs >>= perm2
= perm2 (ys, zs) >>= \(ys', zs') -> partl' p (ys', zs') xs

More generally, if f e x >>= g = g e >>= \e' -> h e' x then
foldlM f e xs >>= g = g e >>= \e' -> foldlM h e' xs
where foldlM :: (b -> a -> m b) -> b -> [a] -> m b
(also holds for ⊑, ⊒)

-- lemma
partl' p ([], []) xs = partition p xs >>= perm2

-- proof {{{
  partition p xs >>= perm2
= partl p ([], []) xs >>= perm2
= perm2 ([], []) >>= \(ys', zs') -> partl' p (ys', zs') xs
= return ([], []) >>= \(ys', zs') -> partl' p (ys', zs') xs
= partl' p ([], []) xs
-- }}}

-- lemma
perm xs >>= return . (++[x]) ⊑ perm (xs++[x])

-- TODO proof

-- lemma
perm (xs++[x]) = perm xs >>= \xs' -> perm (xs'++[x])

-- proof {{{
-- (⊑)
  perm xs >>= \xs' -> perm (xs'++[x])
⊒ return xs >>= \xs' -> perm (xs'++[x])
= perm (xs++[x])

-- (⊒)
  perm (xs++[x])
= perm (xs++[x]) >>= perm
⊒ perm xs >>= \xs' -> return (xs'++[x]) >>= perm
= perm xs >>= \xs' -> perm (xs'++[x])
-- }}}

-- lemma
perm2 (ys++[x], zs) = perm2 (ys, zs) >>= \(ys', zs') -> perm2 (ys'++[x], zs')
perm2 (ys, zs++[x]) = perm2 (ys, zs) >>= \(ys', zs') -> perm2 (ys', zs'++[x])

-- proof {{{
  perm2 (ys++[x], zs)
= perm (ys++[x]) >>= \ysx' -> perm zs >>= \zs' -> return (ysx', zs')
= perm ys >>= \ys' -> perm (ys'++[x]) >>= \ysx'' ->
  perm zs >>= \zs' -> perm zs' >>= \zs'' ->
  return (ysx'', zs'')
= perm ys >>= \ys' -> perm zs >>= \zs' ->
  perm (ys'++[x]) >>= \ysx'' -> perm zs' >>= \zs'' -> return (ysx'', zs'')
= perm2 (ys, zs) >>= \(ys', zs') -> perm2 (ys'++[x], zs')

  perm2 (ys, zs++[x])
= perm ys >>= \ys' -> perm (zs++[x]) >>= \zsx' -> return (ys', zsx')
= perm ys >>= \ys' -> perm ys' >>= \ys'' ->
  perm zs >>= \zs' -> perm (zs'++[x]) >>= \zsx'' ->
  return (ys'', zsx'')
= perm ys >>= \ys' -> perm zs >>= \zs' ->
  perm ys' >>= \ys'' -> perm (zs'++[x]) >>= \zsx'' ->
  return (ys'', zsx'')
= perm2 (ys, zs) >>= \(ys', zs') -> perm2 (ys', zs'++[x])
-- }}}

-- derivation partl' {{{
  partl p (ys, zs) [] >>= perm2
= return (ys, zs) >>= perm2
= perm2 (ys, zs)
= perm2 (ys, zs) >>= \(ys', zs') -> return (ys', zs')

  partl p (ys, zs) (x:xs) >>= perm2
= {- def partl -}
  (mplus (guard (x<=p) >> partl p (ys++[x], zs) xs)
         (guard (p<=x) >> partl p (ys, zs++[x]) xs)) >>= perm2
= {- dist, return x >> f = f x -}
  (mplus (guard (x<=p) >> return (ys++[x], zs))
         (guard (p<=x) >> return (ys, zs++[x]))) >>= \(ys', zs') ->
  partl p (ys', zs') xs >>= perm2
= {- assumption -}
  (mplus (guard (x<=p) >> return (ys++[x], zs))
         (guard (p<=x) >> return (ys, zs++[x]))) >>= perm2 >>= \(ys', zs') ->
  partl' p (ys', zs') xs
= {- dist -}
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  partl' p (ys', zs') xs
= {- lemma -}
  (mplus (guard (x<=p) >> perm2 (ys, zs) >>= \(ys', zs') -> perm2 (ys'++[x], zs'))
         (guard (p<=x) >> perm2 (ys, zs) >>= \(ys', zs') -> perm2 (ys', zs'++[x]))) >>= \(ys'', zs'') ->
  partl' p (ys'', zs'') xs
= {- commute, dist -}
  perm2 (ys, zs) >>= \(ys', zs') ->
  (mplus (guard (x<=p) >> perm2 (ys'++[x], zs'))
         (guard (p<=x) >> perm2 (ys', zs'++[x]))) >>= \(ys'', zs'') ->
  partl' p (ys', zs') xs
-- }}}

-- def
partl' p (ys, zs) [] = return (ys, zs)
partl' p (ys, zs) (x:xs) =
  (mplus (guard (x<=p) >> perm2 (ys++[x], zs))
         (guard (p<=x) >> perm2 (ys, zs++[x]))) >>= \(ys', zs') ->
  partl' p (ys', zs') xs

-- lemma
slowsort [] = return []
slowsort (x:xs) =
  partl' x ([], []) xs >>= \(ys, zs) ->
  slowsort ys >>= \ys' ->
  slowsort zs >>= \zs' ->
  return (ys'++[x]++zs')


-- vim:foldmethod=marker:
