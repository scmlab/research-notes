module Monad where

open import Function
open import Data.Bool hiding (_≤_)
open import Data.Unit using (⊤; tt)
open import Data.Nat
open import Data.List
open import Data.Product hiding (swap)
open import Relation.Binary.PropositionalEquality as P using (_≡_; _≢_)
open import Relation.Binary
open import Algebra.FunctionProperties

private
  variable
    A B C D : Set
    i j : ℕ

record Monad (M : Set → Set) : Set1 where
  infix 2 _≈_
  infixl 3 _>>=_
  field
    _≈_ : M A → M A → Set

    return : A → M A
    _>>=_  : M A → (A → M B) → M B

    isEquivalence : IsEquivalence {_} {_} {M A} _≈_

    >>=-cong : {m n : M A} {f g : A → M B}
             → m ≈ n → (∀ x → f x ≈ g x) → m >>= f ≈ n >>= g

    >>=-identity-l : {x : A} {f : A → M B} → return x >>= f ≈ f x
    >>=-identity-r : {m : M A} → m >>= return ≈ m
    >>=-assoc : {m : M A} {f : A → M B} {g : B → M C}
              → m >>= f >>= g ≈ m >>= λ x → f x >>= g

  module _ {A : Set} where
    open IsEquivalence {_} {_} {M A} isEquivalence public
    setoid : Setoid _ _
    setoid = record
      { Carrier = M A
      ; _≈_ = _≈_
      ; isEquivalence = isEquivalence
      }

    module ≈-Reasoning where
      open import Relation.Binary.Reasoning.MultiSetoid public
      infix 1 begin_
      begin_ : ∀ {x y} → IsRelatedTo setoid x y → x ≈ y
      begin_ = begin⟨ setoid ⟩_

open Monad {{...}} public


module _ {M : Set → Set} {{_ : Monad M}} where
  infixl 3 _>>_ _>=>_
  _>>_ : M A → M B → M B
  m >> n = m >>= λ _ → n

  >>-cong : {m₁ m₂ : M A} {n₁ n₂ : M B}
          → m₁ ≈ m₂ → n₁ ≈ n₂ → m₁ >> n₁ ≈ m₂ >> n₂
  >>-cong m₁≈m₂ n₁≈n₂ = >>=-cong m₁≈m₂ (const n₁≈n₂)

  >>-assoc : {m : M A} {n : M B} {o : M C}
           → (m >> n) >> o ≈ m >> (n >> o)
  >>-assoc = >>=-assoc

  _>=>_ : (A → M B) → (B → M C) → (A → M C)
  f >=> g = λ x → f x >>= g

  >>=-Commutative : M A → M B → Set1
  >>=-Commutative m n = ∀ {C} {f : ∀ {_ _} → M C}
                      → (m >>= λ x → n >>= λ y → f {x} {y}) ≈ (n >>= λ y → m >>= λ x → f {x} {y}) 
  
  liftM2 : (A → B → C) → M A → M B → M C
  liftM2 f m n = m >>= λ x → n >>= λ y → return (f x y)

  liftM2-cong : {f : A → B → C} {m₁ m₂ : M A} {n₁ n₂ : M B}
              → m₁ ≈ m₂ → n₁ ≈ n₂ → liftM2 f m₁ n₁ ≈ liftM2 f m₂ n₂
  liftM2-cong m₁≈m₂ n₁≈n₂ = >>=-cong (m₁≈m₂) (λ _ → >>=-cong n₁≈n₂ (λ _ → refl))

  liftM2->>= : {f : A → B → C} {m : M A} {n : M B} {g : C → M D}
             → liftM2 f m n >>= g ≈ (m >>= λ x → n >>= λ y → g (f x y))
  liftM2->>= {_} {_} {_} {_} {f} {m} {n} {g} =
    begin
      liftM2 f m n >>= g
    ≈⟨ >>=-assoc ⟩
      (m >>= λ x → (n >>= λ y → return (f x y)) >>= g)
    ≈⟨ >>=-cong refl (λ _ → >>=-assoc) ⟩
      (m >>= λ x → n >>= λ y → return (f x y) >>= g)
    ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ → >>=-identity-l)) ⟩
      (m >>= λ x → n >>= λ y → g (f x y))
    ∎
    where open ≈-Reasoning

  if-cong : {b : Bool} {m₁ m₂ n₁ n₂ : M A}
          → m₁ ≈ m₂ → n₁ ≈ n₂ → (if b then m₁ else n₁) ≈ (if b then m₂ else n₂)
  if-cong {_} {true} m₁≈m₂ n₁≈n₂ = m₁≈m₂
  if-cong {_} {false} m₁≈m₂ n₁≈n₂ = n₁≈n₂

  push->>=-into-if-l : {b : Bool} {m n : M A} {f : {_ : A} → M B}
                   →   ((if b then m else n) >>= λ x → f {x})
                     ≈ (if b then (m >>= λ x → f {x}) else (n >>= λ x → f {x}))
  push->>=-into-if-l {_} {_} {true} = refl
  push->>=-into-if-l {_} {_} {false} = refl

  push->>=-into-if-r : {b : Bool} {m : M A} {f g : {_ : A} → M B}
                   →   (m >>= λ x → if b then f {x} else g {x})
                     ≈ (if b then (m >>= λ x → f {x}) else (m >>= λ x → g {x}))
  push->>=-into-if-r {_} {_} {true} = refl
  push->>=-into-if-r {_} {_} {false} = refl


record MonadPlus (M : Set → Set) {{_ : Monad M}} : Set1 where
  infixl 3 _‖_
  field
    ∅ : M A
    _‖_ : M A → M A → M A

    ‖-cong : Congruent₂ {_} {_} {M A} _≈_ _‖_

    ‖-identity-l : {m : M A} → ∅ ‖ m ≈ m
    ‖-assoc : {m n o : M A} → (m ‖ n) ‖ o ≈ m ‖ (n ‖ o)
    ‖-comm : {m n : M A} → m ‖ n ≈ n ‖ m
    ‖-idempotent : {m : M A} → m ‖ m ≈ m

    >>=-zero-l : {f : A → M B} → (∅ >>= f) ≈ ∅
    >>=-zero-r : {m : M A} → (m >>= λ x → ∅ {B}) ≈ ∅
    >>=-distrib-l-‖
      : {m n : M A} {f : A → M B}
      → (m ‖ n) >>= f ≈ (m >>= f) ‖ (n >>= f)
    >>=-distrib-r-‖
      : {m : M A} {f g : A → M B}
      → (m >>= λ x → f x ‖ g x) ≈ (m >>= f) ‖ (m >>= g)

  ‖-identity-r : {m : M A} → m ‖ ∅ ≈ m
  ‖-identity-r = trans ‖-comm ‖-identity-l


open MonadPlus {{...}} public


module _ {M : Set → Set} {{_ : Monad M}} {{_ : MonadPlus M}} where
  guard : Bool → M ⊤
  guard p = if p then return tt else ∅

  guard-distrib-∧ : {p q : Bool} → guard (p ∧ q) ≈ (guard p >> guard q)
  guard-distrib-∧ {true} {q} = sym >>=-identity-l
  guard-distrib-∧ {false} {q} = sym >>=-zero-l

  guard-to-if : {p : Bool} {m n : M A}
              → (guard p >> m) ‖ (guard (not p) >> n) ≈ (if p then m else n)

  guard-to-if {_} {true} {m} {n} =
    begin
      (guard true >> m) ‖ (guard (not true) >> n)
    ≈⟨ ‖-cong >>=-identity-l >>=-zero-l ⟩
      m ‖ ∅
    ≈⟨ ‖-identity-r ⟩
      (if true then m else n)
    ∎
    where open ≈-Reasoning

  guard-to-if {_} {false} {m} {n} =
    begin
      (guard false >> m) ‖ (guard (not false) >> n)
    ≈⟨ ‖-cong >>=-zero-l >>=-identity-l ⟩
      ∅ ‖ n
    ≈⟨ ‖-identity-l ⟩
      (if false then m else n)
    ∎
    where open ≈-Reasoning

  filt : (A → Bool) → A → M A
  filt f x = guard (f x) >> return x

  filt-true : (f : A → Bool) → (x : A) → f x ≡ true → filt f x ≈ return x

  filt-true f x fx≡true =
    begin
      filt f x
    ≡⟨⟩
      guard (f x) >> return x
    ≈⟨ >>-cong (reflexive (P.cong guard fx≡true)) refl ⟩
      guard true >> return x
    ≈⟨ >>=-identity-l ⟩
      return x
    ∎
    where open ≈-Reasoning

  filt->>= : {g : A → M B} → (f : A → Bool) → (x : A) → (filt f x >>= g) ≈ (guard (f x) >> g x)
  filt->>= {_} {_} {g} f x =
    begin
      filt f x >>= g
    ≈⟨ >>=-cong refl (λ _ → refl) ⟩
      (guard (f x) >> return x) >>= g
    ≈⟨ >>=-assoc ⟩
      guard (f x) >> (return x >>= g)
    ≈⟨ >>-cong refl >>=-identity-l ⟩
      guard (f x) >> g x
    ∎
    where open ≈-Reasoning


infixl 3 _mpo->>=_ _mpo-‖_
data MonadPlusOnly (M : Set → Set) {{_ : Monad M}} {{_ : MonadPlus M}} : {A : Set} → M A → Set1 where
  mpo-∅ : MonadPlusOnly M (∅ {M} {A})
  _mpo-‖_ : {m n : M A} → MonadPlusOnly M m → MonadPlusOnly M n → MonadPlusOnly M (m ‖ n)
  mpo-return : (x : A) → MonadPlusOnly M (return x)
  _mpo->>=_ : {m : M A} {f : A → M B}
            → MonadPlusOnly M m
            → ((x : A) → MonadPlusOnly M (f x))
            → MonadPlusOnly M (m >>= f)


module _ {M : Set → Set} {{_ : Monad M}} {{_ : MonadPlus M}} where
  infixl 3 _mpo->>_ _mpo->=>_
  _mpo->=>_ : {f : A → M B} {g : B → M C}
            → ((x : A) → MonadPlusOnly M (f x))  
            → ((y : B) → MonadPlusOnly M (g y))
            → ((x : A) → MonadPlusOnly M ((f >=> g) x))
  f mpo->=> g = λ x → f x mpo->>= g

  _mpo->>_ : {m : M A} {n : M B}
           → MonadPlusOnly M m → MonadPlusOnly M n → MonadPlusOnly M (m >> n)
  m mpo->> n = m mpo->>= λ _ → n

  infix 0 mpo-if_then_else_
  mpo-if_then_else_ : {m n : M A}
                    → (p : Bool) → MonadPlusOnly M m → MonadPlusOnly M n → MonadPlusOnly M (if p then m else n)
  mpo-if true then mpo-m else mpo-n = mpo-m
  mpo-if false then mpo-m else mpo-n = mpo-n

  mpo-liftM2 : {m : M A} {n : M B}
             → (f : A → B → C) → MonadPlusOnly M m → MonadPlusOnly M n → MonadPlusOnly M (liftM2 f m n)
  mpo-liftM2 f mpo-m mpo-n = mpo-m mpo->>= λ x → mpo-n mpo->>= λ y → mpo-return (f x y)

  mpo-guard : (p : Bool) → MonadPlusOnly M (guard p)
  mpo-guard p = mpo-if p then mpo-return tt else mpo-∅

  mpo-filt : (f : A → Bool) → (x : A) → MonadPlusOnly M (filt f x)
  mpo-filt f x = mpo-guard (f x) mpo->> mpo-return x


  mpo-comm : {m : M A} {n : M B} → MonadPlusOnly M m → >>=-Commutative m n

  mpo-comm mpo-∅ {_} {f} = 
    begin
      (∅ >>= λ x → _ >>= λ y → f {x} {y})
    ≈⟨ >>=-zero-l ⟩
      ∅
    ≈⟨ sym >>=-zero-r ⟩
      (_ >>= λ y → ∅)
    ≈⟨ >>=-cong refl (λ y → sym >>=-zero-l) ⟩
      (_ >>= λ y → ∅ >>= λ x → f {x} {y})
    ∎
    where open ≈-Reasoning

  mpo-comm (mpo-return z) {_} {f} =
    begin
      (return z >>= λ x → _ >>= λ y → f {x} {y})
    ≈⟨ >>=-identity-l ⟩
      (_ >>= λ y → f {z} {y})
    ≈⟨ >>=-cong refl (λ y → sym >>=-identity-l) ⟩
      (_ >>= λ y → return z >>= λ x → f {x} {y})
    ∎
    where open ≈-Reasoning

  mpo-comm (mpo-m₁ mpo-‖ mpo-m₂) {_} {f} =
    begin
      ((_ ‖ _) >>= λ x → _ >>= λ y → f {x} {y})
    ≈⟨ >>=-distrib-l-‖ ⟩
      (_ >>= λ x → _ >>= λ y → f {x} {y}) ‖
      (_ >>= λ x → _ >>= λ y → f {x} {y})
    ≈⟨ ‖-cong (mpo-comm mpo-m₁) (mpo-comm mpo-m₂) ⟩
      (_ >>= λ y → _ >>= λ x → f {x} {y}) ‖
      (_ >>= λ y → _ >>= λ x → f {x} {y})
    ≈⟨ sym >>=-distrib-r-‖ ⟩
      (_ >>= λ y → (_ >>= λ x → f {x} {y}) ‖ (_ >>= λ x → f {x} {y}))
    ≈⟨ >>=-cong refl (λ y → sym >>=-distrib-l-‖) ⟩
      (_ >>= λ y → (_ ‖ _) >>= λ x → f {x} {y})
    ∎
    where open ≈-Reasoning

  mpo-comm (m mpo->>= k) {_} {f} =
    begin
      (_ >>= _ >>= λ x → _ >>= λ y → f {x} {y})
    ≈⟨ >>=-assoc ⟩
      (_ >>= λ z → _ >>= λ x → _ >>= λ y → f {x} {y})
    ≈⟨ >>=-cong refl (λ z → mpo-comm (k z))⟩
      (_ >>= λ z → _ >>= λ y → _ >>= λ x → f {x} {y})
    ≈⟨ mpo-comm m ⟩
      (_ >>= λ y → _ >>= λ z → _ >>= λ x → f {x} {y})
    ≈⟨ >>=-cong refl (λ y → sym >>=-assoc) ⟩
      (_ >>= λ y → _ >>= _ >>= λ x → f {x} {y})
    ∎
    where open ≈-Reasoning


record MonadArr S (M : Set → Set) {{_ : Monad M}} : Set1 where
  field
    read : ℕ → M S
    write : ℕ → S → M ⊤

    read-write : (read i >>= write i) ≈ return tt
    write-read : {s : S} → (write i s >> read i) ≈ (write i s >> return s)
    write-write : {s s' : S} → (write i s >> write i s') ≈ write i s'
    read-read : {f : S → S → M A} → (read i >>= λ s → read i >>= λ s' → f s s') ≈ (read i >>= λ s → f s s)

    read-read-comm : i ≢ j → >>=-Commutative (read i) (read j)
    write-write-comm : {s s' : S} → i ≢ j → >>=-Commutative (write i s) (write j s')
    read-write-comm : {s : S} → i ≢ j → >>=-Commutative (read i) (write j s)

open MonadArr {{...}} public

module _ {S : Set} {M : Set → Set} {{_ : Monad M}} {{_ : MonadArr S M}} where
  swap : ℕ → ℕ → M ⊤
  swap i j = read i >>= λ x → read j >>= λ y → write i y >> write j x

  readList : ℕ → ℕ → M (List S)
  readList i 0 = return []
  readList i (suc k) =
    read i >>= λ x → readList (suc i) k >>= λ xs → return (x ∷ xs)

  writeList : ℕ → List S → M ⊤
  writeList i [] = return tt
  writeList i (x ∷ xs) = write i x >> writeList (suc i) xs

  writeL : ℕ → (List S) → M ℕ
  writeL i xs = writeList i xs >> return (length xs)

  write2L : ℕ → (List S × List S) → M (ℕ × ℕ)
  write2L i (xs , ys) =
    writeList i (xs ++ ys) >>
    return (length xs , length ys)

  write3L : ℕ → (List S × List S × List S) → M (ℕ × ℕ × ℕ)
  write3L i (xs , ys , zs) =
    writeList i (xs ++ ys ++ zs) >>
    return (length xs , length ys , length zs)

  postulate
    write-write-swap : {x y : S} → i ≢ j → (write i x >> write j y >> swap i j) ≈ (write i y >> write j x)
    writeList-++
      : (xs ys : List S)
      → writeList i (xs ++ ys) ≈ writeList i xs >> writeList (i + length xs) ys 
    writeList-writeList-comm
      : (xs ys : List S) → i + length xs ≤ j → >>=-Commutative (writeList i xs) (writeList j ys)

  writeList-writeList-comm'
    : (xs ys : List S) → i + length xs ≤ j → writeList i xs >> writeList j ys ≈ writeList j ys >> writeList i xs
  writeList-writeList-comm' {i} {j} xs ys i+#xs≤j =
    begin
      writeList i xs >> writeList j ys
    ≈⟨ trans (sym >>=-identity-r) >>=-assoc ⟩
      writeList i xs >> (writeList j ys >>= return)
    ≈⟨ writeList-writeList-comm xs ys i+#xs≤j ⟩
      writeList j ys >> (writeList i xs >>= return)
    ≈⟨ sym (trans (sym >>=-identity-r) >>=-assoc) ⟩
      writeList j ys >> writeList i xs
    ∎
    where open ≈-Reasoning
