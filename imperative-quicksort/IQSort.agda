-- vim:foldmethod=marker:foldmarker=begin,∎

open import Monad
module IQSort {M : Set → Set} {{_ : Monad M}} where

open import Data.Bool as B hiding (_≤?_)
open import Data.List hiding (partition)
open import Data.List.Properties
open import Data.Product hiding (swap)
open import Data.Unit using (⊤; tt)
open import Data.Nat
open import Data.Nat.Properties
open import Relation.Binary.PropositionalEquality as P using (_≡_)

open import Refinement
open import Ord
open import Nondet {M}
open import IPartl {M}

private
  variable
    A B C D : Set

  # : List A → ℕ
  # = length

{-# TERMINATING #-}
iqsort : {{_ : Ord A}} {{_ : MonadArr A M}} → ℕ → ℕ → M ⊤
iqsort i 0 = return tt
iqsort i (suc n)  =
  read i >>= λ p →
  ipartl p (i + 1) (0 , 0 , n) >>= λ { (ny , nz) →
  swap i (i + ny) >>
  iqsort i ny >> iqsort (i + ny + 1) nz }

postulate
  introduce-swap : {{_ : MonadPlus M}} {{_ : MonadArr A M}} {j : ℕ} (ys : List A) (x : A)
                 → (perm ys >>= λ ys' → writeList j (ys' ++ [ x ])) ⊇
                   writeList j ([ x ] ++ ys) >> swap j (j + # ys)
  introduce-read : {{_ : MonadPlus M}} {{_ : MonadArr A M}} {i : ℕ} (p : A) (xs : List A)
                 → writeList i (p ∷ xs) >> return p ≈
                   writeList i (p ∷ xs) >> read i

{-# TERMINATING #-}
iqsort-spec : {{_ : Ord A}} {{_ : MonadPlus M}} {{_ : MonadArr A M}}
            → (i : ℕ) → writeL i >=> iqsort i ⊑ slowsort >=> writeList i
iqsort-spec i [] =
  begin
    slowsort [] >>= writeList i
  ⊇⟨ >>=-mono (slowsort'-spec []) (λ _ → ⊇-refl) ⟩
    return [] >>= writeList i
  ≈⟨ >>=-identity-l ⟩
    return tt
  ≈⟨ sym >>=-identity-l ⟩
    return 0 >>= iqsort i
  ≈⟨ >>=-cong (sym >>=-identity-l) (λ _ → refl) ⟩
    writeL i [] >>= iqsort i
  ∎
  where open ⊇-Reasoning

iqsort-spec i (p ∷ xs) =
  begin
    slowsort (p ∷ xs) >>= writeList i
  ⊇⟨ >>=-mono (slowsort'-spec (p ∷ xs)) (λ _ → ⊇-refl) ⟩
    (let (ys , zs) = partition p xs
     in liftM2 (_++[ p ]++_) (slowsort ys) (slowsort zs) >>= writeList i)
  ≈⟨ liftM2->>= ⟩
    (let (ys , zs) = partition p xs
     in (slowsort ys >>= λ ys' → slowsort zs >>= λ zs' → writeList i (ys' ++[ p ]++ zs')))
  ≈⟨ (let (ys , zs) = partition p xs
      in >>=-cong (sym (perm-slowsort ys)) (λ _ → >>=-cong (sym (perm-slowsort zs)) (λ _ → refl))) ⟩
    (let (ys , zs) = partition p xs
     in ((perm ys >>= λ ys'' → slowsort ys'') >>= λ ys' →
         (perm zs >>= λ zs'' → slowsort zs'') >>= λ zs' → writeList i (ys' ++[ p ]++ zs')))
  ≈⟨ >>=-assoc ⟩
    (let (ys , zs) = partition p xs
     in (perm ys >>= λ ys'' → slowsort ys'' >>= λ ys' →
         (perm zs >>= λ zs'' → slowsort zs'') >>= λ zs' → writeList i (ys' ++[ p ]++ zs')))
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ → >>=-assoc)) ⟩
    (let (ys , zs) = partition p xs
     in (perm ys >>= λ ys'' → slowsort ys'' >>= λ ys' →
         perm zs >>= λ zs'' → slowsort zs'' >>= λ zs' → writeList i (ys' ++[ p ]++ zs')))
  ≈⟨ >>=-cong refl (λ ys'' → mpo-comm (mpo-slowsort ys'')) ⟩
    (let (ys , zs) = partition p xs
     in (perm ys >>= λ ys'' → perm zs >>= λ zs'' →
         slowsort ys'' >>= λ ys' → slowsort zs'' >>= λ zs' →
         writeList i (ys' ++[ p ]++ zs')))
  ≈⟨ (let (ys , zs) = partition p xs in mpo-comm (mpo-perm ys)) ⟩
    (let (ys , zs) = partition p xs
     in (perm zs >>= λ zs'' → perm ys >>= λ ys'' →
         slowsort ys'' >>= λ ys' → slowsort zs'' >>= λ zs' →
         writeList i (ys' ++[ p ]++ zs')))
  ≈⟨ >>=-cong refl (λ _ → sym >>=-identity-l) ⟩
    (let (ys , zs) = partition p xs
     in (perm zs >>= λ zs'' → return (ys , zs'') >>= λ { (ys , zs'') →
         perm ys >>= λ ys'' →
         slowsort ys'' >>= λ ys' → slowsort zs'' >>= λ zs' →
         writeList i (ys' ++[ p ]++ zs') }))
  ≈⟨ sym >>=-assoc ⟩
    (second perm (partition p xs) >>= λ { (ys , zs) →
     perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList i (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong (reflexive (P.cong (second perm) (partition-to-partl p xs))) (λ _ → refl) ⟩
    (second perm (partl p ([] , [] , xs)) >>= λ { (ys , zs) →
     perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList i (ys' ++[ p ]++ zs') })
  ⊇⟨ >>=-mono (partl'-spec p ([] , [] , xs)) (λ _ → ⊇-refl) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList i (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>=-cong refl (λ ys' → >>=-cong refl (λ zs' →
     writeList-++ ys' ([ p ] ++ zs'))))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList i ys' >> writeList (i + # ys') ([ p ] ++ zs') })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>=-cong refl (λ ys' → >>=-cong refl (λ zs' →
     >>-cong refl (writeList-++ [ p ] zs'))))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList i ys' >>
     (writeList (i + # ys') [ p ] >> writeList (i + # ys' + 1) zs') })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>=-cong refl (λ ys' → >>=-cong refl (λ zs' →
     writeList-writeList-comm ys' [ p ] ≤-refl)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList (i + # ys') [ p ] >> (writeList i ys' >> writeList (i + # ys' + 1) zs') })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     sym >>=-identity-l ))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → return (ys' , # ys') >>= λ { (ys' , #ys') →
     slowsort zs >>= λ zs' →
     writeList (i + #ys') [ p ] >> (writeList i ys' >> writeList (i + # ys' + 1) zs') } })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ → sym >>=-assoc )) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     (slowsort ys'' >>= λ ys' → return (ys' , # ys')) >>= λ { (ys' , #ys') →
     slowsort zs >>= λ zs' →
     writeList (i + #ys') [ p ] >> (writeList i ys' >> writeList (i + # ys' + 1) zs') } })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' → >>=-cong (slowsort-preserves-length ys'') (λ _ → refl) )) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     (slowsort ys'' >>= λ ys' → return (ys' , # ys'')) >>= λ { (ys' , #ys') →
     slowsort zs >>= λ zs' →
     writeList (i + #ys') [ p ] >> (writeList i ys' >> writeList (i + # ys' + 1) zs') } })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ → >>=-assoc )) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → return (ys' , # ys'') >>= λ { (ys' , #ys') →
     slowsort zs >>= λ zs' →
     writeList (i + #ys') [ p ] >> (writeList i ys' >> writeList (i + # ys' + 1) zs') } })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → >>=-identity-l))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList (i + # ys'') [ p ] >> (writeList i ys' >> writeList (i + # ys' + 1) zs') })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → mpo-comm (mpo-slowsort zs))) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     slowsort ys'' >>= λ ys' → writeList (i + # ys'') [ p ] >> (
     slowsort zs >>= λ zs' →
     writeList i ys' >> writeList (i + # ys' + 1) zs') })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' →
     mpo-comm (mpo-slowsort ys''))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → slowsort zs >>= λ zs' →
     writeList i ys' >> writeList (i + # ys' + 1) zs') })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → >>=-cong refl (λ _ →
     >>-cong refl (>>=-cong refl (λ _ → mpo-comm (mpo-slowsort zs)))) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → writeList i ys' >> (
     slowsort zs >>= λ zs' → writeList (i + # ys' + 1) zs')) })
  ⊇⟨ >>=-mono ⊇-refl (λ { (ys , zs) → >>=-mono ⊇-refl (λ _ →
     >>-mono ⊇-refl (>>=-mono ⊇-refl (λ ys' →
     >>-mono ⊇-refl (iqsort-spec (i + # ys' + 1) zs)))) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → writeList i ys' >> (
     writeL (i + # ys' + 1) zs >>= iqsort (i + # ys' + 1))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>=-cong refl (λ _ →
     >>-cong refl >>=-assoc)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → writeList i ys' >> (
     writeList (i + # ys' + 1) zs >> (return (# zs) >>= iqsort (i + # ys' + 1)))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>=-cong refl (λ _ →
     >>-cong refl (>>-cong refl >>=-identity-l))))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → writeList i ys' >> (
     writeList (i + # ys' + 1) zs >> iqsort (i + # ys' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → >>=-cong refl (λ _ →
     >>-cong refl (>>=-cong refl (λ ys' →
     writeList-writeList-comm ys' zs (m≤m+n (i + # ys') 1)))) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → writeList (i + # ys' + 1) zs >> (
     writeList i ys' >> iqsort (i + # ys' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>=-cong refl (λ _ →
     sym >>=-identity-l)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' → return (ys' , # ys') >>= λ { (ys' , #ys') →
     writeList (i + #ys' + 1) zs >> (
     writeList i ys' >> iqsort (i + #ys' + 1) (# zs)) }) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (sym >>=-assoc))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >>
     ((slowsort ys'' >>= λ ys' → return (ys' , # ys')) >>= λ { (ys' , #ys') →
     writeList (i + #ys' + 1) zs >> (
     writeList i ys' >> iqsort (i + #ys' + 1) (# zs)) }) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' →
     >>-cong refl (>>=-cong (slowsort-preserves-length ys'') (λ _ → refl)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >>
     ((slowsort ys'' >>= λ ys' → return (ys' , # ys'')) >>= λ { (ys' , #ys') →
     writeList (i + #ys' + 1) zs >> (
     writeList i ys' >> iqsort (i + #ys' + 1) (# zs)) }) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl >>=-assoc)) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >>
     (slowsort ys'' >>= λ ys' → return (ys' , # ys'') >>= λ { (ys' , #ys') →
     writeList (i + #ys' + 1) zs >> (
     writeList i ys' >> iqsort (i + #ys' + 1) (# zs)) }) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>=-cong refl (λ _ →
     >>=-identity-l)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     slowsort ys'' >>= λ ys' →
     writeList (i + # ys'' + 1) zs >> (
     writeList i ys' >> iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' →
     >>-cong refl (mpo-comm (mpo-slowsort ys'')))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     writeList (i + # ys'' + 1) zs >> (
     slowsort ys'' >>= λ ys' → writeList i ys' >>
     iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>-cong refl (sym >>=-assoc)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     writeList (i + # ys'' + 1) zs >> (
     (slowsort ys'' >>= λ ys' → writeList i ys') >>
     iqsort (i + # ys'' + 1) (# zs))) })
  ⊇⟨ >>=-mono ⊇-refl (λ _ → >>=-mono ⊇-refl (λ ys'' →
     >>-mono ⊇-refl (>>-mono ⊇-refl (
     >>-mono (iqsort-spec i ys'') ⊇-refl)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     writeList (i + # ys'' + 1) zs >> (
     (writeL i ys'' >>= iqsort i) >>
     iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>-cong refl (
     >>-cong >>=-assoc refl)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     writeList (i + # ys'' + 1) zs >> (
     writeList i ys'' >> (return (# ys'') >>= iqsort i) >>
     iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>-cong refl (
     >>-cong (>>-cong refl >>=-identity-l) refl)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     writeList (i + # ys'' + 1) zs >> (
     writeList i ys'' >> iqsort i (# ys'') >>
     iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ →
     >>-cong refl (>>-cong refl (
     >>-assoc)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'') [ p ] >> (
     writeList (i + # ys'' + 1) zs >> (
     writeList i ys'' >> (
     iqsort i (# ys'') >> iqsort (i + # ys'' + 1) (# zs)))) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → >>=-cong refl (λ _ →
     writeList-writeList-comm [ p ] zs ≤-refl) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'' + 1) zs >> (
     writeList (i + # ys'') [ p ] >> (writeList i ys'' >> (
     iqsort i (# ys'') >> iqsort (i + # ys'' + 1) (# zs)))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' →
     >>-cong refl (sym (writeList-writeList-comm ys'' [ p ] ≤-refl)))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'' + 1) zs >> (
     writeList i ys'' >> (writeList (i + # ys'') [ p ] >> (
     iqsort i (# ys'') >> iqsort (i + # ys'' + 1) (# zs)))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' →
     >>-cong refl (sym >>-assoc))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'' + 1) zs >> (
     (writeList i ys'' >> writeList (i + # ys'') [ p ]) >> (
     iqsort i (# ys'') >> iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ ys'' →
     >>-cong refl (>>-cong (sym (writeList-++ ys'' [ p ])) refl))) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) → perm ys >>= λ ys'' →
     writeList (i + # ys'' + 1) zs >> (writeList i (ys'' ++ [ p ]) >> (
     iqsort i (# ys'') >> iqsort (i + # ys'' + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ → sym >>=-identity-l )) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     perm ys >>= λ ys'' → return (ys'' , # ys'') >>= λ { (ys'' , #ys'') →
     writeList (i + #ys'' + 1) zs >> (writeList i (ys'' ++ [ p ]) >> (
     iqsort i (#ys'') >> iqsort (i + #ys'' + 1) (# zs))) } })
  ≈⟨ >>=-cong refl (λ _ → sym >>=-assoc) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     (perm ys >>= λ ys'' → return (ys'' , # ys'')) >>= λ { (ys'' , #ys'') →
     writeList (i + #ys'' + 1) zs >> (writeList i (ys'' ++ [ p ]) >> (
     iqsort i (#ys'') >> iqsort (i + #ys'' + 1) (# zs))) } })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → >>=-cong (perm-preserves-length ys) (λ _ → refl) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     (perm ys >>= λ ys'' → return (ys'' , # ys)) >>= λ { (ys'' , #ys'') →
     writeList (i + #ys'' + 1) zs >> (writeList i (ys'' ++ [ p ]) >> (
     iqsort i (#ys'') >> iqsort (i + #ys'' + 1) (# zs))) } })
  ≈⟨ >>=-cong refl (λ _ → >>=-assoc) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     perm ys >>= λ ys'' → return (ys'' , # ys) >>= λ { (ys'' , #ys'') →
     writeList (i + #ys'' + 1) zs >> (writeList i (ys'' ++ [ p ]) >> (
     iqsort i (#ys'') >> iqsort (i + #ys'' + 1) (# zs))) } })
  ≈⟨ >>=-cong refl (λ _ → >>=-cong refl (λ _ → >>=-identity-l )) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     perm ys >>= λ ys'' →
     writeList (i + # ys + 1) zs >> (writeList i (ys'' ++ [ p ]) >> (
     iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → mpo-comm (mpo-perm ys) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (i + # ys + 1) zs >> (
     perm ys >>= λ ys'' →
     writeList i (ys'' ++ [ p ]) >> (
     iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>-cong refl (sym >>=-assoc)) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (i + # ys + 1) zs >> (
     (perm ys >>= λ ys'' → writeList i (ys'' ++ [ p ])) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ⊇⟨ >>=-mono ⊇-refl (λ { (ys , zs) → >>-mono ⊇-refl (
     >>-mono (introduce-swap ys p) ⊇-refl) }) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (i + # ys + 1) zs >> (
     (writeList i ([ p ] ++ ys) >> swap i (i + # ys)) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>-cong refl >>-assoc) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (i + # ys + 1) zs >>
     (writeList i ([ p ] ++ ys) >> (swap i (i + # ys) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs)))) })
  ≈⟨ >>=-cong refl (λ _ → sym >>-assoc) ⟩
    (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (i + # ys + 1) zs >> writeList i ([ p ] ++ ys) >>
     (swap i (i + # ys) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     >>-cong (lemma₁ p ys zs) refl }) ⟩
     (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList i (p ∷ (ys ++ zs)) >>
     (swap i (i + # ys) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ≈⟨ >>=-cong refl (λ _ → >>-assoc) ⟩
     (partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     write i p >> (writeList (suc i) (ys ++ zs) >>
     (swap i (i + # ys) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs)))) })
  ≈⟨ mpo-comm (mpo-partl' p ([] , [] , xs)) ⟩
     write i p >> (
     partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (suc i) (ys ++ zs) >> (swap i (i + # ys) >>
     (iqsort i (# ys) >> iqsort (i + # ys + 1) (# zs))) })
  ≈⟨ >>-cong refl (>>=-cong refl (λ _ → >>-cong refl (sym >>=-identity-l))) ⟩
     write i p >> (
     partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     writeList (suc i) (ys ++ zs) >> (return (# ys , # zs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) }) })
  ≈⟨ >>-cong refl (>>=-cong refl (λ _ → sym >>=-assoc)) ⟩
     write i p >> (
     partl' p ([] , [] , xs) >>= λ { (ys , zs) →
     write2L (suc i) (ys , zs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) } })
  ≈⟨ >>-cong refl (sym >>=-assoc) ⟩
     write i p >> (
     partl' p ([] , [] , xs) >>= write2L (suc i) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) })
  ⊇⟨ >>-mono ⊇-refl (>>=-mono (ipartl-spec (suc i) p ([] , [] , xs)) (λ _ → ⊇-refl)) ⟩
     write i p >> (
     write3L (suc i) ([] , [] , xs) >>= ipartl p (suc i) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) })
  ≈⟨ >>-cong refl (>>=-cong >>=-assoc (λ _ → refl)) ⟩
     write i p >> (
     (writeList (suc i) xs >> (return (0 , 0 , # xs) >>= ipartl p (suc i))) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) })
  ≈⟨ >>-cong refl (>>=-cong (>>-cong refl >>=-identity-l) (λ _ → refl)) ⟩
     write i p >> (
     (writeList (suc i) xs >> ipartl p (suc i) (0 , 0 , # xs)) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) })
  ≈⟨ >>-cong refl >>=-assoc ⟩
     write i p >> (
     writeList (suc i) xs >> (ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) }))
  ≈⟨ sym >>-assoc ⟩
     writeList i (p ∷ xs) >> (
     ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     (iqsort i #ys >> iqsort (i + #ys + 1) #zs) })
  ≈⟨ >>-cong refl (>>=-cong refl (λ _ → sym >>-assoc)) ⟩
     writeList i (p ∷ xs) >> (
     ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     iqsort i #ys >> iqsort (i + #ys + 1) #zs })
  ≈⟨ >>-cong refl (sym >>=-identity-l) ⟩
     writeList i (p ∷ xs) >> (return p >>= λ p →
     ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     iqsort i #ys >> iqsort (i + #ys + 1) #zs })
  ≈⟨ sym >>=-assoc ⟩
     (writeList i (p ∷ xs) >> return p) >>= (λ p →
     ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     iqsort i #ys >> iqsort (i + #ys + 1) #zs })
  ≈⟨ >>=-cong (introduce-read p xs) (λ _ → refl) ⟩
     (writeList i (p ∷ xs) >> read i) >>= (λ p →
     ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     iqsort i #ys >> iqsort (i + #ys + 1) #zs })
  ≈⟨ >>=-assoc ⟩
     writeList i (p ∷ xs) >> (read i >>= λ p →
     ipartl p (suc i) (0 , 0 , # xs) >>= λ { (#ys , #zs) →
     swap i (i + #ys) >>
     iqsort i #ys >> iqsort (i + #ys + 1) #zs })
  ≈⟨ >>-cong refl (>>=-cong refl (λ p →
     >>=-cong (reflexive (P.cong (λ j → ipartl p j (0 , 0 , # xs)) (+-comm 1 i))) (λ _ → refl))) ⟩
     writeList i (p ∷ xs) >> iqsort i (# (p ∷ xs))
  ≈⟨ >>-cong refl (sym >>=-identity-l) ⟩
     writeList i (p ∷ xs) >> (return (# (p ∷ xs)) >>= iqsort i)
  ≈⟨ sym >>=-assoc ⟩
    writeL i (p ∷ xs) >>= iqsort i
  ∎
  where
    lemma₁ : {{_ : MonadArr A M}} {i : ℕ} (p : A) (ys zs : List A)
           → writeList (i + # ys + 1) zs >> writeList i ([ p ] ++ ys) ≈
             writeList i (p ∷ (ys ++ zs))
    lemma₁ {_} {i} p ys zs =
      begin
        writeList (i + # ys + 1) zs >> writeList i ([ p ] ++ ys)
      ≡⟨ P.cong₂ _>>_ (P.cong (λ j → writeList j zs) (+-assoc i (# ys) 1)) P.refl ⟩
        writeList (i + (# ys + 1)) zs >> writeList i (p ∷ ys)
      ≡⟨ P.cong₂ _>>_ (P.cong (λ j → writeList (i + j) zs) (+-comm (# ys) 1)) P.refl ⟩
        writeList (i + (# (p ∷ ys))) zs >> writeList i (p ∷ ys)
      ≈⟨ sym (writeList-writeList-comm' (p ∷ ys) zs ≤-refl) ⟩
        writeList i (p ∷ ys) >> writeList (i + (# (p ∷ ys))) zs
      ≈⟨ sym (writeList-++ (p ∷ ys) zs) ⟩
        writeList i (p ∷ (ys ++ zs))
      ∎
      where open ≈-Reasoning
    open ⊇-Reasoning
