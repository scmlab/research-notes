module Test where

open import Data.Unit using (⊤; tt)
open import Data.Nat
open import Data.Nat.Properties
open import Data.Nat.Show
open import Data.List
open import Data.String hiding (show)
open import Data.Product using (_×_; _,_)
open import Data.Bool
open import Relation.Nullary.Decidable

import IO

open import Ord
open import Monad
import Implementation

private
  variable
    A : Set
    M : Set → Set

{-# TERMINATING #-}
ipartl : {{_ : Ord A}} {{_ : Monad M}} {{_ : MonadArr A M}}
       → A → ℕ → (ℕ × ℕ × ℕ) → M (ℕ × ℕ)
ipartl p i (ny , nz , 0) = return (ny , nz)
ipartl p i (ny , nz , suc k)  =
  read (i + ny + nz) >>= λ x →
  if x ≤ᵇ p then (swap (i + ny) (i + ny + nz) >>
                 ipartl p i (ny + 1 , nz , k))
            else ipartl p i (ny , nz + 1 , k)
  where open Ord.Ord {{...}}

{-# TERMINATING #-}
iqsort : {{_ : Ord A}} {{_ : Monad M}} {{_ : MonadArr A M}}
       → ℕ → ℕ → M ⊤
iqsort i 0  =  return tt
iqsort i (suc n)  =
  read i >>= λ p →
  ipartl p (i + 1) (0 , 0 , n) >>= λ { (ny , nz) →
  swap i (i + ny) >>
  iqsort i ny >> iqsort (i + ny + 1) nz }

instance
  ordℕ : Ord ℕ
  ordℕ = record
    { _≤_ = Data.Nat._≤_
    ; isDecTotalOrder = ≤-isDecTotalOrder
    }

input : ℕ → ℕ
input 0 = 9
input 1 = 4
input 2 = 8
input 3 = 7
input _ = 0

program : Implementation.M ℕ ⊤
program = iqsort 0 4

getOutput : (⊤ × (ℕ → ℕ)) → String
getOutput (tt , s) = Data.String.concat
  (show (s 0) ∷ " " ∷ show (s 1) ∷ " " ∷ show (s 2) ∷ " " ∷ show (s 3) ∷ [])

output = unlines (Data.List.map getOutput (program input))

main = IO.run (IO.putStrLn output)
