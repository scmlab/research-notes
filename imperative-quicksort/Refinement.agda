open import Monad

module Refinement {M : Set → Set} {{_ : Monad M}} {{_ : MonadPlus M}} where

import Data.Bool as B
open import Function
open import Relation.Binary
open import Relation.Binary.PropositionalEquality using (_≡_)

private
  variable
    A B C : Set

infix 2 _⊆_ _⊑_
_⊆_ : M A → M A → Set
m ⊆ n = m ‖ n ≈ n

_⊑_ : (A → M B) → (A → M B) → Set
f ⊑ g = ∀ x → f x ⊆ g x

⊆-reflexive : _≈_ {M} {A} ⇒ _⊆_
⊆-reflexive {_} {m} {n} m≈n =
  begin
    m ‖ n
  ≈⟨ ‖-cong m≈n refl ⟩
    n ‖ n
  ≈⟨ ‖-idempotent ⟩
    n
  ∎
  where open ≈-Reasoning

⊆-trans : Transitive {_} {M A} _⊆_
⊆-trans {_} {m} {n} {o} m⊆n n⊆o =
  begin
    m ‖ o
  ≈⟨ ‖-cong refl (sym n⊆o) ⟩
    m ‖ (n ‖ o)
  ≈⟨ sym ‖-assoc ⟩
    (m ‖ n) ‖ o
  ≈⟨ ‖-cong m⊆n refl ⟩
    n ‖ o
  ≈⟨ n⊆o ⟩
    o
  ∎
  where open ≈-Reasoning

⊆-antisym : Antisymmetric {_} {M A} _≈_ _⊆_
⊆-antisym {_} {m} {n} m⊆n n⊆m =
  begin
    m
  ≈⟨ sym n⊆m ⟩
    n ‖ m
  ≈⟨ ‖-comm ⟩
    m ‖ n
  ≈⟨ m⊆n ⟩
    n
  ∎
  where open ≈-Reasoning

⊆-isPreorder : IsPreorder {_} {_} {_} {M A} _≈_ _⊆_
⊆-isPreorder = record
  { isEquivalence = isEquivalence
  ; reflexive     = ⊆-reflexive
  ; trans         = ⊆-trans
  }

⊆-refl : Reflexive {_} {M A} _⊆_
⊆-refl = IsPreorder.refl ⊆-isPreorder

⊆-isPartialOrder : IsPartialOrder {_} {_} {_} {M A} _≈_ _⊆_
⊆-isPartialOrder = record
  { isPreorder = ⊆-isPreorder
  ; antisym    = ⊆-antisym
  }

⊆-poset : {A : Set} → Poset _ _ _
⊆-poset {A} = record { isPartialOrder = ⊆-isPartialOrder {A}}

module ⊆-Reasoning {A : Set} where
  open import Relation.Binary.Reasoning.PartialOrder (⊆-poset {A})
    renaming (_≤⟨_⟩_ to _⊆⟨_⟩_)
    hiding (_<⟨_⟩_)
    public


infix 2 _⊇_ _⊒_ 
_⊇_ : M A → M A → Set
_⊇_ = flip _⊆_

_⊒_ : (A → M B) → (A → M B) → Set
_⊒_ = flip _⊑_

⊇-reflexive : _≈_ {M} {A} ⇒ _⊇_
⊇-reflexive = ⊆-reflexive ∘ sym

⊇-trans : Transitive {_} {M A} _⊇_
⊇-trans = flip ⊆-trans

⊇-antisym : Antisymmetric {_} {M A} _≈_ _⊇_
⊇-antisym = flip ⊆-antisym

⊇-isPreorder : IsPreorder {_} {_} {_} {M A} _≈_ _⊇_
⊇-isPreorder = record
  { isEquivalence = isEquivalence
  ; reflexive     = ⊇-reflexive
  ; trans         = ⊇-trans
  }

⊇-refl : Reflexive {_} {M A} _⊇_
⊇-refl = IsPreorder.refl ⊇-isPreorder

⊇-isPartialOrder : IsPartialOrder {_} {_} {_} {M A} _≈_ _⊇_
⊇-isPartialOrder = record
  { isPreorder = ⊇-isPreorder
  ; antisym    = ⊇-antisym
  }

⊇-poset : {A : Set} → Poset _ _ _
⊇-poset {A} = record { isPartialOrder = ⊇-isPartialOrder {A}}

module ⊇-Reasoning {A : Set} where
  open import Relation.Binary.Reasoning.PartialOrder (⊇-poset {A})
    renaming (_≤⟨_⟩_ to _⊇⟨_⟩_)
    hiding (_<⟨_⟩_)
    public


>>=-mono : {m n : M A} {f g : A → M B}
         → m ⊆ n → f ⊑ g → (m >>= f) ⊆ (n >>= g)
>>=-mono {_} {_} {m} {n} {f} {g} m⊆n f⊑g =
  begin
    (m >>= f) ‖ (n >>= g)
  ≈⟨ ‖-cong refl (>>=-cong (sym m⊆n) (λ _ → refl)) ⟩
    (m >>= f) ‖ ((m ‖ n) >>= g)
  ≈⟨ ‖-cong refl >>=-distrib-l-‖ ⟩
    (m >>= f) ‖ ((m >>= g) ‖ (n >>= g))
  ≈⟨ sym ‖-assoc ⟩
    ((m >>= f) ‖ (m >>= g)) ‖ (n >>= g)
  ≈⟨ ‖-cong (sym >>=-distrib-r-‖) refl ⟩
    (m >>= λ x → f x ‖ g x) ‖ (n >>= g)
  ≈⟨ ‖-cong (>>=-cong refl (λ x → f⊑g x)) refl ⟩
    (m >>= g) ‖ (n >>= g)
  ≈⟨ sym >>=-distrib-l-‖ ⟩
    (m ‖ n) >>= g
  ≈⟨ >>=-cong m⊆n (λ _ → refl) ⟩
    n >>= g
  ∎
  where open ≈-Reasoning

>>-mono : {m₁ n₁ : M A} {m₂ n₂ : M B}
        → m₁ ⊆ n₁ → m₂ ⊆ n₂ → m₁ >> m₂ ⊆ n₁ >> n₂
>>-mono m₁⊆n₁ m₂⊆n₂ = >>=-mono m₁⊆n₁ (λ _ → m₂⊆n₂)

‖-mono : {m₁ m₂ n₁ n₂ : M A}
       → m₁ ⊆ n₁ → m₂ ⊆ n₂ → m₁ ‖ m₂ ⊆ n₁ ‖ n₂
‖-mono {_} {m₁} {m₂} {n₁} {n₂} m₁⊆n₁ m₂⊆n₂ =
  begin
    (m₁ ‖ m₂) ‖ (n₁ ‖ n₂)
  ≈⟨ ‖-assoc ⟩
    m₁ ‖ (m₂ ‖ (n₁ ‖ n₂))
  ≈⟨ ‖-cong refl (sym ‖-assoc) ⟩
    m₁ ‖ ((m₂ ‖ n₁) ‖ n₂)
  ≈⟨ ‖-cong refl (‖-cong ‖-comm refl) ⟩
    m₁ ‖ ((n₁ ‖ m₂) ‖ n₂)
  ≈⟨ ‖-cong refl ‖-assoc ⟩
    m₁ ‖ (n₁ ‖ (m₂ ‖ n₂))
  ≈⟨ ‖-cong refl (‖-cong refl m₂⊆n₂) ⟩
    m₁ ‖ (n₁ ‖ n₂)
  ≈⟨ sym ‖-assoc ⟩
    (m₁ ‖ n₁) ‖ n₂
  ≈⟨ ‖-cong m₁⊆n₁ refl ⟩
    n₁ ‖ n₂
  ∎
  where open ≈-Reasoning

liftM2-mono : {{_ : MonadPlus M}} {f : A → B → C} {m₁ n₁ : M A} {m₂ n₂ : M B}
            → m₁ ⊆ n₁ → m₂ ⊆ n₂ → liftM2 f m₁ m₂ ⊆ liftM2 f n₁ n₂
liftM2-mono m₁⊆n₁ m₂⊆n₂ = >>=-mono m₁⊆n₁ (λ _ → >>=-mono m₂⊆n₂ (λ _ → ⊆-refl))

if-mono : {p : B.Bool} {m₁ m₂ n₁ n₂ : M A}
        → m₁ ⊆ n₁ → m₂ ⊆ n₂
        → (B.if p then m₁ else m₂) ⊆ (B.if p then n₁ else n₂)
if-mono {_} {B.true} m₁⊆n₁ m₂⊆n₂ = m₁⊆n₁
if-mono {_} {B.false} m₁⊆n₁ m₂⊆n₂ = m₂⊆n₂

guard-mono : {{_ : MonadPlus M}} {p q : B.Bool}
           → p B.≤ q → guard p ⊆ guard q
guard-mono B.f≤t = ‖-identity-l
guard-mono B.b≤b = ⊇-reflexive refl
