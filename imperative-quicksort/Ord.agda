module Ord where

open import Data.Bool using (Bool)
open import Relation.Nullary.Decidable
open import Relation.Binary
open import Relation.Binary.PropositionalEquality using (_≡_)

-- decidable total preorder
record Ord (A : Set) : Set1 where
  infix 4 _≤_ _≤ᵇ_
  field
    _≤_ : A → A → Set
    isPreorder : IsPreorder _≡_ _≤_
    total : Total _≤_
    _≤?_ : Decidable _≤_

  _≤ᵇ_ : A → A → Bool
  a ≤ᵇ b = ⌊ a ≤? b ⌋
