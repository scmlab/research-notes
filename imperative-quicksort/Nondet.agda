-- vim:foldmethod=marker:foldmarker=begin,∎

open import Monad
module Nondet {M : Set → Set} {{_ : Monad M}} where

open import Data.Bool as B hiding (_≤?_)
open import Data.Bool.Properties
open import Data.List hiding (partition)
open import Data.List.Properties
open import Data.Empty
open import Data.Product hiding (swap)
open import Data.Sum hiding (swap)
open import Data.Nat
open import Function
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality as P using (_≡_)

open import Refinement
open import Ord

private
  variable
    A B C D : Set


split : {{_ : MonadPlus M}} → List A → M (List A × List A)
split [] = return ([] , [])
split (x ∷ xs) =
  split xs >>= λ { (ys , zs) →
  return (x ∷ ys , zs) ‖ return (ys , x ∷ zs) }

mpo-split : {{_ : MonadPlus M}} → (xs : List A) → MonadPlusOnly M (split xs) 
mpo-split [] = mpo-return ([] , [])
mpo-split (x ∷ xs) =
  mpo-split xs mpo->>= λ { (ys , zs) →
  mpo-return (x ∷ ys , zs) mpo-‖ mpo-return (ys , x ∷ zs) }

_++[_]++_ : List A → A → List A → List A
ys ++[ x ]++ zs = ys ++ [ x ] ++ zs

{-# TERMINATING #-}
perm : {{_ : MonadPlus M}} → List A → M (List A)
perm [] = return []
perm (x ∷ xs) = split xs >>= λ { (ys , zs) → liftM2 (_++[ x ]++_) (perm ys) (perm zs) }


{-# TERMINATING #-}
mpo-perm : {{_ : MonadPlus M}} → (xs : List A) → MonadPlusOnly M (perm xs)
mpo-perm [] = mpo-return []
mpo-perm (x ∷ xs) = mpo-split xs mpo->>= λ { (ys , zs) → mpo-liftM2 (_++[ x ]++_) (mpo-perm ys) (mpo-perm zs) }

postulate
  return⊑perm : {{_ : MonadPlus M}} → return {M} {List A} ⊑ perm
  perm-idempotent : {{_ : MonadPlus M}} (xs : List A)
                  → perm xs >>= perm ≈ perm xs
  perm-snoc : {{_ : MonadPlus M}} (xs : List A) (x : A)
            → perm (xs ++ [ x ]) ≈ perm xs >>= λ xs' -> perm (xs' ++ [ x ])

sorted : {{_ : Ord A}} → List A → Bool
sorted [] = true
sorted (x ∷ xs) = all (x ≤ᵇ_) xs ∧ sorted xs where open Ord.Ord {{...}}

module _ where
  open Ord.Ord {{...}}
  postulate
    sorted-cat3 : {{_ : Ord A}}
                → (ys : List A) → (x : A) → (zs : List A)
                → sorted (ys ++[ x ]++ zs) ≡
                  sorted ys ∧ sorted zs ∧ all (_≤ᵇ x) ys ∧ all (x ≤ᵇ_) zs


slowsort : {{_ : Ord A}} {{_ : MonadPlus M}} → List A → M (List A)
slowsort = perm >=> filt sorted

mpo-slowsort : {{_ : Ord A}} {{_ : MonadPlus M}}
             → (xs : List A) → MonadPlusOnly M (slowsort xs)
mpo-slowsort = mpo-perm mpo->=> mpo-filt sorted

perm-slowsort : {{_ : Ord A}} {{_ : MonadPlus M}}
              → (xs : List A) → perm xs >>= slowsort ≈ slowsort xs
perm-slowsort xs =
  begin
    perm xs >>= slowsort
  ≈⟨ sym >>=-assoc ⟩
    (perm xs >>= perm) >>= filt sorted
  ≈⟨ >>=-cong (perm-idempotent xs) (λ _ → refl) ⟩
    slowsort xs
  ∎
  where open ≈-Reasoning

_preserves_ : (A → M A) → (A → B) → Set
f preserves g = ∀ x → (f x >>= λ x' → return (x' , g x')) ≈ (f x >>= λ x' → return (x' , g x))

return-preserves : {{_ : MonadPlus M}} → (g : A → B) → return preserves g
return-preserves {_} {_} g x =
  begin
    (return x >>= λ x' → return (x' , g x'))
  ≈⟨ >>=-identity-l ⟩
    (return (x , g x))
  ≈⟨ sym >>=-identity-l ⟩
    (return x >>= λ x' → return (x' , g x))
  ∎
  where open ≈-Reasoning

preserves-closed->=>
  : {f₁ f₂ : A → M A} {g : A → B}
  → f₁ preserves g → f₂ preserves g → (f₁ >=> f₂) preserves g 

preserves-closed->=> {_} {_} {f₁} {f₂} {g} f₁-preserves-g f₂-preserves-g x =
  begin
    ((f₁ >=> f₂) x >>= λ x' → return (x' , g x'))
  ≡⟨⟩
    (f₁ x >>= f₂ >>= λ x' → return (x' , g x'))
  ≈⟨ >>=-assoc ⟩
    (f₁ x >>= λ x' → f₂ x' >>= λ x'' → return (x'' , g x''))
  ≈⟨ >>=-cong refl (f₂-preserves-g) ⟩
    (f₁ x >>= λ x' → f₂ x' >>= λ x'' → return (x'' , g x'))
  ≈⟨ >>=-cong refl (λ _ → sym >>=-identity-l) ⟩
    (f₁ x >>= λ x' → return (x' , g x') >>= λ { (x' , g-x') → f₂ x' >>= λ x'' → return (x'' , g-x') })
  ≈⟨ sym >>=-assoc ⟩ 
    ((f₁ x >>= λ x' → return (x' , g x')) >>= λ { (x' , g-x') → f₂ x' >>= λ x'' → return (x'' , g-x') })
  ≈⟨ >>=-cong (f₁-preserves-g x) (λ _ → refl) ⟩ 
    ((f₁ x >>= λ x' → return (x' , g x)) >>= λ { (x' , g-x') → f₂ x' >>= λ x'' → return (x'' , g-x') })
  ≈⟨ >>=-assoc ⟩
    (f₁ x >>= λ x' → return (x' , g x) >>= λ { (x' , g-x') → f₂ x' >>= λ x'' → return (x'' , g-x') })
  ≈⟨ >>=-cong refl (λ _ → >>=-identity-l) ⟩
    (f₁ x >>= λ x' → f₂ x' >>= λ x'' → return (x'' , g x))
  ≈⟨ sym >>=-assoc ⟩
    (f₁ x >>= f₂ >>= λ x' → return (x' , g x))
  ≡⟨⟩
    (f₁ >=> f₂) x >>= (λ x' → return (x' , g x))
  ∎
  where open ≈-Reasoning


filt-preserves : {{_ : MonadPlus M}} {p : A → Bool}
               → (g : A → B) → (filt p) preserves g
filt-preserves {_} {_} {p} g x =
  begin
    (filt p x >>= λ x' → return (x' , g x'))
  ≡⟨⟩
    (guard (p x) >> return x >>= λ x' → return (x' , g x'))
  ≈⟨ >>=-assoc ⟩
    (guard (p x) >> (return x >>= λ x' → return (x' , g x')))
  ≈⟨ >>-cong refl (return-preserves g x) ⟩
    (guard (p x) >> (return x >>= λ x' → return (x' , g x)))
  ≈⟨ sym >>=-assoc ⟩
    (guard (p x) >> return x >>= λ x' → return (x' , g x))
  ≡⟨⟩
    (filt p x >>= λ x' → return (x' , g x))
  ∎
  where open ≈-Reasoning


postulate
  perm-preserves-length : {{_ : MonadPlus M}} {{_ : Ord A}} → perm preserves (length {_} {A})
  perm-preserves-all : {{_ : MonadPlus M}} {{_ : Ord A}}
                     → (p : A → Bool) → perm preserves (all p)

slowsort-preserves-length : {{_ : MonadPlus M}} {{_ : Ord A}} → slowsort preserves length
slowsort-preserves-length = preserves-closed->=> (perm-preserves-length) (filt-preserves length)


slowsort-preserves-all : {{_ : MonadPlus M}} {{_ : Ord A}}
                       → (p : A → Bool) → slowsort preserves (all p)
slowsort-preserves-all p = preserves-closed->=> (perm-preserves-all p) (filt-preserves (all p))



isPartition : {{_ : Ord A}} → A → (List A × List A) → Bool
isPartition p (ys , zs) = all (_≤ᵇ p) ys ∧ all (p ≤ᵇ_) zs
  where open Ord.Ord {{...}}

partition : {{_ : Ord A}} → A → List A → (List A × List A)
partition p [] = ([] , [])
partition p (x ∷ xs) =
  let (ys , zs) = partition p xs
  in if x ≤ᵇ p then (x ∷ ys , zs) else (ys , x ∷ zs)
  where open Ord.Ord {{...}}

partition-spec
  : {{_ : Ord A}} {{_ : MonadPlus M}} (p : A)
  → return ∘ partition p ⊑ split >=> filt (isPartition p)

partition-spec p [] =
  begin
    split [] >>= filt (isPartition p) 
  ≈⟨ >>=-identity-l ⟩
    filt (isPartition p) ([] , [])
  ≈⟨ filt-true (isPartition p) ([] , []) P.refl ⟩
    return (partition p [])
  ∎
  where open ⊇-Reasoning

partition-spec p (x ∷ xs) =
  begin
    split (x ∷ xs) >>= filt (isPartition p) 
  ≡⟨⟩
    split xs >>= (λ { (ys , zs) →
    return (x ∷ ys , zs) ‖ return (ys , x ∷ zs) }) >>= filt (isPartition p) 
  ≈⟨ >>=-assoc ⟩
    split xs >>= (λ { (ys , zs) →
    (return (x ∷ ys , zs) ‖ return (ys , x ∷ zs)) >>= filt (isPartition p) })
  ≈⟨ >>=-cong refl (λ _ → >>=-distrib-l-‖ ) ⟩
    split xs >>= (λ { (ys , zs) →
    (return (x ∷ ys , zs) >>= filt (isPartition p)) ‖
    (return (ys , x ∷ zs) >>= filt (isPartition p)) })
  ≈⟨ >>=-cong refl (λ _ → ‖-cong >>=-identity-l >>=-identity-l) ⟩
    split xs >>= (λ { (ys , zs) →
    filt (isPartition p) (x ∷ ys , zs) ‖
    filt (isPartition p) (ys , x ∷ zs) })
  ≡⟨⟩
    split xs >>= (λ { (ys , zs) →
    (guard (isPartition p (x ∷ ys , zs)) >> return (x ∷ ys , zs)) ‖
    (guard (isPartition p (ys , x ∷ zs)) >> return (ys , x ∷ zs)) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     ‖-cong (>>-cong (reflexive (P.cong guard (isPartition-∷-l p x ys zs))) refl)
            (>>-cong (reflexive (P.cong guard (isPartition-∷-r p x ys zs))) refl) }) ⟩
    split xs >>= (λ { (ys , zs) →
    (guard ((x ≤ᵇ p) ∧ isPartition p (ys , zs)) >> return (x ∷ ys , zs)) ‖
    (guard ((p ≤ᵇ x) ∧ isPartition p (ys , zs)) >> return (ys , x ∷ zs)) })
  ≈⟨ >>=-cong refl (λ _ → ‖-cong (>>-cong guard-distrib-∧ refl) (>>-cong guard-distrib-∧ refl)) ⟩
    split xs >>= (λ { (ys , zs) →
    (guard (x ≤ᵇ p) >> guard (isPartition p (ys , zs)) >> return (x ∷ ys , zs)) ‖
    (guard (p ≤ᵇ x) >> guard (isPartition p (ys , zs)) >> return (ys , x ∷ zs)) })
  ≈⟨ >>=-cong refl (λ _ → ‖-cong >>=-assoc >>=-assoc) ⟩
    split xs >>= (λ { (ys , zs) →
    (guard (x ≤ᵇ p) >> (guard (isPartition p (ys , zs)) >> return (x ∷ ys , zs))) ‖
    (guard (p ≤ᵇ x) >> (guard (isPartition p (ys , zs)) >> return (ys , x ∷ zs))) })
  ⊇⟨ >>=-mono ⊇-refl (λ _ →
     ‖-mono ⊇-refl (>>-mono (guard-mono lemma₁) ⊇-refl)) ⟩
    split xs >>= (λ { (ys , zs) →
    (guard (x ≤ᵇ p) >> (guard (isPartition p (ys , zs)) >> return (x ∷ ys , zs))) ‖
    (guard (not (x ≤ᵇ p)) >> (guard (isPartition p (ys , zs)) >> return (ys , x ∷ zs))) })
  ≈⟨ >>=-cong refl (λ _ → guard-to-if) ⟩
    split xs >>= (λ { (ys , zs) →
    if x ≤ᵇ p then guard (isPartition p (ys , zs)) >> return (x ∷ ys , zs)
              else guard (isPartition p (ys , zs)) >> return (ys , x ∷ zs) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → sym push->>=-into-if-r})⟩
    split xs >>= (λ { (ys , zs) →
    guard (isPartition p (ys , zs)) >> 
    (if x ≤ᵇ p then return (x ∷ ys , zs)
               else return (ys , x ∷ zs)) })
  ≈⟨ >>=-cong refl (λ _ → >>-cong refl (sym >>=-identity-l)) ⟩
    split xs >>= (λ { (ys , zs) →
    guard (isPartition p (ys , zs)) >> (return (ys , zs) >>= λ { (ys , zs) → 
    if x ≤ᵇ p then return (x ∷ ys , zs)
              else return (ys , x ∷ zs) }) })
  ≈⟨ >>=-cong refl (λ _ → sym >>=-assoc) ⟩
    split xs >>= (λ { (ys , zs) →
    filt (isPartition p) (ys , zs) >>= λ { (ys , zs) → 
    if x ≤ᵇ p then return (x ∷ ys , zs)
              else return (ys , x ∷ zs) } })
  ≈⟨ sym >>=-assoc ⟩
    split xs >>= filt (isPartition p) >>= (λ { (ys , zs) →
    if x ≤ᵇ p then return (x ∷ ys , zs)
              else return (ys , x ∷ zs) })
  ⊇⟨ >>=-mono (partition-spec p xs) (λ _ → ⊇-refl) ⟩
    return (partition p xs) >>= (λ { (ys , zs) →
    if x ≤ᵇ p then return (x ∷ ys , zs)
              else return (ys , x ∷ zs) })
  ≈⟨ >>=-identity-l ⟩
    (let (ys , zs) = partition p xs
     in if x ≤ᵇ p then return (x ∷ ys , zs) else return (ys , x ∷ zs))
  ≡⟨ P.sym (push-function-into-if return (x ≤ᵇ p)) ⟩
    return (partition p (x ∷ xs))
  ∎
  where 
    open Ord.Ord {{...}} using (_≤ᵇ_; _≤?_; total)

    lemma₁ : {{_ : Ord A}} {a b : A} → not (a ≤ᵇ b) B.≤ (b ≤ᵇ a)
    lemma₁ {_} {{ordA}} {a} {b} with Ord.Ord._≤?_ ordA a b | Ord.Ord._≤?_ ordA b a
    ... | yes _ | yes _  = f≤t
    ... | yes _ | no _  = b≤b
    ... | no _ | yes _  = b≤b
    ... | no ¬a≤b | no ¬b≤a with total a b
    ... | inj₁ a≤b = ⊥-elim (¬a≤b a≤b)
    ... | inj₂ b≤a = ⊥-elim (¬b≤a b≤a)

    isPartition-∷-l : {{_ : Ord A}} (p y : A) (ys zs : List A)
                    → isPartition p (y ∷ ys , zs) ≡ ((y ≤ᵇ p) ∧ isPartition p (ys , zs))
    isPartition-∷-l p y ys zs = ∧-assoc (y ≤ᵇ p) _ _

    isPartition-∷-r : {{_ : Ord A}} (p z : A) (ys zs : List A)
                    → isPartition p (ys , z ∷ zs) ≡ ((p ≤ᵇ z) ∧ isPartition p (ys , zs))
    isPartition-∷-r p z ys zs =
      begin
        isPartition p (ys , z ∷ zs)
      ≡⟨ P.sym (∧-assoc (all (_≤ᵇ p) ys) (p ≤ᵇ z) (all (p ≤ᵇ_) zs)) ⟩
        (all (_≤ᵇ p) ys ∧ (p ≤ᵇ z)) ∧ all (p ≤ᵇ_) zs
      ≡⟨ P.cong₂ _∧_ (∧-comm (all (_≤ᵇ p) ys) (p ≤ᵇ z)) P.refl ⟩
        ((p ≤ᵇ z) ∧ all (_≤ᵇ p) ys) ∧ all (p ≤ᵇ_) zs
      ≡⟨ ∧-assoc (p ≤ᵇ z) (all (_≤ᵇ p) ys) (all (p ≤ᵇ_) zs) ⟩
        (p ≤ᵇ z) ∧ isPartition p (ys , zs)
      ∎
      where open P.≡-Reasoning

    open ⊇-Reasoning


{-# TERMINATING #-}
partl : {{_ : Ord A}} → A → (List A × List A × List A) → (List A × List A)
partl p (ys , zs , []) = (ys , zs)
partl p (ys , zs , x ∷ xs) =
  if x ≤ᵇ p then partl p (ys ++ [ x ] , zs , xs)
            else partl p (ys , zs ++ [ x ] , xs)
  where open Ord.Ord {{...}}

{-# TERMINATING #-}
partl-spec : {{_ : Ord A}} → (p : A) → (ys,zs,xs : List A × List A × List A)
           → let (ys , zs , xs) = ys,zs,xs
             in partl p (ys , zs , xs) ≡ (let (us , vs) = partition p xs in (ys ++ us , zs ++ vs))

partl-spec p (ys , zs , []) = P.sym (P.cong₂ _,_ (++-identityʳ ys) (++-identityʳ zs))
partl-spec p (ys , zs , (x ∷ xs)) =
  begin
    partl p (ys , zs , (x ∷ xs))
  ≡⟨⟩
    (if x ≤ᵇ p then partl p (ys ++ [ x ] , zs , xs)
               else partl p (ys , zs ++ [ x ] , xs))
  ≡⟨ P.cong₂ (if x ≤ᵇ p then_else_) (partl-spec p (ys ++ [ x ] , zs , xs))
                                    (partl-spec p (ys , zs ++ [ x ] , xs)) ⟩
    (let (us , vs) = partition p xs
     in if x ≤ᵇ p then ((ys ++ [ x ]) ++ us , zs ++ vs) else (ys ++ us , (zs ++ [ x ]) ++ vs))
  ≡⟨ (let (us , vs) = partition p xs
      in P.cong₂ (if x ≤ᵇ p then_else_) (P.cong₂ _,_ (++-assoc ys [ x ] us) P.refl)
                                        (P.cong₂ _,_ P.refl (++-assoc zs [ x ] vs))) ⟩
    (let (us , vs) = partition p xs
     in if x ≤ᵇ p then (ys ++ (x ∷ us) , zs ++ vs) else (ys ++ us , zs ++ (x ∷ vs)))
  ≡⟨ P.sym (let (us , vs) = partition p xs
            in push-function-into-if (λ { (us , vs) → (ys ++ us , zs ++ vs) })
                                     (x ≤ᵇ p)
                                     {(x ∷ us , vs)}
                                     {(us , x ∷ vs)}) ⟩
    (let (us , vs) = partition p (x ∷ xs) in (ys ++ us , zs ++ vs))
  ∎
  where open P.≡-Reasoning
        open Ord.Ord {{...}} using (_≤ᵇ_)

partition-to-partl
  : {{_ : Ord A}} → (p : A) → (xs : List A) → partition p xs ≡ partl p ([] , [] , xs)
partition-to-partl p xs = P.sym (partl-spec p ([] , [] , xs))


second : (B → M C) → (A × B) → M (A × C)
second f (x , y) = f y >>= λ y' → return (x , y')

{-# TERMINATING #-}
partl' : {{_ : Ord A}} {{_ : MonadPlus M}}
       → A → (List A × List A × List A) → M (List A × List A)
partl' p (ys , zs , []) = return (ys , zs)
partl' p (ys , zs , x ∷ xs) =
  if x ≤ᵇ p then (perm zs >>= λ zs' → partl' p (ys ++ [ x ] , zs' , xs))
            else (perm (zs ++ [ x ]) >>= λ zs' → partl' p (ys , zs' , xs))
  where open Ord.Ord {{...}}

{-# TERMINATING #-}
mpo-partl' : {{_ : Ord A}} {{_ : MonadPlus M}}
           → (p : A) → (t : List A × List A × List A) → MonadPlusOnly M (partl' p t)
mpo-partl' p (ys , zs , []) = mpo-return (ys , zs)
mpo-partl' p (ys , zs , x ∷ xs) =
  mpo-if x ≤ᵇ p then (mpo-perm zs mpo->>= λ zs' → mpo-partl' p (ys ++ [ x ] , zs' , xs))
                else (mpo-perm (zs ++ [ x ]) mpo->>= λ zs' → mpo-partl' p (ys , zs' , xs))
  where open Ord.Ord {{...}}

{-# TERMINATING #-}
partl'-spec : {{_ : Ord A}} {{_ : MonadPlus M}}
            → (p : A) → partl' p ⊑ second perm ∘ partl p
partl'-spec p (ys , zs , xs) =
  begin
    second perm (partl p (ys , zs , xs))
  ⊇⟨ go (ys , zs , xs) ⟩
    (perm zs >>= λ zs' → return (ys , zs' , xs)) >>= partl' p
  ≈⟨ >>=-assoc ⟩
    perm zs >>= (λ zs' → return (ys , zs' , xs) >>= partl' p)
  ⊇⟨ >>=-mono (return⊑perm zs) (λ zs' → ⊇-reflexive >>=-identity-l) ⟩
    return zs >>= (λ zs' → partl' p (ys , zs' , xs))
  ≈⟨ >>=-identity-l ⟩
    partl' p (ys , zs , xs)
  ∎
  where
    open Ord.Ord {{...}} using (_≤ᵇ_)

    snd3 : (B → M D) → (A × B × C) → M (A × D × C)
    snd3 f (x , y , z) = f y >>= λ y' → return (x , y' , z)

    go : snd3 perm >=> partl' p ⊑ second perm ∘ partl p
    go (ys , zs , []) =
      begin
        second perm (partl p (ys , zs , []))
      ≡⟨⟩
        (perm zs >>= λ zs' → return (ys , zs'))
      ≈⟨ >>=-cong refl (λ _ → sym >>=-identity-l) ⟩
        (perm zs >>= λ zs' → return (ys , zs' , []) >>= partl' p)
      ≈⟨ sym >>=-assoc ⟩
        snd3 perm (ys , zs , []) >>= partl' p
      ∎
      where open ⊇-Reasoning

    go (ys , zs , x ∷ xs) =
      begin
        second perm (partl p (ys , zs , x ∷ xs))
      ≡⟨ P.cong (second perm) (P.sym (push-function-into-if (partl p) (x ≤ᵇ p))) ⟩
        second perm (partl p (if x ≤ᵇ p then (ys ++ [ x ] , zs , xs)
                                      else (ys , zs ++ [ x ] , xs)))
      ⊇⟨ go _ ⟩
        snd3 perm (if x ≤ᵇ p then (ys ++ [ x ] , zs , xs)
                             else (ys , zs ++ [ x ] , xs)) >>= partl' p
      ≡⟨ P.cong₂ _>>=_ (push-function-into-if (snd3 perm) (x ≤ᵇ p)) P.refl ⟩
        (if x ≤ᵇ p then snd3 perm (ys ++ [ x ] , zs , xs)
                   else snd3 perm (ys , zs ++ [ x ] , xs)) >>= partl' p
      ≈⟨ >>=-cong (if-cong lemma₁ lemma₂) (λ _ → refl) ⟩
        (if x ≤ᵇ p then (perm zs >>= λ zs' → snd3 perm (ys ++ [ x ] , zs' , xs))
                   else (perm zs >>= λ zs' → snd3 perm (ys , zs' ++ [ x ] , xs))) >>= partl' p
      ≈⟨ >>=-cong (sym push->>=-into-if-r) (λ _ → refl) ⟩
        (perm zs >>= λ zs' → 
         if x ≤ᵇ p then snd3 perm (ys ++ [ x ] , zs' , xs)
                   else snd3 perm (ys , zs' ++ [ x ] , xs)) >>= partl' p
      ≈⟨ >>=-assoc ⟩
        (perm zs >>= λ zs' → 
        (if x ≤ᵇ p then snd3 perm (ys ++ [ x ] , zs' , xs)
                   else snd3 perm (ys , zs' ++ [ x ] , xs)) >>= partl' p)
      ≈⟨ >>=-cong refl (λ zs' → push->>=-into-if-l) ⟩
        (perm zs >>= λ zs' →
         if x ≤ᵇ p then snd3 perm (ys ++ [ x ] , zs' , xs) >>= partl' p
                   else snd3 perm (ys , zs' ++ [ x ] , xs) >>= partl' p)
      ≈⟨ >>=-cong refl (λ _ → if-cong >>=-assoc >>=-assoc) ⟩
        (perm zs >>= λ zs' →
         if x ≤ᵇ p then perm zs' >>= (λ zs'' → return (ys ++ [ x ] , zs'' , xs) >>= partl' p)
                   else perm (zs' ++ [ x ]) >>= (λ zs'' → return (ys , zs'' , xs) >>= partl' p))
      ≈⟨ >>=-cong refl (λ _ → if-cong (>>=-cong refl (λ _ → >>=-identity-l))
                                      (>>=-cong refl (λ _ → >>=-identity-l))) ⟩
        (perm zs >>= λ zs' → partl' p (ys , zs' , x ∷ xs))
      ≈⟨ >>=-cong refl (λ _ → sym >>=-identity-l) ⟩
        (perm zs >>= λ zs' → return (ys , zs' , x ∷ xs) >>= partl' p)
      ≈⟨ sym >>=-assoc ⟩
        snd3 perm (ys , zs , x ∷ xs) >>= partl' p
      ∎
      where
        lemma₁ : snd3 perm (ys ++ [ x ] , zs , xs) ≈ (perm zs >>= λ zs' → snd3 perm (ys ++ [ x ] , zs' , xs))
        lemma₁ =
          begin
            snd3 perm (ys ++ [ x ] , zs , xs)
          ≈⟨ >>=-cong (sym (perm-idempotent zs)) (λ _ → refl) ⟩
            (perm zs >>= perm) >>= (λ zs' → return (ys ++ [ x ] , zs' , xs))
          ≈⟨ >>=-assoc ⟩
            (perm zs >>= λ zs' → snd3 perm (ys ++ [ x ] , zs' , xs))
          ∎
          where open ≈-Reasoning

        lemma₂ : snd3 perm (ys , zs ++ [ x ] , xs) ≈ (perm zs >>= λ zs' → snd3 perm (ys , zs' ++ [ x ] , xs))
        lemma₂ =
          begin
            snd3 perm (ys , zs ++ [ x ] , xs)
          ≈⟨ >>=-cong (perm-snoc zs x) (λ _ → refl) ⟩
            (perm zs >>= λ zs' → perm (zs' ++ [ x ])) >>= (λ zs'' → return (ys , zs'' , xs))
          ≈⟨ >>=-assoc ⟩
            (perm zs >>= λ zs' → snd3 perm (ys , zs' ++ [ x ] , xs))
          ∎
          where open ≈-Reasoning

        open ⊇-Reasoning
    open ⊇-Reasoning


slowsort' : {{_ : Ord A}} {{_ : MonadPlus M}} → List A → M (List A)
slowsort' [] = return []
slowsort' (p ∷ xs) =
  let (ys , zs) = partition p xs
  in liftM2 (_++[ p ]++_) (slowsort ys) (slowsort zs)


perm-guard-sorted
  : {{_ : Ord A}} {{_ : MonadPlus M}} {f : List A → M B}
  → (xs : List A) → (perm xs >>= λ xs' → guard (sorted xs') >> f xs') ≈ (slowsort xs >>= f)

perm-guard-sorted {_} {_} {f} xs =
  begin
    perm xs >>= (λ xs' → guard (sorted xs') >> f xs')
  ≈⟨ >>=-cong refl ((λ xs' → sym (filt->>= sorted xs'))) ⟩
    perm xs >>= (λ xs' → filt sorted xs' >>= f)
  ≈⟨ sym >>=-assoc ⟩
    perm xs >>= filt sorted >>= f
  ≡⟨⟩
    slowsort xs >>= f
  ∎
  where open ≈-Reasoning

slowsort'-spec : {{_ : Ord A}} {{_ : MonadPlus M}} → slowsort' ⊑ slowsort

slowsort'-spec [] =
  begin
    slowsort []
  ≈⟨ >>=-identity-l ⟩
    filt sorted []
  ≈⟨ filt-true sorted [] P.refl ⟩
    slowsort' []
  ∎
  where open ⊇-Reasoning

slowsort'-spec (p ∷ xs) =
  begin
    slowsort (p ∷ xs)
  ≡⟨⟩
    split xs >>= (λ { (ys , zs) → liftM2 (_++[ p ]++_) (perm ys) (perm zs) }) >>= filt sorted
  ≈⟨ >>=-assoc ⟩
    split xs >>= (λ { (ys , zs) → liftM2 (_++[ p ]++_) (perm ys) (perm zs) >>= filt sorted })
  ≈⟨ >>=-cong refl (λ _ → liftM2->>= ) ⟩
    split xs >>= (λ { (ys , zs) →
    perm ys >>= λ ys' → perm zs >>= λ zs' → filt sorted (ys' ++[ p ]++ zs') })
  ≡⟨⟩
    split xs >>= (λ { (ys , zs) →
    perm ys >>= λ ys' → perm zs >>= λ zs' →
    guard (sorted (ys' ++[ p ]++ zs')) >>
    return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ ys' →
     >>=-cong refl (λ zs' →
     >>-cong (reflexive (P.cong guard (sorted-cat3 ys' p zs'))) refl))) ⟩
    split xs >>= (λ { (ys , zs) →
    perm ys >>= λ ys' → perm zs >>= λ zs' →
    guard (sorted ys' ∧ sorted zs' ∧ all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >>
    return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>-cong guard-distrib-∧ refl ))) ⟩
    split xs >>= (λ { (ys , zs) →
    perm ys >>= λ ys' → perm zs >>= λ zs' →
    guard (sorted ys') >> guard (sorted zs' ∧ all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >>
    return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → >>-assoc ))) ⟩
    split xs >>= (λ { (ys , zs) →
    perm ys >>= λ ys' →
    perm zs >>= λ zs' → guard (sorted ys') >>
    (guard (sorted zs' ∧ all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >> return (ys' ++[ p ]++ zs')) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     >>=-cong refl (λ ys' →
     mpo-comm (mpo-perm zs) )}) ⟩
    split xs >>= (λ { (ys , zs) →
    perm ys >>= λ ys' → guard (sorted ys') >>
    (perm zs >>= λ zs' →
    guard (sorted zs' ∧ all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >> return (ys' ++[ p ]++ zs') )})
  ≈⟨ >>=-cong refl (λ { (ys , zs) → perm-guard-sorted ys }) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    perm zs >>= λ zs' →
    guard (sorted zs' ∧ all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >> return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>-cong guard-distrib-∧ refl ))) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    perm zs >>= λ zs' →
    guard (sorted zs') >> guard (all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >> return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → >>-assoc ))) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    perm zs >>= λ zs' →
    guard (sorted zs') >> (guard (all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >> return (ys' ++[ p ]++ zs')) })
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     >>=-cong refl (λ _ → perm-guard-sorted zs )}) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    slowsort zs >>= λ zs' →
    guard (all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs') >>
    return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → sym >>=-identity-l ))) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    slowsort zs >>= λ zs' → return (zs' , all (p ≤ᵇ_) zs') >>= λ { (zs' , all-p-≤ᵇ-zs') →
    guard (all (_≤ᵇ p) ys' ∧ all-p-≤ᵇ-zs') >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → sym >>=-assoc )) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    (slowsort zs >>= λ zs' → return (zs' , all (p ≤ᵇ_) zs')) >>= λ { (zs' , all-p-≤ᵇ-zs') →
    guard (all (_≤ᵇ p) ys' ∧ all-p-≤ᵇ-zs') >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     >>=-cong refl (λ _ →
     >>=-cong (slowsort-preserves-all (p ≤ᵇ_) zs) (λ _ → refl) )}) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    (slowsort zs >>= λ zs' → return (zs' , all (p ≤ᵇ_) zs)) >>= λ { (zs' , all-p-≤ᵇ-zs') →
    guard (all (_≤ᵇ p) ys' ∧ all-p-≤ᵇ-zs') >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → >>=-assoc )) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    slowsort zs >>= λ zs' → return (zs' , all (p ≤ᵇ_) zs) >>= λ { (zs' , all-p-≤ᵇ-zs') →
    guard (all (_≤ᵇ p) ys' ∧ all-p-≤ᵇ-zs') >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → >>=-identity-l ))) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    slowsort zs >>= λ zs' →
    guard (all (_≤ᵇ p) ys' ∧ all (p ≤ᵇ_) zs) >>
    return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → sym >>=-identity-l )) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' → return (ys' , all (_≤ᵇ p) ys') >>= λ { (ys' , all-≤ᵇ-p-ys') →
    slowsort zs >>= λ zs' →
    guard (all-≤ᵇ-p-ys' ∧ all (p ≤ᵇ_) zs) >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ _ → sym >>=-assoc ) ⟩
    split xs >>= (λ { (ys , zs) →
    (slowsort ys >>= λ ys' → return (ys' , all (_≤ᵇ p) ys')) >>= λ { (ys' , all-≤ᵇ-p-ys') →
    slowsort zs >>= λ zs' →
    guard (all-≤ᵇ-p-ys' ∧ all (p ≤ᵇ_) zs) >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ { (ys , zs) → >>=-cong (slowsort-preserves-all (_≤ᵇ p) ys) (λ _ → refl) }) ⟩
    split xs >>= (λ { (ys , zs) →
    (slowsort ys >>= λ ys' → return (ys' , all (_≤ᵇ p) ys)) >>= λ { (ys' , all-≤ᵇ-p-ys') →
    slowsort zs >>= λ zs' →
    guard (all-≤ᵇ-p-ys' ∧ all (p ≤ᵇ_) zs) >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ _ → >>=-assoc ) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' → return (ys' , all (_≤ᵇ p) ys) >>= λ { (ys' , all-≤ᵇ-p-ys') →
    slowsort zs >>= λ zs' →
    guard (all-≤ᵇ-p-ys' ∧ all (p ≤ᵇ_) zs) >>
    return (ys' ++[ p ]++ zs') } })
  ≈⟨ >>=-cong refl (λ _ →
     >>=-cong refl (λ _ → >>=-identity-l )) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    slowsort zs >>= λ zs' →
    guard (all (_≤ᵇ p) ys ∧ all (p ≤ᵇ_) zs) >>
    return (ys' ++[ p ]++ zs') })
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     >>=-cong refl (λ _ →
     mpo-comm (mpo-slowsort zs) )}) ⟩
    split xs >>= (λ { (ys , zs) →
    slowsort ys >>= λ ys' →
    guard (all (_≤ᵇ p) ys ∧ all (p ≤ᵇ_) zs) >>
    (slowsort zs >>= λ zs' → return (ys' ++[ p ]++ zs') )})
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
     mpo-comm (mpo-slowsort ys) }) ⟩
    split xs >>= (λ { (ys , zs) →
    guard (all (_≤ᵇ p) ys ∧ all (p ≤ᵇ_) zs) >>
    (slowsort ys >>= λ ys' → slowsort zs >>= λ zs' → return (ys' ++[ p ]++ zs') )})
  ≈⟨ >>=-cong refl (λ { (ys , zs) →
    sym (filt->>= (isPartition p) (ys , zs)) }) ⟩
    split xs >>= (λ { (ys , zs) →
    filt (isPartition p) (ys , zs) >>= λ { (ys , zs) →
    slowsort ys >>= λ ys' → slowsort zs >>= λ zs' → return (ys' ++[ p ]++ zs') } })
  ≈⟨ sym >>=-assoc ⟩
    split xs >>= filt (isPartition p) >>= (λ { (ys , zs) → liftM2 (_++[ p ]++_) (slowsort ys) (slowsort zs) })
  ⊇⟨ >>=-mono (partition-spec p xs) (λ _ → ⊇-refl) ⟩
    return (partition p xs) >>= (λ { (ys , zs) → liftM2 (_++[ p ]++_) (slowsort ys) (slowsort zs) })
  ≈⟨ >>=-identity-l ⟩
    slowsort' (p ∷ xs)
  ∎
  where open ⊇-Reasoning
        open Ord.Ord {{...}} using (_≤ᵇ_)

{-# TERMINATING #-}
qsort : {{_ : Ord A}} → List A → List A
qsort [] = []
qsort (p ∷ xs) =
  let (ys , zs) = partition p xs
  in qsort ys ++ [ p ] ++ qsort zs

{-# TERMINATING #-}
qsort-spec : {{_ : Ord A}} {{_ : MonadPlus M}} → return ∘ qsort ⊑ slowsort
qsort-spec [] = slowsort'-spec []
qsort-spec (p ∷ xs) =
  let (ys , zs) = partition p xs in
  begin
    slowsort (p ∷ xs)
  ⊇⟨ slowsort'-spec (p ∷ xs) ⟩
    liftM2 (_++[ p ]++_) (slowsort ys) (slowsort zs)
  ⊇⟨ liftM2-mono (qsort-spec ys) (qsort-spec zs) ⟩
    liftM2 (_++[ p ]++_) (return (qsort ys)) (return (qsort zs))
  ≈⟨ trans >>=-identity-l >>=-identity-l ⟩
    return (qsort (p ∷ xs))
  ∎
  where open ⊇-Reasoning

