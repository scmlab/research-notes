\documentclass{article}
% build by
%  lhs2TeX NonDetQSort.lhs | pdflatex --jobname=NonDetQSort

%if False
\begin{code}
import Data.List hiding (partition)
\end{code}
%endif

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{scalerel}


%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt

%include ../common/Formatting.fmt
%include ../common/Monad.fmt

%let showproofs = True


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\todo}[1]{{\bf todo}: {#1}}

%format m1
%format m2
%format m3
%format f1
%format f2

%format `sse` = "\mathrel{\subseteq}"
%format `spe` = "\mathrel{\supseteq}"

%format `sqse` = "\mathrel{\sqsubseteq}"
%format sqse = "(\sqsubseteq)"
%format `sqpe` = "\mathrel{\sqsupseteq}"
%format sqpe = "(\sqsupseteq)"

%format exists = "\exists"
%format *** = "\times"

\begin{document}

\title{Non-deterministic Quicksort}
\author{Shin-Cheng Mu}
\date{2019}
\maketitle

\paragraph{Specification} {~}\\
\begin{spec}
slowsort = perm >=> filt sorted {-"~~,"-}

perm []      =  return []
perm (x:xs)  =  split xs >>=
                first perm >>= second perm >>=
                return . (++[x]++) {-"~~,"-}

split []      =  return ([],[])
split (x:xs)  =  split xs >>= \(ys,zs) ->
                 return (x:ys, zs) `mplus` return (ys, x:zs) {-"~~."-}
\end{spec}

\begin{spec}
  sorted (xs ++ [x] ++ ys) = sortedL x xs && sortedR x ys {-"~~,"-}
  sortedL x xs  = all (<= x) xs && sorted xs {-"~~,"-}
  sortedR x xs  = all (x <=) xs && sorted xs {-"~~."-}
\end{spec}

\paragraph{Main Skeleton} We derive:
\begin{spec}
   slowsort (x:xs)
=  perm (x:xs) >>= filt sorted
=  split xs >>= first perm >>= second perm >>=
   (return . (++[x]++)) >>= filt sorted
=    {- by \eqref{eq:filt-apply} -}
   split xs >>= first perm >>= second perm >>=
   filt (\(ys,zs) -> sortedL x ys && sortedR x zs) >>=
   (return . (++[x]++))
=  split xs >>= first perm >>= second perm >>=
   filt (sortedL x . fst) >>= filt (sortedR x . snd)
   (return . (++[x]++))
=    {- by \eqref{eq:first-snd} -}
   split xs >>=  first perm >>= filt (sortedL x . fst) >>=
                 second perm >>= filt (sortedR x . snd) >>=
   (return . (++[x]++))
=    {- by \eqref{eq:first-fst} -}
   split xs >>=  first (perm >>= filt (sortedL x))) >>=
                 second (perm >>= filt (sortedR x))) >>=
   (return . (++[x]++))
=    {- by \eqref{eq:first-fst} -}
   split xs >>=  first (filt (all (<= x)) >>= perm >>= filt sorted)) >>=
                 second (filt (all (x <=)) >>= perm >>= filt sorted)) >>=
   (return . (++[x]++))
=  split xs >>= filt (and . (all (<= x) *** all (x <=))) >>=
   first (perm >>= filt sorted) >>=
   second (perm >>= filt sorted) >>=
   (return . (++[x]++))
=  split xs >>= filt (and . (all (<= x) *** all (x <=))) >>=
   first slowsort >>= second slowsort >>=
   (return . (++[x]++)) {-"~~."-}
\end{spec}
Therefore, the following definition of |qsort| satisfies that
|qsort `sqse` slowsort|,
\begin{spec}
qsort []      =  return []
qsort (x:xs)  =  partition x xs >>= first qsort >>= second qsort >>=
                 (return . (++[x]++)) {-"~~,"-}
\end{spec}
if |partition x xs `sqse` split xs >>= filt (and . (all (<= x) *** all (x <=)))|.

Properties used: the following one similar to pure |filter|:
\begin{equation}
|(return . f) >=> filt p = filt (p . f) >=> (return . f)| \mbox{~~,}
\label{eq:filt-apply}
\end{equation}
and some properties about |first| and |second|:
\begin{align}
|first f >=> filt (p . snd)| &=
  |filt (p . snd) >=> first f| \mbox{~~,} \label{eq:first-snd}\\
|first f >=> filt (p . fst)| &=
  |first (f >=> filt p)| \mbox{~~,} \label{eq:first-fst}\\
|first (filt p) >>= second (filt q)| &=
  |filt (and . (p *** q))| \label{eq:first-second-filt}
\end{align}

\paragraph{Quick Select} Define
|qselect n xs = (!!n) <$> qsort xs|.
Note that
\begin{spec}
(ys ++ [x] ++ zs) !! n  | length ys > n   = ys !! n
                        | length ys == n  = x
                        | length ys < n   = zs !! (n - length ys - 1)  {-"~~."-}
\end{spec}
Let |pickCat n x (ys,zs) = (ys ++ [x] ++ zs) !! n|.
\begin{spec}
   partition x xs >>= first qsort >>= second qsort >>=
   (return . (++[x]++)) >>= return (!!n)
=  partition x xs >>= \(ys, zs) ->
   qsort ys >>= \ys' -> qsort zs >>= \zs' ->
   return (pickCat n x (ys',zs'))
=  partition x xs >>= \(ys, zs) ->
   qsort ys >>= \ys' -> qsort zs >>= \zs' ->
   if length ys' > n then return (ys' !! n)
     else if length ys' == n then return x
       else return (zs !! (n - length ys - 1))
=   {- |qsort| length preserving -}
   partition x xs >>= \(ys, zs) ->
   qsort ys >>= \ys' -> qsort zs >>= \zs' ->
   if length ys > n then return (ys' !! n)
     else if length ys == n then return x
       else return (zs' !! (n - length ys - 1))
=  partition x xs >>= \(ys, zs) ->
   if length ys > n then
      qsort ys >>= \ys' -> qsort zs >>= \zs' -> return (ys' !! n)
    else if length ys == n then
      qsort ys >>= \ys' -> qsort zs >>= \zs' -> return x
    else  qsort ys >>= \ys' -> qsort zs >>= \zs' ->
          else return (zs' !! (n - length ys - 1))
=  partition x xs >>= \(ys, zs) ->
   if length ys > n then (!! n) <$> (qsort ys)
    else if length ys == n then return x
    else (!! (n - length ys - 1)) <$> qsort zs
=  partition x xs >= \(ys, zs) ->
   if length ys > n then qselect ys n
    else if length ys == n then return x
    else qselect zs (n - length ys - 1))  {-"~~."-}
\end{spec}

\begin{spec}
qselect []      n  = mzero
qselect (x:xs)  n  =
  partition x xs >>= \(ys, zs) ->
   if length ys > n then qselect ys n
    else if length ys == n then return x
    else qselect zs (n - length ys - 1)  {-"~~."-}
\end{spec}

%if False
\begin{code}
qsort []     = []
qsort (x:xs) =
  let (ys, zs) = partition x xs
  in qsort ys ++ [x] ++ qsort zs

qselect []      n  = undefined
qselect (x:xs)  n  =
  let (ys, zs) = partition x xs in
   if length ys > n then qselect ys n
    else if length ys == n then x
    else qselect zs (n - length ys - 1)

partition x xs = (filter (<x) xs, filter (x <=) xs)
\end{code}
%endif

\paragraph{Partition} Define:
\begin{spec}
partition x xs =  split xs >>= filt (and . (all (<= x) *** all (x <=))) >>=
                  first perm >>= second perm {-"~~."-}
\end{spec}
\end{document}
