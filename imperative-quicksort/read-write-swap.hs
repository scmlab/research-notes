-- axiom
read :: Int -> m a
write :: Int -> a -> m ()

read-write:  read i >>= write i = return ()
write-read:  write i x >> read i = write i x >> return x
write-write: write i x >> write i x' = write i x'
read-read:   read i >>= (\x -> read i >>= (\x' -> f x x')) =
             read i >>= (\x -> f x x)

-- def
m commutes with n if
  m >>= \x -> n >>= \y -> f x y
= n >>= \y -> m >>= \x -> f x y

-- lemma
(return x) commutes with m for every m

-- proof {{{
  return x >>= \x' -> m >>= \y -> f x' y
= m >>= \y -> f x y
= m >>= \y -> return x >>= \x' -> f x' y
-- }}}


-- axiom
(read i) commutes with (read j)

(write i x) commutes with (write j y) if i /= j

(write i x) commutes with (read j) if i /= j


-- def
swap :: Int -> Int -> m ()
swap i j = read i >>= \x -> read j >>= \y -> write i x >> write j y

-- lemma
if m commutes with (read i), (read j), (write i x), (write j y)
then m commutes with (swap i j)

-- lemma
swap i i = return ()

-- proof {{{
  swap i i
= read i >>= \x -> read i >>= \x' -> write i x >> write i x'
= {- read-read -}
  read i >>= \x -> write i x >> write i x
= {- write-write -}
  read i >>= \x -> write i x
= {- read-write -}
  return ()
-- }}}

-- lemma
swap i j = swap j i

-- proof {{{
  swap i j
= read i >>= \xi -> read j >>= \xj -> write i xj >> write j xi
= {- write-commute -}
  read i >>= \xi -> read j >>= \xj -> write j xi >> write i xj
= {- read-commute -}
  read j >>= \xj -> read i >>= \xi -> write j xi >> write i xj
= swap j i
-- }}}

-- lemma (write-swap)
if i /= j then
write i x >> write j y = write i y >> write j x >> swap i j

-- proof {{{
  write i x >> write j y
= write i y >> write j x >> write i x >> write j y
= write i y >> write j x >> return x >>= \xj -> write i xj >> write j y
= write i y >> write j x >> read j >>= \xj -> write i xj >> write j y
= write j x >> write i y >> read j >>= \xj -> write i xj >> write j y
= write j x >> write i y >> return y >>= \xi -> read j >>= \xj -> write i xj >> write j xi
= write j x >> write i y >> read i >>= \xi -> read j >>= \xj -> write i xj >> write j xi
= write i y >> write j x >> read i >>= \xi -> read j >>= \xj -> write i xj >> write j xi
= write i y >> write j x >> swap i j
-- }}}

-- def
readList :: Int -> Nat -> m [a]
readList i 0 = return []
readList i (1+k) = liftM2 (:) (read i) (readList (i + 1) k)

writeList :: Int -> [a] -> m ()
writeList i [] = return ()
writeList i (x:xs) = write i x >> writeList (i + 1) xs

-- lemma
readList i 1 = read i >>= return . (:[])

-- lemma
writeList i [x] = write i x

-- lemma
  readList i k >>= \xs -> f xs (length xs)
= readList i k >>= \xs -> f xs k

-- proof {{{
  readList i 0 >>= \xs -> f xs (length xs)
= return [] >>= \xs -> f xs (length xs)
= f [] (length [])
= f [] 0
= return [] >>= \xs -> f xs 0
= readList i 0 >>= \xs -> f xs 0

  readList i (1+k) >>= \xs -> f xs (length xs)
= read i >>= \x -> readList (i + 1) k >>= \xs -> f (x:xs) (length (x:xs))
= read i >>= \x -> readList (i + 1) k >>= \xs -> f (x:xs) (1 + length xs)
= {- induction -}
  read i >>= \x -> readList (i + 1) k >>= \xs -> f (x:xs) (1 + k)
= readList i (1+k) >>= \xs -> f xs (1+k)
-- }}}

-- lemma (readList-++)
readList i (k+k') = liftM2 (++) (readList i k) (readList (i+k) k')

-- proof {{{
  readList i (0+k')
= readList (i+0) k' >>= \xs' -> xs'
= readList (i+0) k' >>= \xs' -> []++xs'
= return [] >>= \xs -> readList (i+0) k' >>= \xs' -> xs++xs'
= readList i 0 >>= \xs -> readList (i+0) k' >>= \xs' -> xs++xs'
= liftM2 (++) (readList i 0) (readList (i+0) k')
  
  readList i ((1+k)+k')
= readList i (1+(k+k'))
= liftM2 (:) (read i) (readList (i+1) (k+k'))
= {- induction -}
  liftM2 (:) (read i) (liftM2 (++) (readList (i+1) k) (readList (i+1+k) k'))
= read i >>= \x -> readList (i+1) k >>= \xs -> readList (i+1+k) k' >>= \xs' -> x:(xs++xs')
= read i >>= \x -> readList (i+1) k >>= \xs -> readList (i+1+k) k' >>= \xs' -> (x:xs)++xs'
= readList i (1+k) >>= \(x:xs) -> readList (i+1+k) k' >>= \xs' -> (x:xs)++xs'
= liftM2 (++) (readList i (1+k)) (readList (i+(1+k)) k')
-- }}}

-- lemma (writeList-++)
writeList i (xs ++ ys) = writeList i xs >> writeList (i + length xs) ys

-- proof {{{
  writeList i ([] ++ ys)
= return () >> writeList i ([] ++ ys)
= writeList i [] >> writeList (i + length []) ([] ++ ys)

  writeList i ((x:xs) ++ ys)
= writeList i (x:(xs ++ ys))
= write i x >> writeList (i + 1) (xs ++ ys)
= {- induction -}
  write i x >> writeList (i + 1) xs >> writeList (i + 1 + length xs) ys
= writeList i (x:xs) >> writeList (i + 1 + length xs) ys
= writeList i (x:xs) >> writeList (i + length (x:xs)) ys
-- }}}

-- lemma
writeList i (xs ++ [x]) = writeList i xs >> write (i + length xs) x

-- proof {{{
  writeList i (xs ++ [x])
= writeList i xs >> writeList (i + length xs) [x]
= writeList i xs >> write (i + length xs) x
-- }}}

-- lemma
if m commutes with (read j) for i<=j<i+k
then m commutes with (readList i k)

-- proof {{{
  m >>= \x -> readList i 0 >>= \ys -> f x ys
= m >>= \x -> return [] >>= \ys -> f x ys
= return [] >>= \ys -> m >>= \x -> f x ys
= readList i 0 >>= \ys -> m >>= \x -> f x ys

  m >>= \x -> readList i (1+k) >>= \yys -> f x yys
= m >>= \x -> read i >>= \y -> readList (i+1) k >>= \ys -> f x (y:ys)
= {- i<=i && i<i+(1+k) => read i commutes with m -}
  read i >>= \y -> m >>= \x -> readList (i+1) k >>= \ys -> f x (y:ys)
= {- i+1<=j<(i+1)+k => i<=j<i+(1+k), induction -}
  read i >>= \y -> readList (i+1) k >>= \ys -> m >>= \x -> f x (y:ys)
= readList i (1+k) >>= \yys -> m >>= \x -> f x yys
-- }}}

-- lemma
if m commutes with (write j x) for every x, i<=j<i+(length ys)
then m commutes with (writeList i ys)

-- proof {{{
  m >>= \x -> writeList i [] >>= \u -> f x u
= m >>= \x -> return () >>= \u -> f x u
= return () >>= \u -> m >>= \x -> f x u
= writeList i [] >>= \u -> m >>= \x -> f x u

  m >>= \x -> writeList i (y:ys) >>= \u -> f x u
= m >>= \x -> write i y >> writeList (i+1) ys >>= \u -> f x u
= {- i<=i && i<i+length (y:ys) => write i x commutes with m -}
  write i y >> m >>= \x -> writeList (i+1) ys >>= \u -> f x u
= {- i+1<=j<(i+1)+(length ys)=i+length (y:ys) => i<=j<i+length (y:ys), induction -}
  write i y >> writeList (i+1) ys >>= \u -> m >>= \x -> f x u
= writeList i (y:ys) >>= \u -> m >>= \x -> f x u
-- }}}

-- lemma
(read j) commutes with (readList i k)

-- proof {{{
   read j commutes with read j' for every j'
=> read j commutes with read j' for i<=j'<i+k
=> read j commutes with readList i k
-- }}}

-- lemma
(write j x) commutes with (readList i k) if j<i || j>=i+k

-- proof {{{
-- Assume j<i || j>=i+k
   write j x commutes with read j' if j' /= j
=> write j x commutes with read j' for i<=j'<i+k
=> write j x commutes with readList i k
-- }}}

-- lemma
(read j) commutes with (writeList i xs) if j<i || j>=i+(length xs)

-- proof {{{
-- Assume j<i || j>=i+(length xs)
   read j commutes with write j' x if j' /= j
=> read j commutes with write j' x for i<=j'<i+(length xs)
=> read j commutes with writeList i xs
-- }}}

-- lemma
(write j x) commutes with (writeList i xs) if j<i || j>=i+(length xs)

-- proof {{{
   write j x commutes with write j' x' if j' /= j
=> write j x commutes with write j' x' for i<=j'<i+(length xs)
=> write j x commutes with writeList i xs
-- }}}

-- lemma
(readList i k) commutes with (readList j l)

-- proof {{{
   readList i k commutes with read j' for every j'
=> readList i k commutes with read j' for j<=j'<j+l
=> readList i k commutes with readList j l
-- }}}

-- lemma
(writeList i xs) commutes with (writeList j ys) if j>=i+(length xs)

-- proof {{{
-- Assume j>=i+(length xs)
   writeList i xs commutes with write j' x' if j'<i || j'>=i+(length xs)
=> {- j>=i+(length xs) -}
   writeList i xs commutes with write j' x' for j=<j'
=> writeList i xs commutes with write j' x' for j=<j'<j+(length ys)
=> writeList i xs commutes with writeList j ys
-- }}}

-- lemma
(writeList i xs) commutes with (readList j l) if j>=i+(length xs) || i>=j+l

-- proof {{{
-- Assume j>=i+(length xs)
   writeList i xs commutes with read j' if j'<i || j'>=i+(length xs)
=> {- j>=i+(length xs) -}
   writeList i xs commutes with read j' for j=<j'
=> writeList i xs commutes with read j' for j=<j'<j+l
=> writeList i xs commutes with readList j l

-- Assume i>=j+l
   writeList i xs commutes with read j' if j'<i || j'>=i+(length xs)
=> {- j+l<=i -}
   writeList i xs commutes with read j' for j'<j+l
=> writeList i xs commutes with read j' for j<j'<j+l
=> writeList i xs commutes with readList j l
-- }}}

-- lemma (readList-writeList)
readList i k >>= writeList i = return ()

-- proof {{{
  readList i 0 >>= writeList i
= return [] >>= writeList i
= writeList i []
= return ()

  readList i (1+k) >>= writeList i
= read i >>= \x -> readList (i+1) >>= \xs -> writeList i (x:xs)
= read i >>= \x -> readList (i+1) >>= \xs -> write i x >> writeList (i+1) xs
= {- i < i + 1 => write i x commutes with readList (i+1) xs  -}
  read i >>= \x -> write i x >> readList (i+1) >>= \xs -> writeList (i+1) xs
= {- induction -}
  read i >>= \x -> write i x >> return ()
= {- read-write -}
  return () >> return ()
= return ()
-- }}}

-- lemma (writeList-readList)
writeList i xs >> readList i (length xs) = writeList i xs >> return xs

-- proof {{{
  writeList i [] >> readList i (length [])
= writeList i [] >> readList i 0
= writeList i [] >> return []

  writeList i (x:xs) >> readList i (length (x:xs))
= writeList i (x:xs) >> readList i (1 + length xs)
= write i x >> writeList (i+1) xs >>
  read i >>= \x' -> readList (i+1) (length xs) >>= \xs' -> return (x':xs')
= {- i < i+1 => read i commutes with writeList (i+1) xs -}
  write i x >> read i >>= \x' -> writeList (i+1) xs >>
  readList (i+1) (length xs) >>= \xs' -> return (x':xs')
= {- induction -}
  write i x >> read i >>= \x' ->
  writeList (i+1) xs >> return xs >>= \xs' -> return (x':xs')
= write i x >> read i >>= \x' -> writeList (i+1) xs >> return (x':xs)
= {- write-read -}
  write i x >> return x >>= \x' -> writeList (i+1) xs >> return (x':xs)
= write i x >> writeList (i+1) xs >> return (x:xs)
= write i (x:xs) >> return (x:xs)
-- }}}

-- lemma (writeList-writeList)
if length xs == length xs' then
writeList i xs >> writeList i xs' = writeList i xs'

-- proof {{{
  writeList i [] >> writeList i []
= return () >> writeList i []
= writeList i []

  writeList i (x:xs) >> writeList i (x':xs')
= write i x >> writeList (i+1) xs >> write i x' >> writeList (i+1) xs'
= {- i < i+1 => write i x' commutes with writeList (i+1) xs -}
  write i x >> write i x' >> writeList (i+1) xs >> writeList (i+1) xs'
= {- induction -}
  write i x >> write i x' >> writeList i xs'
= {- write-write -}
  write i x' >> writeList i xs'
= writeList i (x':xs')
-- }}}

-- lemma (readList-readList)
  readList i k >>= \xs -> readList i k >>= \xs' -> f xs xs'
= readList i k >>= \xs -> f xs xs

-- proof {{{
  readList i 0 >>= \xs -> readList i 0 >>= \xs' -> f xs xs'
= return [] >>= \xs -> return [] >>= \xs' -> f xs xs'
= f [] []
= return [] >>= \xs -> f xs xs
= readList i 0 >>= \xs -> f xs xs

  readList i (1+k) >>= \xs -> readList i (1+k) >>= \xs' -> f xs xs'
= read i >>= \x -> readList (i+1) k >>= \xs ->
  read i >>= \x' -> readList (i+1) k >>= \xs' -> f (x:xs) (x':xs')
= {- i < i+1 => read i commutes with readList (i+1) -}
  read i >>= \x -> read i >>= \x' -> readList (i+1) k >>= \xs ->
  readList (i+1) k >>= \xs' -> f (x:xs) (x':xs')
= {- induction -}
  read i >>= \x -> read i >>= \x' -> readList (i+1) k >>= \xs ->
  f (x:xs) (x':xs)
= {- read-read -}
  read i >>= \x -> readList (i+1) k >>= \xs -> f (x:xs) (x:xs)
= readList i (1+k) >>= \xs -> f xs
-- }}}

-- vim:foldmethod=marker:
