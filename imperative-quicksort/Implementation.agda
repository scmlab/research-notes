module Implementation where

open import Algebra
open import Algebra.FunctionProperties
open import Data.Bool using (if_then_else_)
open import Data.Empty
open import Data.List
open import Data.List.Properties
open import Data.List.Relation.Binary.BagAndSetEquality renaming (>>=-cong to >>=ᴸ-cong)
open import Data.Nat
open import Data.Product using (_×_; _,_; uncurry)
open import Data.Unit using (⊤; tt)
open import Function
open import Relation.Binary
open import Relation.Binary.PropositionalEquality as P using (_≡_; _≢_)
open import Relation.Nullary
open import Relation.Nullary.Decidable

import Data.List.Categorical as ListMonad
import Relation.Binary.Reasoning.MultiSetoid as SetoidReasoning
import Monad as M

private
  variable
    A B C : Set
    i j : ℕ
  postulate
    ext : {f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g

private
  infixl 3 _>>=ᴸ_
  _>>=ᴸ_ : List A → (A → List B) → List B
  _>>=ᴸ_ = flip concatMap

  >>=ᴸ-identity-l : (x : A) (f : A → List B) → ([ x ] >>=ᴸ f) ≡ f x
  >>=ᴸ-identity-l = ListMonad.MonadProperties.left-identity

  >>=ᴸ-identity-r : (xs : List A) → (xs >>=ᴸ [_]) ≡ xs
  >>=ᴸ-identity-r = ListMonad.MonadProperties.right-identity

  >>=ᴸ-assoc : (xs : List A) (f : A → List B) (g : B → List C)
             → (xs >>=ᴸ f >>=ᴸ g) ≡ (xs >>=ᴸ λ x → f x >>=ᴸ g)
  >>=ᴸ-assoc xs f g = P.sym (ListMonad.MonadProperties.associative xs f g)

  >>=ᴸ-zero-r : (xs : List A) → (xs >>=ᴸ λ _ → ([] {_} {B})) ≡ []
  >>=ᴸ-zero-r = ListMonad.MonadProperties.right-zero


  >>=ᴸ-distrib-l-++ : (xs ys : List A) (f : A → List B)
                    → ((xs ++ ys) >>=ᴸ f) ≡ ((xs >>=ᴸ f) ++ (ys >>=ᴸ f))
  >>=ᴸ-distrib-l-++ = ListMonad.MonadProperties.right-distributive

  >>=ᴸ-distrib-r-++ : (xs : List A) (f g : A → List B)
                    → (xs >>=ᴸ λ x → f x ++ g x) ∼[ set ] (xs >>=ᴸ f) ++ (xs >>=ᴸ g)
  >>=ᴸ-distrib-r-++ xs f g = bag-=⇒ (>>=-left-distributive xs {f} {g})

  module _ {S : Set} where
    insert : ℕ → S → (ℕ → S) → (ℕ → S)
    insert i si s j = if ⌊ i ≟ j ⌋ then si else s j

    lookup-insert : (i : ℕ) (s : ℕ → S) → insert i (s i) s ≡ s
    lookup-insert i s = ext go where
      go : (j : ℕ) → insert i (s i) s j ≡ s j
      go j with i ≟ j
      go j | yes P.refl = P.refl
      go j | no _ = P.refl

    insert-lookup : (i : ℕ) (si : S) (s : ℕ → S) → (insert i si s) i ≡ si
    insert-lookup i si s with i ≟ i
    insert-lookup i si s | yes P.refl = P.refl
    insert-lookup i si s | no i≢i = ⊥-elim (i≢i P.refl)

    insert-insert : (i : ℕ) (si si' : S) (s : ℕ → S) → insert i si' (insert i si s) ≡ insert i si' s
    insert-insert i si si' s = ext go where
      go : (j : ℕ) → insert i si' (insert i si s) j ≡ insert i si' s j
      go j with i ≟ j
      go j | yes P.refl = P.refl
      go j | no _ = P.refl


    insert-insert-comm : (i j : ℕ) (si sj : S) (s : ℕ → S)
                       → i ≢ j → insert j sj (insert i si s) ≡ insert i si (insert j sj s)
    insert-insert-comm i j si sj s i≢j = ext go where
      go : (k : ℕ) → insert j sj (insert i si s) k ≡ insert i si (insert j sj s) k
      go k with j ≟ k | i ≟ k
      go k | yes j≡k | yes i≡k = ⊥-elim (i≢j (P.trans i≡k (P.sym j≡k)))
      go k | yes _ | no _ = P.refl
      go k | no _ | yes _ = P.refl
      go k | no _ | no _ = P.refl

    insert-lookup-≢ : (i j : ℕ) (sj : S) (s : ℕ → S) → i ≢ j → insert j sj s i ≡ s i
    insert-lookup-≢ i j sj s i≢j with j ≟ i
    insert-lookup-≢ i j sj s i≢j | yes j≡i = ⊥-elim (i≢j (P.sym j≡i))
    insert-lookup-≢ i j sj s i≢j | no _ = P.refl

M : Set → Set → Set
M S A = (ℕ → S) → List (A × (ℕ → S))

module _ {S : Set} where
  infix 2 _≈_
  _≈_ : M S A → M S A → Set
  m ≈ n =  ∀ s → m s ∼[ set ] n s

  refl : Reflexive {_} {M S A} _≈_
  refl = λ _ → Setoid.refl ([ set ]-Equality _)

  sym : Symmetric {_} {M S A} _≈_
  sym m≈n = λ s → Setoid.sym ([ set ]-Equality _) (m≈n s)

  trans : Transitive {_} {M S A} _≈_
  trans m≈n n≈o = λ s → Setoid.trans ([ set ]-Equality _) (m≈n s) (n≈o s)

  isEquivalence : IsEquivalence {_} {_} {M S A} _≈_
  isEquivalence =
    record { refl = refl ; sym = sym ; trans = trans }

  setoid : {A : Set} → Setoid _ _
  setoid {A} =
    record { Carrier = M S A ; _≈_ = _≈_ ; isEquivalence = isEquivalence }

  return : A → M S A
  return x = λ s → [ (x , s) ]


  infixl 3 _>>=_ _>>_
  _>>=_ : M S A → (A → M S B) → M S B
  m >>= f = λ s → m s >>=ᴸ uncurry f

  _>>_ : M S A → M S B → M S B
  m >> n = m >>= λ _ → n

  >>=-cong : {m n : M S A} {f g : A → M S B}
           → m ≈ n → (∀ x → f x ≈ g x) → m >>= f ≈ n >>= g
  >>=-cong m≈n f≈g = λ s → >>=ᴸ-cong (m≈n s) (λ { (x , s) → f≈g x s })

  >>=-identity-l : {x : A} {f : A → M S B} → return x >>= f ≈ f x
  >>=-identity-l {_} {_} {x} {f} = λ s → reflexive (>>=ᴸ-identity-l (x , s) (uncurry f))
    where open Setoid ([ set ]-Equality _)

  >>=-identity-r : {m : M S A} → m >>= return ≈ m
  >>=-identity-r = λ s → reflexive (>>=ᴸ-identity-r _)
    where open Setoid ([ set ]-Equality _)

  >>=-assoc : {m : M S A} {f : A → M S B} {g : B → M S C}
            → m >>= f >>= g ≈ m >>= (λ x → f x >>= g)
  >>=-assoc {_} {_} {_} {m} {f} {g} = λ s → reflexive (>>=ᴸ-assoc (m s) (uncurry f) (uncurry g))
    where open Setoid ([ set ]-Equality _)

  instance
    monad : M.Monad (M S)
    monad = record
      { _≈_ = _≈_
      ; isEquivalence = isEquivalence
      ; return = return
      ; _>>=_ = _>>=_
      ; >>=-cong = >>=-cong
      ; >>=-identity-l = λ {A} {B} {x} {f} →
          >>=-identity-l {A} {B} {x} {f}
      ; >>=-identity-r = >>=-identity-r
      ; >>=-assoc = λ {A} {B} {C} {m} {f} {g} →
          >>=-assoc {A} {B} {C} {m} {f} {g}
      }

  ∅ : M S A
  ∅ = λ s → []

  infixl 3 _‖_
  _‖_ : M S A → M S A → M S A
  m ‖ n = λ s → m s ++ n s

  ‖-cong : Congruent₂ {_} {_} {M S A} _≈_ _‖_
  ‖-cong m₁≈m₂ n₁≈n₂ = λ s → ++-cong (m₁≈m₂ s) (n₁≈n₂ s)

  ‖-identity-l : {m : M S A} → ∅ ‖ m ≈ m
  ‖-identity-l = refl

  ‖-assoc : {m n o : M S A} → (m ‖ n) ‖ o ≈ m ‖ (n ‖ o)
  ‖-assoc {_} {m} {n} {o} = λ s → assoc (m s) (n s) (o s)
    where open CommutativeMonoid (commutativeMonoid set _)

  ‖-comm : {m n : M S A} → m ‖ n ≈ n ‖ m
  ‖-comm {_} {m} {n} = λ s → comm (m s) (n s)
    where open CommutativeMonoid (commutativeMonoid set _)

  ‖-idempotent : {m : M S A} → m ‖ m ≈ m
  ‖-idempotent {_} {m} = λ s → ++-idempotent (m s)

  >>=-zero-l : {f : A → M S B} → ∅ >>= f ≈ ∅
  >>=-zero-l = refl

  >>=-zero-r : {m : M S A} → (m >>= λ _ → (∅ {B})) ≈ ∅
  >>=-zero-r {_} {_} {m} = λ s → reflexive (>>=ᴸ-zero-r (m s))
    where open Setoid ([ set ]-Equality _)

  >>=-distrib-l-‖ : {m n : M S A} {f : A → M S B}
                      → (m ‖ n) >>= f ≈ (m >>= f) ‖ (n >>= f)
  >>=-distrib-l-‖ {_} {_} {m} {n} {f} = λ s →
    reflexive (>>=ᴸ-distrib-l-++ (m s) (n s) (λ { (x , s') → f x s' }))
    where open Setoid ([ set ]-Equality _)

  >>=-distrib-r-‖ : {m : M S A} {f g : A → M S B}
                      → (m >>= λ x → f x ‖ g x) ≈ (m >>= f) ‖ (m >>= g)
  >>=-distrib-r-‖ {_} {_} {m} {f} {g} = λ s →
    >>=ᴸ-distrib-r-++ (m s) (λ { (x , s') → f x s' }) (λ { (x , s') → g x s' })


  instance
    monadPlus : M.MonadPlus (M S)
    monadPlus = record
      { ∅ = ∅
      ; _‖_ = _‖_
      ; ‖-cong = ‖-cong
      ; ‖-identity-l = ‖-identity-l
      ; ‖-assoc = λ {A} {m} {n} {o} →
          ‖-assoc {A} {m} {n} {o}
      ; ‖-comm = λ {A} {m} {n} →
          ‖-comm {A} {m} {n}
      ; ‖-idempotent = λ {A} {m} →
          ‖-idempotent {A} {m}
      ; >>=-zero-l = λ {A} {B} {f} →
          >>=-zero-l {A} {B} {f}
      ; >>=-zero-r = λ {A} {B} {m} →
          >>=-zero-r {A} {B} {m}
      ; >>=-distrib-l-‖ = λ {A} {B} {m} {n} {f} →
          >>=-distrib-l-‖ {A} {B} {m} {n} {f}
      ; >>=-distrib-r-‖ = λ {A} {B} {m} {f} {g} →
          >>=-distrib-r-‖ {A} {B} {m} {f} {g}
      }


  read : ℕ → M S S
  read i = λ s → [ (s i , s) ]

  write : ℕ → S → M S ⊤
  write i si = λ s → [ (tt , insert i si s) ]

  read-write : read i >>= write i ≈ return tt
  read-write {i} = λ s →
    begin⟨ [ set ]-Equality _ ⟩
      (read i >>= write i) s
    ≡⟨⟩
      [ (tt , insert i (s i) s) ]
    ≡⟨ P.cong [_] (P.cong₂ _,_ P.refl (lookup-insert i s)) ⟩
      [ (tt , s) ]
    ≡⟨⟩
      (return tt) s
    ∎
    where open SetoidReasoning

  write-read : {si : S} → write i si >> read i ≈ write i si >> return si
  write-read {i} {si} = λ s →
    begin⟨ [ set ]-Equality _ ⟩
      (write i si >> read i) s
    ≡⟨⟩
      [ ((insert i si s) i , insert i si s) ]
    ≡⟨ P.cong [_] (P.cong₂ _,_ (insert-lookup i si s) P.refl) ⟩
      [ (si , insert i si s) ]
    ≡⟨⟩
      (write i si >> return si) s
    ∎
    where open SetoidReasoning

  write-write : {si si' : S} → write i si >> write i si' ≈ write i si'
  write-write {i} {si} {si'} = λ s →
    begin⟨ [ set ]-Equality _ ⟩
      (write i si >> write i si') s
    ≡⟨⟩
      [ (tt , insert i si' (insert i si s)) ]
    ≡⟨ P.cong [_] (P.cong₂ _,_ P.refl (insert-insert i si si' s)) ⟩
      [ (tt , insert i si' s) ]
    ≡⟨⟩
      (write i si') s
    ∎
    where open SetoidReasoning

  read-read : {f : S → S → M S A} →
    (read i >>= λ si → read i >>= λ si' → f si si') ≈ (read i >>= λ si → f si si)
  read-read {_} {i} {f} = λ s →
    begin⟨ [ set ]-Equality _ ⟩
      (read i >>= λ si → read i >>= λ si' → f si si') s
    ≡⟨⟩
      (f (s i) (s i) s ++ []) ++ []
    ≡⟨ ++-identityʳ _ ⟩
      f (s i) (s i) s ++ []
    ≡⟨⟩
      (read i >>= λ si → f si si) s
    ∎
    where open SetoidReasoning

  read-read-comm : i ≢ j → M.>>=-Commutative (read i) (read j)
  read-read-comm i≢j = refl

  write-write-comm : {si sj : S} → i ≢ j → M.>>=-Commutative (write i si) (write j sj)
  write-write-comm {i} {j} {si} {sj} i≢j {_} {m} = λ s →
    begin⟨ [ set ]-Equality _ ⟩
      (write i si >> (write j sj >> m)) s
    ≡⟨⟩
      (m (insert j sj (insert i si s)) ++ []) ++ []
    ≡⟨ P.cong (λ s' → (m s' ++ []) ++ []) (insert-insert-comm i j si sj s i≢j) ⟩
      (m (insert i si (insert j sj s)) ++ []) ++ []
    ≡⟨⟩
      (write j sj >> (write i si >> m)) s
    ∎
    where open SetoidReasoning

  read-write-comm : {sj : S} → i ≢ j → M.>>=-Commutative (read i) (write j sj)
  read-write-comm {i} {j} {sj} i≢j {_} {f} = λ s →
    begin⟨ [ set ]-Equality _ ⟩
      (read i >>= λ x → write j sj >> f {x}) s
    ≡⟨⟩
      (f {s i} (insert j sj s) ++ []) ++ []
    ≡⟨ P.cong (λ si → (f {si} _ ++ []) ++ []) (P.sym (insert-lookup-≢ i j sj s i≢j)) ⟩
      (f {(insert j sj s) i} (insert j sj s) ++ []) ++ []
    ≡⟨⟩
      (write j sj >> (read i >>= λ x → f {x})) s
    ∎
    where open SetoidReasoning

  instance
    monadArr : M.MonadArr S (M S)
    monadArr = record
      { read = read
      ; write = write
      ; read-write = λ {i} → read-write {i}
      ; write-read = λ {i} {s} → write-read {i} {s}
      ; write-write = λ {i} {s} {s'} → write-write {i} {s} {s'}
      ; read-read = λ {A} {i} {f} → read-read {A} {i} {f}
      ; read-read-comm = read-read-comm
      ; write-write-comm = write-write-comm
      ; read-write-comm = read-write-comm
      }
