module State where

open import Data.Nat
-- open import Data.String
-- open import Data.Product
open import Function using (_∘_; _$_)

-- 𝕍 : Set
-- 𝕍 = String

data Op : Set where
  PUT : (n : ℕ) → Op
  GET : Op

data Expr : Set where
  lit : (n : ℕ) → Expr
  add : (a : Expr) → (b : Expr) → Expr
  op  : Op → Expr

data Domain : Set where
  lit : (n : ℕ) → Domain
  op  : (x : Op) → (k : ℕ → Domain) → Domain

lift : (ℕ → Domain) → Domain → Domain
lift f (lit n) = f n
lift f (op x k) = op x (lift f ∘ k)

ℰ : Expr → Domain
ℰ (lit n) = lit n
ℰ (add a b) = lift (λ a' → lift (λ b' → lit (a' + b')) (ℰ b)) (ℰ a)
ℰ (op x) = op x lit


-- record Env : Set where
--   constructor env
--   field
--     state : ℕ


-- without Handlers
module Free where

  open import Relation.Binary.PropositionalEquality
  open ≡-Reasoning
  open import Data.Nat.Properties

  postulate
      extensionality :
          ∀ {a b} {A : Set a} {B : Set b} {f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g

  -- GET + 1 ≡ 1 + GET
  module Thm10 where

    theorem : ℰ (add (op GET) (lit 1)) ≡ ℰ (add (lit 1) (op GET))
    theorem = cong (op GET) (extensionality (λ x → cong lit $ (
      begin
          x + 1
      ≡⟨ +-suc x 0 ⟩
          suc (x + 0)
      ≡⟨ cong suc (+-identityʳ x) ⟩
          suc x
      ∎
      )))

Env : Set
Env = Op → (ℕ → ℕ) → ℕ

hdl : Env → Domain → ℕ
hdl ρ (lit  n) = n
hdl ρ (op x k) = ρ x (hdl ρ ∘ k)

-- with Handlers
module NonFree where

  open import Relation.Binary.PropositionalEquality
  open ≡-Reasoning
  open import Data.Nat.Properties

  postulate
      extensionality :
          ∀ {a b} {A : Set a} {B : Set b} {f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g

  -- Given a Environment
  ρ :  ℕ → Env
  ρ s (PUT n) f = f n
  ρ s GET     f = f s

  -- PUT i + GET ≡ PUT i + i
  module Add where

    theorem : ∀ init n → hdl (ρ init) (ℰ (add (op (PUT n)) (op GET))) ≡ hdl (ρ init) (ℰ (add (op (PUT n)) (lit n)))
    theorem init n =
      begin
          hdl (ρ init) (ℰ (add (op (PUT n)) (op GET)))
      ≡⟨ refl ⟩
          hdl (ρ init) (lift (λ n' → lift (λ m' → lit (n' + m')) (lit init)) (lit n))
      ≡⟨ refl ⟩
          n + init

      ≡⟨ {!  !} ⟩
          n + n
          -- hdl (ρ init) (ℰ (add (op GET) (lit n)))
      ∎
