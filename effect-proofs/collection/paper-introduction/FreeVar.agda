module FreeVar where

open import Data.Nat
open import Data.String
open import Data.Product
open import Function using (_∘_; _$_)


𝕍 : Set
𝕍 = String

data Expr : Set where
  lit : (n : ℕ) → Expr
  var : (x : 𝕍) → Expr
  add : (e₁ : Expr) → (e₂ : Expr) → Expr

data Domain : Set where
  lit : (n : ℕ) → Domain
  var : (x : 𝕍) → (k : ℕ → Domain) → Domain

lift : (ℕ → Domain) → Domain → Domain
lift f (lit n)   = f n                            --  (8)
lift f (var x k) = var x (lift f ∘ k)             --  (9)

infixl 5 _>>=_
_>>=_ : Domain → (ℕ → Domain) → Domain
_>>=_ x f = lift f x

ℰ : Expr → Domain
ℰ (lit n)     = lit n         -- (5)
ℰ (var x)     = var x lit     -- (6)
ℰ (add e₁ e₂) = lift (λ v₁ → lift (λ v₂ → lit (v₁ + v₂)) (ℰ e₂)) (ℰ e₁)
-- ℰ (add e₁ e₂) = do            -- (7)
--   v₁ ← ℰ e₁
--   v₂ ← ℰ e₂
--   lit (v₁ + v₂)

-- without Handlers
module Free where

  open import Relation.Binary.PropositionalEquality
  open ≡-Reasoning
  open import Data.Nat.Properties

  postulate
      extensionality :
          ∀ {a b} {A : Set a} {B : Set b} {f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g

  -- x + 1 ≡ 1 + x
  module Eq4 where

    theorem : (x : 𝕍) → ℰ (add (var x) (lit 1)) ≡ ℰ (add (lit 1) (var x))
    theorem x =
      begin
          var x (λ z → lit (z + 1))
      ≡⟨ cong (var x) (extensionality $ λ z → cong lit $
          begin
              z + 1
          ≡⟨ +-suc z 0 ⟩
              suc (z + zero)
          ≡⟨ cong suc (+-identityʳ z) ⟩
              suc z
          ∎
      ) ⟩
          var x (λ z → lit (suc z))
      ∎

  -- e + i ≡ i + e
  module Eq10-1 where

    open import Relation.Binary.PropositionalEquality
    open ≡-Reasoning
    open import Data.Nat.Properties

    theorem : (e : Expr) (i : ℕ) → ℰ (add e (lit i)) ≡ ℰ (add (lit i) e)
    theorem e i = cong (λ x → lift x (ℰ e)) (extensionality λ x → cong lit (+-comm x i))

  -- associativity, (e₁ + e₂) + e₃ ≡ e₁ + (e₂ + e₃)
  module Eq10-2 where

    open import Relation.Binary.PropositionalEquality
    open ≡-Reasoning
    open import Data.Nat.Properties

    inc : ℕ → Domain → Domain
    inc k = lift (λ x → lit (k + x))

    _⊕_ : Domain → Domain → Domain
    m ⊕ n = lift (λ v₁ → lift (λ v₂ → lit (v₁ + v₂)) (n)) (m)

    lemma1 : ∀ m n o → inc (m + n) o ≡ inc m (inc n o)
    lemma1 m n (lit o)   = cong lit (+-assoc m n o)
    lemma1 m n (var x k) = cong (var x) (extensionality (λ w → lemma1 m n (k w)))

    lemma2 : ∀ m n o → (inc m n) ⊕ o ≡ inc m (n ⊕ o)
    lemma2 m (lit n) o   = lemma1 m n o
    lemma2 m (var x k) o = cong (var x) (extensionality (λ w → lemma2 m (k w) o))

    lemma3 : ∀ m n o p → ((inc m n) ⊕ o) ⊕ p ≡ (inc m n) ⊕ (o ⊕ p)
    lemma3 m (lit n) o p   = lemma2 (m + n) o p
    lemma3 m (var x k) o p = cong (var x) (extensionality (λ w → lemma3 m (k w) o p))

    lemma4 : ∀ m n o p → ((m ⊕ n) ⊕ o) ⊕ p ≡ (m ⊕ n) ⊕ (o ⊕ p)
    lemma4 (lit m)   (lit n)   (lit o)   p =                                     lemma1 (m + n)       o p
    lemma4 (lit m)   (lit n)   (var x k) p = cong (var x) (extensionality (λ w → lemma2 (m + n) (k w)   p))
    lemma4 (lit m)   (var x k) o         p = cong (var x) (extensionality (λ w → lemma3 m       (k w) o p))
    lemma4 (var x k) n         o         p = cong (var x) (extensionality (λ w → lemma4 (k w)   n     o p))

    -- associativity
    theorem : (a b c : Expr) → ℰ (add (add a b) c) ≡ ℰ (add a (add b c))
    theorem (lit m)     b c = lemma2 m (ℰ b) (ℰ c)
    theorem (var x)     b c = cong (var x) (extensionality (λ w → lemma2 w (ℰ b) (ℰ c)))
    theorem (add a₁ a₂) b c = lemma4 (ℰ a₁) (ℰ a₂) (ℰ b) (ℰ c)


Env : Set
Env = 𝕍 → ℕ

hdl : Env → Domain → ℕ
hdl ρ (lit n)   = n
hdl ρ (var x k) = hdl ρ (k (ρ x))

run : Env → Expr → ℕ
run ρ expr = hdl ρ (ℰ expr)


-- with Handlers
module Handler where


  open import Relation.Binary.PropositionalEquality
  open ≡-Reasoning
  open import Data.Nat.Properties

  postulate
      extensionality :
          ∀ {a b} {A : Set a} {B : Set b} {f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g

  -- due to Zong-Ru Jiang
  lemma : ∀ env f d → hdl env (lift f d) ≡ hdl env (f (hdl env d))
  lemma env f (lit n)   = refl
  lemma env f (var x k) = lemma env f (k (env x))

  -- x + y ≡ y + x
  module Comm where

    open import Relation.Binary.PropositionalEquality
    open ≡-Reasoning
    open import Data.Nat.Properties

    -- due to Shin-Chen Mu
    add-+ : (env : Env) (a b : Expr) → run env (add a b) ≡ run env a + run env b
    add-+ env a b =
      begin
          run env (add a b)
      ≡⟨ refl ⟩
          hdl env (lift (λ z → lift (λ z₁ → lit (z + z₁)) (ℰ b)) (ℰ a))
      ≡⟨ lemma env (λ z → lift (λ z₁ → lit (z + z₁)) (ℰ b)) (ℰ a) ⟩
          hdl env (lift (λ z → lit (hdl env (ℰ a) + z)) (ℰ b))
      ≡⟨ lemma env (λ z → lit (hdl env (ℰ a) + z)) (ℰ b) ⟩
        hdl env (ℰ a) + hdl env (ℰ b)
      ≡⟨ refl ⟩
        run env a + run env b
      ∎

    theorem : (env : Env) (a b : Expr) → run env (add a b) ≡ run env (add b a)
    theorem env a b =
      begin
          run env (add a b)
      ≡⟨ add-+ env a b ⟩
          run env a + run env b
      ≡⟨ +-comm (run env a) (run env b) ⟩
          run env b + run env a
      ≡⟨ sym (add-+ env b a) ⟩
          run env (add b a)
      ∎
