module E where

module State where

open import Data.Bool renaming (T to Tru)
open import Data.String using (String; primStringEquality)
open import Data.Nat
open import Function using (_∘_; _$_)


𝕍 : Set
𝕍 = String

data Req : Set where
  -- VAR : (x : 𝕍) → Req
  PUT : Req
  GET : Req

data Val : Set where
  const : (n : ℕ) → Val
  -- add : Val
  var : (v : 𝕍) → Val
  abs : (v : 𝕍) → Val → Val
  app : Val → Val → Val
  -- add : (a : Val) → (b : Val) → Val
  op  : (r : Req) → Val

-- the semantics domain
data Domain : Set where
  V : (n : ℕ) → Domain
  R : (r : Req) → (k : ℕ → Domain) → Domain

lift : (ℕ → Domain) → Domain → Domain
lift f (V n) = f n
lift f (R r k) = R r (lift f ∘ k)

module Typing where
  open import Data.List

  infixr 12 _⟶_
  data Type : Set where
    Int : Type
    _⟶_ : (A : Type) → (B : Type) → Type  -- pure
    _o→_ : (A : Type) → (B : Type) → Type  -- effectful

  -- typing judgement

  infix 11 _∶_
  data Judgement : Set where
      _∶_ : Val → Type → Judgement

  Context : Set
  Context = List Judgement

  open import Data.Product using (_×_; _,_)
  open import Membership Judgement

  infix 3 _v⊢_∶_
  data _v⊢_∶_ : Context → Val → Type → Set where

    const : ∀ {Γ n}
        --------------------------
        → Γ v⊢ const n ∶ Int

    -- add : ∀ {Γ}
    --     --------------------------
    --     → Γ v⊢ add ∶ Int ⟶ Int ⟶ Int

    var : ∀ {Γ x A}
        → var x ∶ A ∈ Γ
        --------------------------
        → Γ v⊢ var x ∶ A


    app : ∀ {Γ f x A B}
        → Γ v⊢ f ∶ A ⟶ B
        → Γ v⊢ x ∶ A
        --------------------------
        → Γ v⊢ app f x ∶ B

    abs : ∀ {Γ x v A B}
        → var v ∶ A ∷ Γ v⊢ x ∶ B
        --------------------------
        → Γ v⊢ abs v x ∶ A ⟶ B


    op-GET : ∀ {Γ}
        --------------------------
        → Γ v⊢ op GET ∶ Int o→ Int

    op-PUT : ∀ {Γ}
        --------------------------
        → Γ v⊢ op PUT ∶ Int o→ Int

  -- module Example where

  open import Data.Sum using (_⊎_; inj₁; inj₂)




  𝒯[_] : Type → Set


  data ℛ[_] : Type → Set where
    V : {T : Type} → 𝒯[ T ] → ℛ[ T ]
    R : {!   !}

  𝒯[ Int ]     = ℕ
  𝒯[ A ⟶ B ] = 𝒯[ A ] → 𝒯[ B ]
  𝒯[ A o→ B ]  = 𝒯[ A ] → ℛ[ B ]


  isVar : Val → Bool
  isVar (var v) = true
  isVar _       = false

  data Assignment : Judgement → Set where
    _≔_    : {T : Type} (v : 𝕍) → (value : 𝒯[ T ]) → Assignment (var v ∶ T)
    others : {v : Val} {T : Type} → Tru (not (isVar v)) → Assignment (v ∶ T)

  data Env : Context → Set where
      [] : Env []
      _∷_ : ∀ {Γ J} → Assignment J → Env Γ → Env (J ∷ Γ)

  open import Membership.Core hiding (_∈_)
  open import Relation.Binary.PropositionalEquality using (refl)
  lookupEnv : ∀ {Γ x T} → Env Γ → var x ∶ T ∈ Γ → 𝒯[ T ]
  lookupEnv [] ()
  lookupEnv ((_ ≔ value) ∷ env) (here refl) = value
  lookupEnv (others ()   ∷ env) (here refl)
  lookupEnv (_           ∷ env) (there P)   = lookupEnv env P


  ℰ[_v⊢_∶_][_]_ : ∀ Γ x T → Γ v⊢ x ∶ T → Env Γ → 𝒯[ T ]
  ℰ[ Γ v⊢ const n ∶ .Int       ][ const             ] ρ
    = n

  ℰ[ Γ v⊢ var v   ∶ T          ][ var P             ] ρ
    = lookupEnv ρ P

  ℰ[ Γ v⊢ abs v x ∶ .(_ ⟶ _) ][ abs {A = A} {B} D ] ρ
    = λ value → ℰ[ var v ∶ A ∷ Γ v⊢ x ∶ B ][ D ] (v ≔ value ∷ ρ)

  ℰ[ Γ v⊢ app f x ∶ T          ][ app {A = A} P Q   ] ρ
    = (ℰ[ Γ v⊢ f ∶ A ⟶ T ][ P ] ρ) (ℰ[ Γ v⊢ x ∶ A ][ Q ] ρ)

  ℰ[ Γ v⊢ op r    ∶ T          ][ D                 ] ρ = {!   !}
