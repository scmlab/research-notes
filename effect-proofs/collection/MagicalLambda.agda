-- 2019/Feb/22 
module MagicalLambda where

open import Data.Nat
open import Data.List hiding ([_])
open import Data.Product

postulate
    𝕍 : Set

Env : Set
Env = List (𝕍 × ℕ)


open import Relation.Binary.PropositionalEquality hiding ([_])

postulate
    lookupEnv : Env → 𝕍 → ℕ


data E : Set where
  var : 𝕍 → E
  add : E → E → E
  const : ℕ → E

data S : Set where
  return : E → S
  _>>=_ : S → (𝕍 × S) → S
  GET : (𝕍 × S) → S
  PUT : E → S → S

data Op : Set where
  GET : Op
  PUT : ℕ → Op

data Domain : Set where
  V : ℕ → Domain
  R : Op → (ℕ → Domain) → Domain

E[_] : E → Env → ℕ
E[ var x   ] ρ = lookupEnv ρ x
E[ add a b ] ρ = E[ a ] ρ  + E[ b ] ρ
E[ const x ] ρ = x

open import Function using (_∘_)

_bind_ : Domain → (ℕ → Domain) → Domain
V x   bind f = f x
R r k bind f = R r ((λ m → m bind f) ∘ k)

S[_] : S → Env → Domain
S[ return x         ] ρ = V (E[ x ] ρ)
S[ s >>= (x , body) ] ρ = (S[ s ] ρ) bind (λ n → S[ body ] ((x , n) ∷ ρ))
S[ GET (x , body)   ] ρ = R GET (λ n → S[ body ] ((x , n) ∷ ρ))
S[ PUT x s          ] ρ = R (PUT (E[ x ] ρ)) (λ n → S[ s ] ρ)

handlerS : ℕ → Domain → ℕ
handlerS init (V x)         = x
handlerS init (R GET k)     = handlerS init (k init)
handlerS init (R (PUT x) k) = handlerS x (k x)

runS : ℕ → Domain → (ℕ × ℕ)
runS s₀ (V x)         = s₀ , x
runS s₀ (R GET k)     = runS s₀ (k s₀)
runS s₀ (R (PUT x) k) = runS x (k x)

left-identity : ∀ x v body ρ → S[ return x >>= (v , body) ] ρ ≡ S[ body ] ((v , E[ x ] ρ) ∷ ρ)
left-identity x v body ρ = refl

postulate
  lookup-lemma : ∀ ρ v n → lookupEnv ((v , n) ∷ ρ) v ≡ n
  extensionality : ∀ {A B : Set} {f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g
open ≡-Reasoning

bind-V : ∀ m → (m bind V) ≡ m
bind-V (V x) = refl
bind-V (R r k) = cong (R r) (extensionality (λ x → bind-V (k x)))

right-identity : ∀ m v ρ → S[ m >>= (v , return (var v)) ] ρ ≡ S[ m ] ρ
right-identity m v ρ =
  begin
      (S[ m ] ρ bind (λ z → V (lookupEnv ((v , z) ∷ ρ) v)))
  ≡⟨ cong (λ x → S[ m ] ρ bind x) (extensionality (λ x → cong V (lookup-lemma ρ v x))) ⟩
      (S[ m ] ρ bind V)
  ≡⟨ bind-V (S[ m ] ρ) ⟩
      S[ m ] ρ
  ∎

get-put : ∀ init m ρ v → runS init (S[ GET (v , PUT (var v) m) ] ρ) ≡ runS init (S[ GET (v , m) ] ρ)
get-put i m ρ v =
  begin
      runS (lookupEnv ((v , i) ∷ ρ) v) (S[ m ] ((v , i) ∷ ρ))
  ≡⟨ cong (λ x → runS x (S[ m ] ((v , i) ∷ ρ))) (lookup-lemma ρ v i) ⟩
      runS i (S[ m ] ((v , i) ∷ ρ))
  ∎

data Context : Set where
  [_] : S → Context
  bind-left : Context → (𝕍 × S) → Context
  bind-right : S → 𝕍 → Context → Context
  put : E → Context → Context
  get : 𝕍 → Context → Context

fill : Context → S → S
fill [ x ] s = s
fill (bind-left c x) s = fill c s >>= x
fill (bind-right x v c) s = x >>= (v , (fill c s))
fill (put x c) s = PUT x (fill c s)
fill (get x c) s = GET (x , (fill c s))

-- thm : ∀ i ctx s → handlerS i (fill ctx s)

lemma : ∀ s₀ D E → runS s₀ (D bind E) ≡ runS (proj₁ (runS s₀ D)) (E (proj₂ (runS s₀ D)))
lemma s₀ (V x) E         = refl
lemma s₀ (R GET k) E     = lemma s₀ (k s₀) E
lemma s₀ (R (PUT x) k) E = lemma x (k x) E

--
-- get-put' : ∀ ctx s₀ m ρ v → handlerS s₀ (S[ fill ctx (GET (v , PUT (var v) m)) ] ρ) ≡ handlerS s₀ (S[ fill ctx (GET (v , m)) ] ρ)
-- get-put' [ x ] s₀ m ρ v = get-put s₀ m ρ v
-- get-put' (bind-left ctx x) s₀ m ρ v =
--   begin
--       handlerS s₀ (S[ fill ctx (GET (v , PUT (var v) m)) >>= x ] ρ)
--   ≡⟨ {!   !} ⟩
--       {!   !}
--   ≡⟨ {!   !} ⟩
--       {!   !}
--   ≡⟨ {!   !} ⟩
--       {!   !}
--   ≡⟨ {!   !} ⟩
--       handlerS s₀ (S[ fill ctx (GET (v , m)) >>= x ] ρ)
--   ∎
-- get-put' (bind-right x x₁ ctx) s₀ m ρ v = {!   !}
-- get-put' (put x ctx) s₀ m ρ v = {!   !}
-- get-put' (get x ctx) s₀ m ρ v = {!   !}
--   -- begin
--   --     {!  !}
--   -- ≡⟨ {!   !} ⟩
--   --     {!   !}
--   -- ≡⟨ {!   !} ⟩
--   --     {!   !}
--   -- ≡⟨ {!   !} ⟩
--   --     {!   !}
--   -- ≡⟨ {!   !} ⟩
--   --     {!   !}
--   -- ∎
