\documentclass{article}

% build by
%  lhs2TeX EffProofs01.lhs | pdflatex --jobname=EffProofs01

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{scalerel}

%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt

%include ../common/Formatting.fmt
%include ../common/Monad.fmt

%let showproofs = True


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\newcommand{\todo}[1]{{\bf todo}: {#1}}

%format m1
%format m2
%format m3
%format f1
%format f2
%format e1
%format e2
%format v1
%format v2
%format x1
%format x2
%format x3
%format x4
%format k1
%format k2
%format k3
%format k4

%format `sse` = "\mathrel{\subseteq}"
%format `spe` = "\mathrel{\supseteq}"

%format `sqse` = "\mathrel{\sqsubseteq}"
%format sqse = "(\sqsubseteq)"
%format `sqpe` = "\mathrel{\sqsupseteq}"
%format sqpe = "(\sqsupseteq)"

%format exists = "\exists"
%format rho = "\rho"

\begin{document}

\title{Misc. Proofs Regarding Effect Handling}
\author{Shin-Cheng Mu}
\date{2019}
\maketitle

%format lift f = "{"f"}^{\dagger}"
%format semE = "{\cal E}"

\paragraph{Specification} {~}\\
\begin{spec}
data E = Lit Nat | Var Str | E + E {-"~~,"-}
data D = L Nat | V Str (Nat -> D) {-"~~."-}
\end{spec}
\begin{spec}
(lift) :: (Nat -> D) -> D -> D
lift f (L n)    = f n
lift f (V x k)  = V x (lift f . k) {-"~~."-}
\end{spec}
\begin{spec}
semE :: E -> D
semE (Lit n)    = L n
semE (Var x)    = V x L
semE (e1 + e2)  = lift (\v1 -> lift (\v2 -> L (v1+v2)) (semE e2)) (semE e1) {-"~~."-}
\end{spec}

\begin{spec}
hdl :: Env -> D -> Nat
hdl rho (L n)    = n
hdl rho (V x k)  = hdl rho (k (rho x)) {-"~~."-}
\end{spec}

The following lemma allows us to cancel a |(lift)| after
a |hdl| by promoting |hdl| to the right.
It is due to Zong-Ru Jiang.
\begin{lemma}
\label{lma:hdl-lift-promote}
   For all |rho| and |f| we have
|hdl rho . lift f = hdl rho . f . hdl rho|.
\end{lemma}
\begin{proof}
Prove that |(hdl rho . lift f) d = (hdl rho . f . hdl rho) d|
by induction on |d|. For |d := L n| we have
\begin{spec}
   hdl rho (lift f (L n))
=    {- definition of |(lift)| -}
   hdl rho (f n)
=    {- definition of |hdl| -}
   hdl rho (f (hdl rho (L n))) {-"~~."-}
\end{spec}
For |d := V x k| we reason:
\begin{spec}
   hdl rho (lift f (V x k))
=    {- definition of |(lift)| -}
   hdl rho (V x (lift f . k))
=    {- definitin of |hdl| -}
   hdl rho (lift f (k (rho x)))
=    {- induction -}
   hdl rho (f (hdl rho (k (rho x))))
=    {- definition of |hdl| -}
   hdl rho (f (hdl rho (V x k))) {-"~~."-}
\end{spec}
\end{proof}

\begin{theorem} For all |rho|, |e1|, and |e2| we have
|hdl rho (semE (e1 + e2)) = hdl rho (semE e1) + hdl rho (semE e2)|.
\end{theorem}
\begin{proof} We reason:
\begin{spec}
   hdl rho (semE (e1 + e2))
=    {- definition of |semE| -}
   hdl rho (lift (\v1 -> lift (L . (v1+)) (semE e2)) (semE e1))
=    {- by Lemma~\ref{lma:hdl-lift-promote} -}
   hdl rho ((\v1 -> lift (L . (v1+)) (semE e2)) (hdl rho (semE e1)))
=  hdl rho (lift (L . ((hdl rho (semE e1)) +)) (semE e2))
=    {- by Lemma~\ref{lma:hdl-lift-promote} -}
   hdl rho (L (hdl rho (semE e1) + hdl rho (semE e2)))
=    {- definition of |hdl| -}
   hdl rho (semE e1) + hdl rho (semE e2) {-"~~."-}
\end{spec}
\end{proof}

\begin{corollary} For all |rho|, |e1|, and |e2| we have:
\begin{equation*}
 |hdl rho (semE (e1 + e2)) = hdl rho (semE (e2 + e1))| \mbox{~~.}
\end{equation*}
\end{corollary}
\begin{proof}
Immediate, from the previous theorem.
\end{proof}
%
% To prove the theorem, the case when the semantics of either |e1| or |e2| is |L| is easier.
% Consider the case |semE e1 = V x1 k1| and |semE e2 = V x2 k2|.
% Expanding |semE (e1 + e2)|:
% \begin{spec}
%    semE (e1 + e2)
% =  lift (\v1 -> lift (L . (v1+)) (semE e2)) (semE e1)
% =    {- since |semE e1 = V x1 k1| -}
%    V x1 (lift (\v1 -> lift (L . (v1+)) (semE e2)) . k1)
% =    {- since |semE e2 = V x2 k2| -}
%    V x1 (lift (\v1 -> V x2 (lift (L . (v1+)) . k2)) . k1) {-"~~."-}
% \end{spec}
% Similarly with |semE (e2 + e1)|.
% Hence we have
% \begin{align*}
% |hdl rho (semE (e1 + e2))| &= |hdl rho (lift (\v1 -> V x2 (lift (L . (v1+)) . k2)) (k1 (rho x1)))| \mbox{~~,}\\
% |hdl rho (semE (e2 + e1))| &= |hdl rho (lift (\v2 -> V x1 (lift (L . (v2+)) . k1)) (k2 (rho x2)))| \mbox{~~.}
% \end{align*}
% The aim is to prove the following lemma.
%
% \begin{lemma}
% For all |x1|, |x2|, |k1|, |k2|, we have
% \begin{spec}
%   hdl rho (lift (\v1 -> V x2 (lift (L . (v1+)) . k2)) (k1 (rho x1))) =
%      hdl rho (lift (\v2 -> V x1 (lift (L . (v2+)) . k1)) (k2 (rho x2))) {-"~~."-}
% \end{spec}
% \end{lemma}
% \begin{proof}
% I assume that the proof can be done easily when
% either |k1 (rho x1)| yields a |V n| for some |n|.
% We consider the case when |k1 (rho x1) = V x3 k3|, and reason:
% \begin{spec}
%   hdl rho (lift (\v1 -> V x2 (lift (L . (v1+)) . k2)) (k1 (rho x1)))
% =  {-  since |k1 (rho x1) = V x3 k3|, definition of |(lift)| -}
%   hdl rho (V x3 (lift (\v1 -> V x2 (lift (L . (v1+)) . k2)) . k3))
% =  {- definition of |hdl| -}
%   hdl rho (lift (\v1 -> V x2 (lift (L . (v1+)) . k2)) (k3 (rho x3)))
% =  {- induction -}
%   hdl rho (lift (\v2 -> V x3 (lift (L . (v2+)) . k3)) (k2 (rho x2)))
% =  {- since |k1 (rho x1) = V x3 k3|, see lemma below.-}
%   hdl rho (lift (\v2 -> V x1 (lift (L . (v2+)) . k1)) (k2 (rho x2)))
% \end{spec}
% \end{proof}
%
% \begin{lemma}
% If |k1 (rho x1) = V x3 k3|, we have that for all |d|:
% \begin{spec}
%  hdl rho (lift (\v -> V x1 (lift (L . (v+)) . k1)) d) =
%      hdl rho (lift (\v -> V x3 (lift (L . (v+)) . k3)) d) {-"~~."-}
% \end{spec}
% \end{lemma}
% \begin{proof}
% Analyzing |d|. If |d = L n|:
% \begin{spec}
%    hdl rho (lift (\v -> V x1 (lift (L . (v+)) . k1)) (L n))
% =    {- definition of |(lift)| -}
%    hdl rho (V x1 (lift (L . (n+)) . k1))
% =    {- definition of |hdl| -}
%    hdl rho (lift (L . (n+)) (k1 (rho x1)))
% =    {- since |k1 (rho x1) = V x3 k3|, definition of |(lift)| -}
%    hdl rho (V x3 (lift (L . (n+)) . k3))
% =    {- definition of |(lift)|  -}
%    hdl rho (lift (\v -> V x3 (lift (L . (v+)) . k3)) (L n)) {-"~~."-}
% \end{spec}
% If |d = V y g|,
% \begin{spec}
%    hdl rho (lift (\v -> V x1 (lift (L . (v+)) . k1)) (V y g))
% =    {- definition of |(lift)| -}
%    hdl rho (V y (lift (\v -> V x1 (lift (L . (v+)) . k1)) . g))
% =    {- definition of |hdl| -}
%    hdl rho (lift (\v -> V x1 (lift (L . (v+)) . k1)) (g (rho y)))
% =    {- induction -}
%    hdl rho (lift (\v -> V x3 (lift (L . (v+)) . k3)) (g (rho y)))
% =    {- definition of |hdl| -}
%    hdl rho (V y (lift (\v -> V x3 (lift (L . (v+)) . k3)) . g))
% =    {- definition of |(lift)|  -}
%    hdl rho (lift (\v -> V x3 (lift (L . (v+)) . k3)) (V y g)) {-"~~."-}
% \end{spec}
% \end{proof}
\end{document}
